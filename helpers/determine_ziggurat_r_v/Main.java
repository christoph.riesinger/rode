public class Main {
	static final int N = 256;
	
	static public double f(double x) {
		double result;
		
		result = Math.exp(-(x * x) / 2.0d);
		
		return result;
	}
	
	static public double fInv(double y) {
		double result;
		
		result = Math.sqrt(-2. * Math.log(y));
		
		return result;
	}
	
	static public double getNextY(double x, double y, double v) {
		double result;
		
		result = y + v / x;
		
		return result;
	}
	
	static public double getTailArea(double x) {
		double result;
		
		result = Math.sqrt(Math.PI / 2.) * org.apache.commons.math3.special.Erf.erfc(x / Math.sqrt(2.));
		
		return result;
	}
	
	static public double iterate(double x) {
		double x_i, y_i, v;
		
		x_i = x;
		y_i = f(x);
		v = x_i * y_i + getTailArea(x);
		
		for(int i = N - 1; i >= 1; i--) {
			y_i = getNextY(x_i, y_i, v);
			
			x_i = fInv(y_i);
		}
		
		return y_i;
	}
	
	public static void main(String[] args) {
		double r, v, lowerBound, upperBound, y_1;
		
		lowerBound = 0.;
		upperBound = 10.;
		r = 3.;
		
		System.out.println("iteration 0:\t" + 
				"lowerBound = " + lowerBound + "\t" +
				"upperBound = " + upperBound);
		
		for(int i = 1; i <= 100; i++) {
			r = (lowerBound + upperBound) / 2.;
			y_1 = iterate(r);
			
			if(y_1 < 1.) {
				upperBound = r;
			} else {
				lowerBound = r;
			}
			
			System.out.println("iteration " + i + ":\t" + 
					"lowerBound = " + lowerBound + "\t" +
					"upperBound = " + upperBound + "\t" +
					"y_1 = " + y_1 + "\t" +
					"r = " + r);
		}
		
		v = r * f(r) + getTailArea(r);
		
		System.out.println("r = " + r + " v = " + v);
	}
}
