#include "RDTSCTimer.hpp"

RDTSCTimer::RDTSCTimer() : Timer() {
}

RDTSCTimer::~RDTSCTimer() {
}

#if defined(__i386__)

unsigned long long RDTSCTimer::rdtsc(void) {
	unsigned long long int x;
	__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
	return x;
}

#elif defined(__x86_64__)

unsigned long long RDTSCTimer::rdtsc(void) {
	unsigned hi, lo;
	__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
	return ((unsigned long long) lo) | (((unsigned long long) hi)<<32);
}

#endif

int RDTSCTimer::startTimer() {
	if(timerRunning) {
		return 0;
	} else {
		startTime = rdtsc();

		timerHasBeenStartedOnce = 1;
		timerRunning = 1;

		return 1;
	}
}

int RDTSCTimer::endTimer() {
	if(!timerRunning) {
		return 0;
	} else {
		endTime = rdtsc();

		timerRunning = 0;

		return 1;
	}
}

int RDTSCTimer::isTimerRunning() {
	return timerRunning;
}

int RDTSCTimer::getElapsedTime(float &elapsedTime) {
	elapsedTime = -1;

	if(timerHasBeenStartedOnce && !timerRunning) {
		/*
		 * TODO: Transform from "cycles elapsed" to "seconds"
		 */
		elapsedTime = (float) (endTime - startTime);

		return 1;
	} else {
		return 0;
	}
}
