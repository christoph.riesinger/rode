#pragma once

#include "../helper.h"

class Timer {
/*
 * NOTE: Protected members are inherited to sub-classes. That does not hold for
 * private members.
 */
protected:
	bool timerHasBeenStartedOnce;
	bool timerRunning;

public:
	Timer() {
		timerHasBeenStartedOnce = false;
		timerRunning = false;
	}

	/**
	 *
	 *
	 * NOTE: There are NO pure ("= 0") virtual ("virtual") destructors. So even
	 * a virtual destructor needs an (at least) empty body.
	 */
	virtual ~Timer() {
	}

	virtual int startTimer() = 0;
	virtual int endTimer() = 0;
	virtual int isTimerRunning() = 0;
	virtual int getElapsedTime(float &elapsedTime) = 0;
};
