#include "GPUTimer.hpp"

#include <cuda.h>
#include <cuda_runtime.h>

GPUTimer::GPUTimer() : Timer() {
	this->stream = 0;
	hasStream = FALSE;

	cudaEventCreate(&startTime);
	cudaEventCreate(&endTime);
}

GPUTimer::GPUTimer(cudaStream_t &stream) : Timer() {
	this->stream = &stream;
	hasStream = TRUE;

	cudaEventCreate(&startTime);
	cudaEventCreate(&endTime);
}

GPUTimer::~GPUTimer() {
	cudaEventDestroy(endTime);
	cudaEventDestroy(startTime);
}

int GPUTimer::startTimer() {
	if(timerRunning) {
		return 0;
	} else {
		if(hasStream) {
			cudaStreamSynchronize(*stream);
		}
		cudaEventRecord(startTime, hasStream ? *(this->stream) : 0);
		cudaEventSynchronize(startTime);

		timerHasBeenStartedOnce = 1;
		timerRunning = 1;

		return 1;
	}
}

int GPUTimer::endTimer() {
	if(!timerRunning) {
		return 0;
	} else {
		if(hasStream) {
			cudaStreamSynchronize(*stream);
		}
		cudaEventRecord(endTime, hasStream ? *(this->stream) : 0);
		cudaEventSynchronize(endTime);

		timerRunning = 0;

		return 1;
	}
}

int GPUTimer::isTimerRunning() {
	return timerRunning;
}

int GPUTimer::getElapsedTime(float &elapsedTime) {
	elapsedTime = -1;

	if(timerHasBeenStartedOnce && !timerRunning) {
		cudaEventElapsedTime(&elapsedTime, startTime, endTime);
		elapsedTime *= 1e3f;

		return 1;
	} else {
		return 0;
	}
}
