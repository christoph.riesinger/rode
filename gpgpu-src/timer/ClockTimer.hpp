#pragma once

#include "Timer.hpp"

class ClockTimer : public Timer{
private:
	double startTime;
	double endTime;

public:
	ClockTimer();
	~ClockTimer();

	int startTimer();
	int endTimer();
	int isTimerRunning();
	int getElapsedTime(float &elapsedTime);
};
