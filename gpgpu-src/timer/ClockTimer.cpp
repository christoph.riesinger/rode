#include "ClockTimer.hpp"

#include <time.h>

ClockTimer::ClockTimer() : Timer() {
}

ClockTimer::~ClockTimer() {
}

int ClockTimer::startTimer() {
	if(timerRunning) {
		return 0;
	} else {
		startTime = clock();

		timerHasBeenStartedOnce = 1;
		timerRunning = 1;

		return 1;
	}
}

int ClockTimer::endTimer() {
	if(!timerRunning) {
		return 0;
	} else {
		endTime = clock();

		timerRunning = 0;

		return 1;
	}
}

int ClockTimer::isTimerRunning() {
	return timerRunning;
}

int ClockTimer::getElapsedTime(float &elapsedTime) {
	elapsedTime = -1;

	if(timerHasBeenStartedOnce && !timerRunning) {
		elapsedTime = (float) (endTime - startTime);
		elapsedTime /= CLOCKS_PER_SEC;

		return 1;
	} else {
		return 0;
	}
}
