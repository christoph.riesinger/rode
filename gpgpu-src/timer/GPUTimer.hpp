#pragma once

#include "Timer.hpp"

#include <cuda.h>
#include <cuda_runtime.h>

class GPUTimer : public Timer{
private:
	cudaEvent_t startTime;
	cudaEvent_t endTime;

	int hasStream;
	cudaStream_t *stream;

public:
	GPUTimer();
	GPUTimer(cudaStream_t &stream);
	~GPUTimer();

	int startTimer();
	int endTimer();
	int isTimerRunning();
	int getElapsedTime(float &elapsedTime);
};
