#pragma once

#include "Timer.hpp"

class RDTSCTimer : public Timer{
private:
	unsigned long long startTime;
	unsigned long long endTime;

	unsigned long long rdtsc();

public:
	RDTSCTimer();
	~RDTSCTimer();

	int startTimer();
	int endTimer();
	int isTimerRunning();
	int getElapsedTime(float &elapsedTime);
};
