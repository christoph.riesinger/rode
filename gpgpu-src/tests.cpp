#include "tests.h"

void printConstants() {
	std::cout << std::setprecision(10) << "END_TIME                          = " << END_TIME << std::endl;
	std::cout << std::setprecision(10) << "NUM_OF_COARSE_STEPS_PER_TIME_UNIT = " << NUM_OF_COARSE_STEPS_PER_TIME_UNIT << std::endl;
	std::cout << std::setprecision(10) << "NUM_OF_FINE_STEPS_PER_COARSE_STEP = " << NUM_OF_FINE_STEPS_PER_COARSE_STEP << std::endl;
	std::cout << std::setprecision(10) << "NUM_OF_TOTAL_COARSE_STEPS         = " << NUM_OF_TOTAL_COARSE_STEPS << std::endl;
	std::cout << std::setprecision(10) << "NUM_OF_TOTAL_FINE_STEPS           = " << NUM_OF_TOTAL_FINE_STEPS << std::endl;
	std::cout << std::setprecision(10) << "COARSE_STEP_SIZE                  = " << COARSE_STEP_SIZE << std::endl;
	std::cout << std::setprecision(10) << "FINE_STEP_SIZE                    = " << FINE_STEP_SIZE << std::endl;
	std::cout << std::setprecision(10) << "DEGREE_OF_SOLVER_PARALLELISM      = " << DEGREE_OF_SOLVER_PARALLELISM << std::endl;
}

void _scanExclusiveGold(
		TYPE *input,
		const TYPE mu,
		const unsigned int numOfElements,
		TYPE *output) {
	output[0] = (TYPE) 0.0;

	for(unsigned int i = 1; i < numOfElements; i++) {
		output[i] = mu * output[i - 1] + input[i - 1];
	}
}

void _scanInclusiveGold(
		TYPE *input,
		const TYPE mu,
		const unsigned int numOfElements,
		TYPE *output) {
	output[0] = (TYPE) input[0];

	for(unsigned int i = 1; i < numOfElements; i++) {
		output[i] = mu * output[i - 1] + input[i];
	}
}

void _ouGold(
		TYPE *input,
		const TYPE O_0,
		const TYPE stepSize,
		const unsigned int numOfRealizations,
		TYPE *output) {
	TYPE mu, sigma;

	computeMu<TYPE>(stepSize, mu);
	computeSigma<TYPE>(stepSize, sigma);

	output[0] = mu * O_0 + sigma * input[0];
	for(unsigned int i = 1; i < numOfRealizations; i++) {
		output[i] = mu * output[i - 1] + sigma * input[i];
	}
}

void _singleAverageGold(
		const TYPE *input,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerAverage,
		const unsigned int numOfAverages,
		TYPE *output) {
	TYPE sum;

	for(unsigned int i = 0; i < numOfAverages; i++) {
		sum = (TYPE) 0;

		for(unsigned int j = 0; j < numOfElementsPerAverage; j++) {
			if(i * numOfElementsPerAverage + j == numOfElements) {
				break;
			}

			sum += input[i * numOfElementsPerAverage + j];
		}

		output[i] = ((TYPE) 1 / numOfElementsPerAverage) * sum;
	}
}

void _doubleAverageGold(
		const TYPE *input,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerAverage,
		const unsigned int numOfAverages,
		TYPE *output) {
	TYPE sum;

	for(unsigned int i = 0; i < numOfAverages; i++) {
		sum = (TYPE) 0;

		for(unsigned int j = 0; j < numOfElementsPerAverage; j++) {
			if(i * numOfElementsPerAverage + j == numOfElements) {
				break;
			}

			sum += (numOfElementsPerAverage - j) * input[i * numOfElementsPerAverage + j];
		}

		output[i] = ((TYPE) 2 / (numOfElementsPerAverage * numOfElementsPerAverage)) * sum;
	}
}

void _tridiagIntegralApproximationGold(
		const TYPE *input,
		const TYPE initElement,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerIntegral,
		const unsigned int numOfIntegrals,
		const unsigned int d,
		const TYPE coarseStepSize,
		const TYPE fineStepSize,
		TYPE *output) {
	TYPE initElement_current, x, integral;

	initElement_current = initElement;

	for(unsigned int i = 0; i < numOfIntegrals; i++) {
		integral = (TYPE) 0;

		for(unsigned int j = 0; j < numOfElementsPerIntegral; j++) {
			if(i * numOfElementsPerIntegral + j >= numOfElements) {
				break;
			}

			power((coarseStepSize - (j + 1) * fineStepSize), d, x);
			integral += x * (initElement_current - input[i * numOfElementsPerIntegral + j]);
		}

		output[i] = (fineStepSize / fac(d)) * integral;

		if((i + 1) * numOfElementsPerIntegral <= numOfElements) {
			initElement_current = input[(i + 1) * numOfElementsPerIntegral - 1];
		}
	}
}

void testRand() {
	cudaStream_t stream;
	// UniformSerialNormalSharedConfiguration<curandStateXORWOW_t, RandStateZiggurat> *dConfigurations;
	// UniformSerialNormalSerialConfiguration<curandStateXORWOW_t, RandStateWichura> *dConfigurations;
	UniformSerialNormalParallelConfiguration<curandStateXORWOW_t, RandStateWallace> *dConfigurations;
	TYPE *hRn;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	// GPU_ERROR_CHECK(cudaMalloc(&dConfigurations, RAND_NUM_OF_BLOCKS * sizeof(UniformSerialNormalSharedConfiguration<curandStateXORWOW_t, RandStateZiggurat>)))
	// GPU_ERROR_CHECK(cudaMalloc(&dConfigurations, RAND_NUM_OF_BLOCKS * sizeof(UniformSerialNormalSerialConfiguration<curandStateXORWOW_t, RandStateWichura>)))
	GPU_ERROR_CHECK(cudaMalloc(&dConfigurations, RAND_NUM_OF_BLOCKS * sizeof(UniformSerialNormalParallelConfiguration<curandStateXORWOW_t, RandStateWallace>)))
	hRn = new TYPE[RAND_NUM_OF_BLOCKS * RAND_NUM_OF_THREADS * RAND_NUM_OF_NUMBERS_PER_THREAD];

	initStatesNormal_D<UniformSerialNormalParallelConfiguration, curandStateXORWOW_t, RandStateWallace>(
			5678,
			RAND_NUM_OF_BLOCKS,
			dConfigurations,
			stream);

	getRandomNumbersNormal_H<UniformSerialNormalParallelConfiguration, curandStateXORWOW_t, RandStateWallace, TYPE>(
			dConfigurations,
			RAND_NUM_OF_BLOCKS * RAND_NUM_OF_THREADS * RAND_NUM_OF_NUMBERS_PER_THREAD,
			RAND_NUM_OF_NUMBERS_PER_THREAD,
			hRn,
			stream);

	/*
	for(unsigned int i = 0; i < RAND_NUM_OF_BLOCKS * RAND_NUM_OF_THREADS * RAND_NUM_OF_NUMBERS_PER_THREAD; i++) {
		std::cout << std::setprecision(10) << hRn[i] << std::endl;
	}
	*/

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete hRn;
	GPU_ERROR_CHECK(cudaFree(dConfigurations))
}

void testScan() {
	const unsigned int NUM_OF_ELEMENTS = 33217721;
	// const unsigned int NUM_OF_ELEMENTS = 843397;

	cudaStream_t stream;
	TYPE *input, *scanGold;
	TYPE *scan;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	input = new TYPE[NUM_OF_ELEMENTS];
	scanGold = new TYPE[NUM_OF_ELEMENTS];
	scan = new TYPE[NUM_OF_ELEMENTS];

	for(unsigned int i = 0; i < NUM_OF_ELEMENTS; i++) {
		input[i] = (TYPE) (toFloat2(rand()) * 2.0f - 1.0f);
		// input[i] = (TYPE) 1.0;
	}

	/*
	_scanExclusiveGold(
			input,
			(TYPE) 0.999999,
			NUM_OF_ELEMENTS,
			scanGold);
	*/

	scanOU_H2H(
			input,
			(TYPE) 0.999999,
			NUM_OF_ELEMENTS,
			scan,
			stream);

	/*
	for(unsigned int i = 0; i < NUM_OF_ELEMENTS; i++) {
		std::cout << std::setprecision(10) << ((scan[i] - scanGold[i]) / scanGold[i]) << std::endl;
	}
	*/

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete scan;
	delete scanGold;
	delete input;
}

void testOU() {
	const unsigned int NUM_OF_REALIZATIONS = (1<<26);
	// const unsigned int NUM_OF_REALIZATIONS = 33217721;
	// const unsigned int NUM_OF_REALIZATIONS = 843397;

	cudaStream_t stream;
	TYPE *input, *ouGold;
	TYPE *ou;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	input = new TYPE[NUM_OF_REALIZATIONS];
	ouGold = new TYPE[NUM_OF_REALIZATIONS];
	ou = new TYPE[NUM_OF_REALIZATIONS];

	for(unsigned int i = 0; i < NUM_OF_REALIZATIONS; i++) {
		input[i] = (TYPE) (toFloat2(rand()) * 2.0f - 1.0f);
		// input[i] = (TYPE) 1.0;
	}

	/*
	_ouGold(
			input,
			(TYPE) 0,
			(TYPE) FINE_STEP_SIZE,
			NUM_OF_REALIZATIONS,
			ouGold);
	*/

	realizeOUProcess_H2H<TYPE>(
			input,
			(TYPE) 0,
			(TYPE) FINE_STEP_SIZE,
			NUM_OF_REALIZATIONS,
			ou,
			stream);

	/*
	for(unsigned int i = 0; i < NUM_OF_REALIZATIONS; i++) {
		std::cout << std::setprecision(10) << ((ou[i] - ouGold[i]) / ouGold[i]) << std::endl;
	}
	*/

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete ou;
	delete ouGold;
	delete input;
}

/*
void testOU() {
	cudaStream_t stream;
	TYPE *rnInput, *ouGold;
	TYPE *ou;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	rnInput = new TYPE[NUM_OF_TOTAL_FINE_STEPS];
	ouGold = new TYPE[NUM_OF_TOTAL_FINE_STEPS];
	ou = new TYPE[NUM_OF_TOTAL_FINE_STEPS];

	const char *rnInputFilename = "rn_gold.dat";
	const char *ouGoldFilename = "ou_gold.dat";
	std::ifstream rnInputFile(rnInputFilename);
	std::ifstream ouGoldFile(ouGoldFilename);

	rnInputFile >> rnInput[0];
	ouGoldFile >> ouGold[0];
	for(unsigned int i = 0; i < NUM_OF_TOTAL_FINE_STEPS; i++) {
		rnInputFile >> rnInput[i];
		ouGoldFile >> ouGold[i];
	}

	ouGoldFile.close();
	rnInputFile.close();

	realizeOUProcess_H2H<TYPE>(
			rnInput,
			0.0,
			(TYPE) FINE_STEP_SIZE,
			NUM_OF_TOTAL_FINE_STEPS,
			ou,
			stream);

	for(unsigned int i = 0; i < NUM_OF_TOTAL_FINE_STEPS; i++) {
		std::cout << std::setprecision(10) << ((ou[i] - ouGold[i]) / ouGold[i]) << std::endl;
	}

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete ou;
	delete ouGold;
	delete rnInput;
}
*/

void testSingleAverage() {
	const unsigned int NUM_OF_ELEMENTS = (1<<27);
	// const unsigned int NUM_OF_ELEMENTS = 147441387;
	const unsigned int NUM_OF_ELEMENTS_PER_SINGLE_AVERAGE = (1<<15);
	// const unsigned int NUM_OF_ELEMENTS_PER_SINGLE_AVERAGE = 27997;
	// const unsigned int NUM_OF_ELEMENTS_PER_SINGLE_AVERAGE = 8434199;
	const unsigned int NUM_OF_SINGLE_AVERAGES = divUp(NUM_OF_ELEMENTS, NUM_OF_ELEMENTS_PER_SINGLE_AVERAGE);

	cudaStream_t stream;
	TYPE *input, *averagesGold, *averages;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	input = new TYPE[NUM_OF_ELEMENTS];
	averagesGold = new TYPE[NUM_OF_SINGLE_AVERAGES];
	averages = new TYPE[NUM_OF_SINGLE_AVERAGES];

	for(unsigned int i = 0; i < NUM_OF_ELEMENTS; i++) {
		input[i] = (TYPE) (toFloat2(rand()) * 2.0f - 1.0f);
		// input[i] = (TYPE) 1.0;
	}

	/*
	_singleAverageGold(
			input,
			NUM_OF_ELEMENTS,
			NUM_OF_ELEMENTS_PER_SINGLE_AVERAGE,
			NUM_OF_SINGLE_AVERAGES,
			averagesGold);
	*/

	singleAverage_H2H<TYPE>(
			input,
			NUM_OF_ELEMENTS,
			NUM_OF_ELEMENTS_PER_SINGLE_AVERAGE,
			NUM_OF_SINGLE_AVERAGES,
			averages,
			stream);

	for(unsigned int i = 0; i < NUM_OF_SINGLE_AVERAGES; i++) {
		// std::cout << std::setprecision(10) << ((averages[i] - averagesGold[i]) / averagesGold[i]) << std::endl;
	}

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete averages;
	delete averagesGold;
	delete input;
}

void testDoubleAverage() {
	const unsigned int NUM_OF_ELEMENTS = (1<<27);
	// const unsigned int NUM_OF_ELEMENTS = 147441387;
	const unsigned int NUM_OF_ELEMENTS_PER_DOUBLE_AVERAGE = (1<<15);
	// const unsigned int NUM_OF_ELEMENTS_PER_DOUBLE_AVERAGE = 27997;
	// const unsigned int NUM_OF_ELEMENTS_PER_DOUBLE_AVERAGE = 8434199;
	const unsigned int NUM_OF_DOUBLE_AVERAGES = divUp(NUM_OF_ELEMENTS, NUM_OF_ELEMENTS_PER_DOUBLE_AVERAGE);

	cudaStream_t stream;
	TYPE *input, *averagesGold, *averages;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	input = new TYPE[NUM_OF_ELEMENTS];
	averagesGold = new TYPE[NUM_OF_DOUBLE_AVERAGES];
	averages = new TYPE[NUM_OF_DOUBLE_AVERAGES];

	for(unsigned int i = 0; i < NUM_OF_ELEMENTS; i++) {
		input[i] = (TYPE) (toFloat2(rand()) * 2.0f - 1.0f);
		// input[i] = (TYPE) 1.0;
	}

	/*
	_doubleAverageGold(
			input,
			NUM_OF_ELEMENTS,
			NUM_OF_ELEMENTS_PER_DOUBLE_AVERAGE,
			NUM_OF_DOUBLE_AVERAGES,
			averagesGold);
	*/

	doubleAverage_H2H<TYPE>(
			input,
			NUM_OF_ELEMENTS,
			NUM_OF_ELEMENTS_PER_DOUBLE_AVERAGE,
			NUM_OF_DOUBLE_AVERAGES,
			averages,
			stream);

	for(unsigned int i = 0; i < NUM_OF_DOUBLE_AVERAGES; i++) {
		// std::cout << std::setprecision(10) << ((averages[i] - averagesGold[i]) / averagesGold[i]) << std::endl;
	}

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete averages;
	delete averagesGold;
	delete input;
}

void testTridiagIntegralApproximation() {
	const unsigned int NUM_OF_ELEMENTS = (1<<27);
	// const unsigned int NUM_OF_ELEMENTS = 147441387;
	const unsigned int NUM_OF_ELEMENTS_PER_TRIDIAG_INTEGRAL = (1<<15);
	// const unsigned int NUM_OF_ELEMENTS_PER_TRIDIAG_INTEGRAL = 27997;
	// const unsigned int NUM_OF_ELEMENTS_PER_TRIDIAG_INTEGRAL = 8434199;
	const unsigned int NUM_OF_TRIDIAG_INTEGRALS = divUp(NUM_OF_ELEMENTS, NUM_OF_ELEMENTS_PER_TRIDIAG_INTEGRAL);

	cudaStream_t stream;
	TYPE *input, *integralsGold, *integrals;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	input = new TYPE[NUM_OF_ELEMENTS];
	integralsGold = new TYPE[NUM_OF_TRIDIAG_INTEGRALS];
	integrals = new TYPE[NUM_OF_TRIDIAG_INTEGRALS];

	for(unsigned int i = 0; i < NUM_OF_ELEMENTS; i++) {
		input[i] = (TYPE) (toFloat2(rand()) * 2.0f - 1.0f);
		// input[i] = (TYPE) 1.0;
	}

	/*
	_tridiagIntegralApproximationGold(
			input,
			(TYPE) 0.0,
			NUM_OF_ELEMENTS,
			NUM_OF_ELEMENTS_PER_TRIDIAG_INTEGRAL,
			NUM_OF_TRIDIAG_INTEGRALS,
			3,
			COARSE_STEP_SIZE,
			FINE_STEP_SIZE,
			integralsGold);
	*/

	tridiagIntegralApproximation_H2H<TYPE>(
			input,
			(TYPE) 0.0,
			NUM_OF_ELEMENTS,
			NUM_OF_ELEMENTS_PER_TRIDIAG_INTEGRAL,
			NUM_OF_TRIDIAG_INTEGRALS,
			3,
			COARSE_STEP_SIZE,
			FINE_STEP_SIZE,
			integrals,
			stream);

	for(unsigned int i = 0; i < NUM_OF_TRIDIAG_INTEGRALS; i++) {
		// std::cout << std::setprecision(10) << ((integrals[i] - integralsGold[i]) / integralsGold[i]) << std::endl;
	}

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete integrals;
	delete integralsGold;
	delete input;
}

void testAveragedEuler() {
	cudaStream_t stream;
	TYPE *z1Init, *z2Init, *ouInput, *ouSingleAveragesInput, *z1Gold, *z2Gold, *dduGold;
	TYPE *z1, *z2, *ddu;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	z1Init = new TYPE[DEGREE_OF_SOLVER_PARALLELISM];
	z2Init = new TYPE[DEGREE_OF_SOLVER_PARALLELISM];
	ouInput = new TYPE[NUM_OF_TOTAL_FINE_STEPS];
	ouSingleAveragesInput = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z1Gold = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z2Gold = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	dduGold = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z1 = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z2 = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	ddu = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];

	const char *ouInputFilename = "ou_gold.dat";
	const char *ouSingleAveragesInputFilename = "singleAverage_gold.dat";
	const char *zGoldFilename = "z_gold.dat";
	const char *dduGoldFilename = "ddu_gold.dat";
	std::ifstream ouInputFile(ouInputFilename);
	std::ifstream ouSingleAveragesInputFile(ouSingleAveragesInputFilename);
	std::ifstream zGoldFile(zGoldFilename);
	std::ifstream dduGoldFile(dduGoldFilename);

	for(unsigned int i = 0; i < DEGREE_OF_SOLVER_PARALLELISM; i++) {
		z1Init[i] = z2Init[i] = 0.0;
	}

	ouInputFile >> ouInput[0];
	zGoldFile >> z1Gold[0];
	zGoldFile >> z2Gold[0];
	dduGoldFile >> dduGold[0];
	for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
		for(unsigned int j = 0; j < NUM_OF_FINE_STEPS_PER_COARSE_STEP; j++) {
			ouInputFile >> ouInput[i * NUM_OF_FINE_STEPS_PER_COARSE_STEP + j];
		}

		ouSingleAveragesInputFile >> ouSingleAveragesInput[i];
		zGoldFile >> z1Gold[i];
		zGoldFile >> z2Gold[i];
		dduGoldFile >> dduGold[i];

	}

	dduGoldFile.close();
	zGoldFile.close();
	ouSingleAveragesInputFile.close();
	ouInputFile.close();

	averagedEuler_H2H<TYPE>(
			z1Init,
			z2Init,
			ouInput,
			NUM_OF_FINE_STEPS_PER_COARSE_STEP,
			ouSingleAveragesInput,
			NUM_OF_TOTAL_COARSE_STEPS,
			DEGREE_OF_SOLVER_PARALLELISM,
			(TYPE) COARSE_STEP_SIZE,
			z1,
			z2,
			ddu,
			stream);

	for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
		// std::cout << std::setprecision(10) << ((z1[i] - z1Gold[i]) / z1Gold[i]) << std::endl;
		// std::cout << std::setprecision(10) << ((z2[i] - z2Gold[i]) / z2Gold[i]) << std::endl;
		// std::cout << std::setprecision(10) << ((ddu[i] - dduGold[i]) / dduGold[i]) << std::endl;
	}

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete ddu;
	delete z2;
	delete z1;
	delete dduGold;
	delete z2Gold;
	delete z1Gold;
	delete ouSingleAveragesInput;
	delete ouInput;
	delete z2Init;
	delete z1Init;
}

void testAveragedHeun() {
	cudaStream_t stream;
	TYPE *z1Init, *z2Init, *ouInput, *ouSingleAveragesInput, *ouDoubleAveragesInput, *z1Gold, *z2Gold, *dduGold;
	TYPE *z1, *z2, *ddu;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))

	z1Init = new TYPE[DEGREE_OF_SOLVER_PARALLELISM];
	z2Init = new TYPE[DEGREE_OF_SOLVER_PARALLELISM];
	ouInput = new TYPE[NUM_OF_TOTAL_FINE_STEPS];
	ouSingleAveragesInput = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	ouDoubleAveragesInput = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z1Gold = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z2Gold = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	dduGold = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z1 = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z2 = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	ddu = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];

	const char *ouInputFilename = "ou_gold.dat";
	const char *ouSingleAveragesInputFilename = "singleAverage_gold.dat";
	const char *ouDoubleAveragesInputFilename = "doubleAverage_gold.dat";
	const char *zGoldFilename = "z_gold.dat";
	const char *dduGoldFilename = "ddu_gold.dat";
	std::ifstream ouInputFile(ouInputFilename);
	std::ifstream ouSingleAveragesInputFile(ouSingleAveragesInputFilename);
	std::ifstream ouDoubleAveragesInputFile(ouDoubleAveragesInputFilename);
	std::ifstream zGoldFile(zGoldFilename);
	std::ifstream dduGoldFile(dduGoldFilename);

	for(unsigned int i = 0; i < DEGREE_OF_SOLVER_PARALLELISM; i++) {
		z1Init[i] = z2Init[i] = 0.0;
	}

	ouInputFile >> ouInput[0];
	zGoldFile >> z1Gold[0];
	zGoldFile >> z2Gold[0];
	dduGoldFile >> dduGold[0];
	for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
		for(unsigned int j = 0; j < NUM_OF_FINE_STEPS_PER_COARSE_STEP; j++) {
			ouInputFile >> ouInput[i * NUM_OF_FINE_STEPS_PER_COARSE_STEP + j];
		}

		ouSingleAveragesInputFile >> ouSingleAveragesInput[i];
		ouDoubleAveragesInputFile >> ouDoubleAveragesInput[i];
		zGoldFile >> z1Gold[i];
		zGoldFile >> z2Gold[i];
		dduGoldFile >> dduGold[i];
	}

	dduGoldFile.close();
	zGoldFile.close();
	ouDoubleAveragesInputFile.close();
	ouSingleAveragesInputFile.close();
	ouInputFile.close();

	averagedHeun_H2H<TYPE>(
			z1Init,
			z2Init,
			ouInput,
			NUM_OF_FINE_STEPS_PER_COARSE_STEP,
			ouSingleAveragesInput,
			ouDoubleAveragesInput,
			NUM_OF_TOTAL_COARSE_STEPS,
			DEGREE_OF_SOLVER_PARALLELISM,
			(TYPE) COARSE_STEP_SIZE,
			z1,
			z2,
			ddu,
			stream);

	for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
		// std::cout << std::setprecision(10) << ((z1[i] - z1Gold[i]) / z1Gold[i]) << std::endl;
		// std::cout << std::setprecision(10) << ((z2[i] - z2Gold[i]) / z2Gold[i]) << std::endl;
		// std::cout << std::setprecision(10) << ((ddu[i] - dduGold[i]) / dduGold[i]) << std::endl;
	}

	GPU_ERROR_CHECK(cudaStreamDestroy(stream));

	delete ddu;
	delete z2;
	delete z1;
	delete dduGold;
	delete z2Gold;
	delete z1Gold;
	delete ouDoubleAveragesInput;
	delete ouSingleAveragesInput;
	delete ouInput;
	delete z2Init;
	delete z1Init;
}
