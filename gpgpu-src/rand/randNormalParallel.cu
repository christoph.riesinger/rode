#include "randNormalParallel.cuh"
#include "randUniform.cuh"
#include "randUniformParallel.cuh"

__device__ void randInitNormal(unsigned long long seed, RandStateWallace &wallaceState) {
	if(threadIdx.x == 0) {
		__shared__ curandStateXORWOW_t xorwowState;

		curand_init(seed, ULLONG_MAX, 0ull, &xorwowState);

		for(unsigned int i = 0; i < WALLACE_K; i++) {
			for(unsigned int j = 0; j < WALLACE_L; j++) {
				wallaceState.elements[i * WALLACE_L + j] = curand_normal(&xorwowState);
			}
		}

		wallaceState.numOfRemainingElems = WALLACE_K;
	}

	__syncthreads();
}

template<typename UniformGenerator>
__device__ void _updateWallace(UniformGenerator &uniformState, RandStateWallace &wallaceState) {
	__shared__ unsigned int chunkIdxsPtrs[4];

	if(threadIdx.x == 0) {
		bool chunksOK;
		unsigned int randomness;
		unsigned int permutation1, permutation2, permutation3, permutation4;
		unsigned int chunkIdx1, chunkIdx2, chunkIdx3, chunkIdx4;

		permutation1 = 0;
		permutation2 = 1;
		permutation3 = 2;
		permutation4 = 3;

		do {
			randomness = randUniform(uniformState);
			// randomness = 345170947;
			randomness = randomness >> (31 - 2 * WALLACE_L_LOG);
			chunkIdx1 = randomness & (WALLACE_L - 1);
			randomness = randomness >> WALLACE_L_LOG;
			chunkIdx2 = randomness & (WALLACE_L - 1);
			randomness = randUniform(uniformState);
			// randomness = 6577175;
			randomness = randomness >> (31 - 2 * WALLACE_L_LOG);
			chunkIdx3 = randomness & (WALLACE_L - 1);
			randomness = randomness >> WALLACE_L_LOG;
			chunkIdx4 = randomness & (WALLACE_L - 1);

			if(chunkIdx1 < chunkIdx2)	{
				swap(chunkIdx1, chunkIdx2);
				swap(permutation1, permutation2);
			}
			if(chunkIdx2 < chunkIdx3)	{
				swap(chunkIdx2, chunkIdx3);
				swap(permutation2, permutation3);
			}
			if(chunkIdx3 < chunkIdx4)	{
				swap(chunkIdx3, chunkIdx4);
				swap(permutation3, permutation4);
			}
			if(chunkIdx1 < chunkIdx2)	{
				swap(chunkIdx1, chunkIdx2);
				swap(permutation1, permutation2);
			}
			if(chunkIdx2 < chunkIdx3)	{
				swap(chunkIdx2, chunkIdx3);
				swap(permutation2, permutation3);
			}
			if(chunkIdx1 < chunkIdx2)	{
				swap(chunkIdx1, chunkIdx2);
				swap(permutation1, permutation2);
			}

			chunksOK  = (chunkIdx1 != chunkIdx2);
			chunksOK &= (chunkIdx2 != chunkIdx3);
			chunksOK &= (chunkIdx3 != chunkIdx4);
		} while(!chunksOK);

		chunkIdxsPtrs[permutation1] = permutation1 * WALLACE_L + chunkIdx1;
		chunkIdxsPtrs[permutation2] = permutation2 * WALLACE_L + chunkIdx2;
		chunkIdxsPtrs[permutation3] = permutation3 * WALLACE_L + chunkIdx3;
		chunkIdxsPtrs[permutation4] = permutation4 * WALLACE_L + chunkIdx4;
	}

	__syncthreads();

	// =========================================================================

	float c, p, q, r, s;
	__shared__ float elementsSwap[WALLACE_K * WALLACE_L];

	{
		int idx;

		p = wallaceState.elements[threadIdx.x * WALLACE_K];
		s = wallaceState.elements[threadIdx.x * WALLACE_K + 1];
		q = wallaceState.elements[threadIdx.x * WALLACE_K + 2];
		r = wallaceState.elements[threadIdx.x * WALLACE_K + 3];
		c = 0.5f * (p + q + r + s);
		idx = ((chunkIdxsPtrs[0] + threadIdx.x - 0 * WALLACE_L) & (WALLACE_L - 1)) + 0 * WALLACE_L;
		elementsSwap[idx] = c - p;
		idx = ((chunkIdxsPtrs[1] + threadIdx.x - 1 * WALLACE_L) & (WALLACE_L - 1)) + 1 * WALLACE_L;
		elementsSwap[idx] = c - q;
		idx = ((chunkIdxsPtrs[2] + threadIdx.x - 2 * WALLACE_L) & (WALLACE_L - 1)) + 2 * WALLACE_L;
		elementsSwap[idx] = r - c;
		idx = ((chunkIdxsPtrs[3] + threadIdx.x - 3 * WALLACE_L) & (WALLACE_L - 1)) + 3 * WALLACE_L;
		elementsSwap[idx] = s - c;
	}

	__syncthreads();

	// =========================================================================

	{
		const unsigned int shuffle = (randUniform(uniformState) >> (29 - WALLACE_L_LOG)) & (WALLACE_L * WALLACE_K - 1);
		// const unsigned int shuffle = (108682812 >> (29 - WALLACE_L_LOG)) & (WALLACE_L * WALLACE_K - 1);

		p = elementsSwap[(threadIdx.x * WALLACE_K + 0)^shuffle];
		s = elementsSwap[(threadIdx.x * WALLACE_K + 1)^shuffle];
		q = elementsSwap[(threadIdx.x * WALLACE_K + 2)^shuffle];
		r = elementsSwap[(threadIdx.x * WALLACE_K + 3)^shuffle];
		c = 0.5f * (p + q + r + s);
		wallaceState.elements[0 * WALLACE_L + threadIdx.x] = c - p;
		wallaceState.elements[1 * WALLACE_L + threadIdx.x] = q - c;
		wallaceState.elements[2 * WALLACE_L + threadIdx.x] = c - r;
		wallaceState.elements[3 * WALLACE_L + threadIdx.x] = s - c;
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		wallaceState.numOfRemainingElems = WALLACE_K;
	}

	__syncthreads();
}

template<typename UniformGenerator>
__device__ float randNormal(UniformGenerator &uniformState,	RandStateWallace &wallaceState) {
	float result;

	if(wallaceState.numOfRemainingElems == 0) {
		_updateWallace(uniformState, wallaceState);
	}

	__syncthreads();

	result = wallaceState.elements[(wallaceState.numOfRemainingElems - 1) * blockDim.x + threadIdx.x];

	__syncthreads();

	if(threadIdx.x == 0) {
		wallaceState.numOfRemainingElems--;
	}

	__syncthreads();

	return result;
}

#include "randNormalParallelTemplates"
