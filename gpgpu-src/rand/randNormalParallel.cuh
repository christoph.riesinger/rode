#pragma once

#include "../helper.h"
#include "randHelper.h"

struct RandStateWallace {
	float elements[WALLACE_K * WALLACE_L];		// 4 Bytes * WALLACE_K * WALLACE_L
	unsigned int numOfRemainingElems;			// 4 Bytes
};

__device__ void randInitNormal(unsigned long long seed, RandStateWallace &wallaceState);

template<typename UniformGenerator>
__device__ float randNormal(UniformGenerator &uniformGenerator,	RandStateWallace &wallaceState);
