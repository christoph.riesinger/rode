#include "randNormal.cuh"
#include "randUniform.cuh"
#include "randUniformParallel.cuh"

__device__ void randInitNormal(unsigned long long seed, RandStateBoxMuller &boxMullerState) {
	boxMullerState.numOfRemainingElems = 0u;
}

__device__ void randInitNormal(unsigned long long seed, RandStateInvCDF &invCDFState) {
}

__device__ void randInitNormal(unsigned long long seed, RandStateWichura &wichuraState) {
}

__device__ void randInitNormal(unsigned long long seed, RandStateZiggurat &zigguratState) {
	if(threadIdx.x == 0) {
#if defined ZIGGURAT_WORKAROUND
		RandStateZiggurat localZigguratState;
#endif
		float x_i;

		x_i = ZIGGURAT_R;
#if defined ZIGGURAT_WORKAROUND
		localZigguratState.x[0]                          = 0.0f;
		localZigguratState.x[ZIGGURAT_NUM_OF_STRIPS - 1] = ZIGGURAT_R;
		localZigguratState.x[ZIGGURAT_NUM_OF_STRIPS]     = ZIGGURAT_V / pdf(ZIGGURAT_R);
#else
		zigguratState.x[0]                          = 0.0f;
		zigguratState.x[ZIGGURAT_NUM_OF_STRIPS - 1] = ZIGGURAT_R;
		zigguratState.x[ZIGGURAT_NUM_OF_STRIPS]     = ZIGGURAT_V / pdf(ZIGGURAT_R);
#endif

		for(int i = (ZIGGURAT_NUM_OF_STRIPS - 2); i >= 1; i--) {
			x_i = sqrtf(-2.0f * logf(ZIGGURAT_V / x_i + pdf(x_i)));
#if defined ZIGGURAT_WORKAROUND
			localZigguratState.x[i] = x_i;
#else
			zigguratState.x[i] = x_i;
#endif
		}

#if defined ZIGGURAT_WORKAROUND
		zigguratState = localZigguratState;
#endif
	}

	__syncthreads();
}

template<typename UniformGenerator>
__device__ float randNormal(
		UniformGenerator &uniformState,
		RandStateBoxMuller &boxMullerState) {
	if(boxMullerState.numOfRemainingElems == 0u) {
		float u1, u2, temp1, temp2, value;

		u1 = toFloat1(randUniform(uniformState));
		u2 = toFloat1(randUniform(uniformState));

		temp1 = sqrtf(-2.0f * logf(u1));
		temp2 = 2.0f * M_PI * u2;

		value                      = temp1 * sinf(temp2);
		boxMullerState.elements[0] = temp1 * cosf(temp2);

		boxMullerState.numOfRemainingElems = 1u;

		return value;
	} else {
		boxMullerState.numOfRemainingElems = 0u;

		return boxMullerState.elements[0];
	}
}

template<typename UniformGenerator>
__device__ float randNormal(
		UniformGenerator &uniformState,
		RandStateInvCDF &invCDFState) {
	float result;

	result = normcdfinvf(toFloat1(randUniform(uniformState)));

	return result;
}

template<typename UniformGenerator>
__device__ float _randNormal(
		UniformGenerator &uniformState,
		RandStateWichura &wichuraState) {
	const float a[8] = {3.3871328727963666080e0,
			1.3314166789178437745e+2,
			1.9715909503065514427e+3,
			1.3731693765509461125e+4,
			4.5921953931549871457e+4,
			6.7265770927008700853e+4,
			3.3430575583588128105e+4,
			2.5090809287301226727e+3
	};
	const float b[8] = {0.0,
			4.2313330701600911252e+1,
			6.8718700749205790830e+2,
			5.3941960214247511077e+3,
			2.1213794301586595867e+4,
			3.9307895800092710610e+4,
			2.8729085735721942674e+4,
			5.2264952788528545610e+3
	};
	const float c[8] = {1.42343711074968357734e0,
			4.63033784615654529590e0,
			5.76949722146069140550e0,
			3.64784832476320460504e0,
			1.27045825245236838258e0,
			2.41780725177450611770e-1,
			2.27238449892691845833e-2,
			7.74545014278341407640e-4
	};
	const float d[8] = {0.0,
			2.05319162663775882187e0,
			1.67638483018380384940e0,
			6.89767334985100004550e-1,
			1.48103976427480074590e-1,
			1.51986665636164571966e-2,
			5.47593808499534494600e-4,
			1.05075007164441684324e-9
	};
	const float e[8] = {6.65790464350110377720e0,
			5.46378491116411436990e0,
			1.78482653991729133580e0,
			2.96560571828504891230e-1,
			2.65321895265761230930e-2,
			1.24266094738807843860e-3,
			2.71155556874348757815e-5,
			2.01033439929228813265e-7
	};
	const float f[8] = {0.0,
			5.99832206555887937690e-1,
			1.36929880922735805310e-1,
			1.48753612908506148525e-2,
			7.86869131145613259100e-4,
			1.84631831751005468180e-5,
			1.42151175831644588870e-7,
			2.04426310338993978564e-15
	};

	float p, r, result;

	p = toFloat1(randUniform(uniformState));

	if(fabs(p - 0.5f) <= 0.425f) {
		r = 0.180625f - (p - 0.5f) * (p - 0.5f);
		result = (p - 0.5f) * (((((((a[7] * r + a[6]) * r + a[5]) * r + a[4]) * r + a[3]) * r + a[2]) * r + a[1]) * r + a[0]) /
				(((((((b[7] * r + b[6]) * r + b[5]) * r + b[4]) * r + b[3]) * r + b[2]) * r + b[1]) * r + 1.0);

		return result;
	} else {
		r = sqrtf(-logf(fabs((p < 0.5f) * 1.0f - p)));

		if (r <= 5.0) {
			r -= 1.6;
			result = (((((((c[7] * r + c[6]) * r + c[5]) * r + c[4]) * r + c[3]) * r + c[2]) * r + c[1]) * r + c[0]) /
					(((((((d[7] * r + d[6]) * r + d[5]) * r + d[4]) * r + d[3]) * r + d[2]) * r + d[1]) * r + 1.0);
		} else {
			r -= 5.0;
			result = (((((((e[7] * r + e[6]) * r + e[5]) * r + e[4]) * r + e[3]) * r + e[2]) * r + e[1]) * r + e[0]) /
					(((((((f[7] * r + f[6]) * r + f[5]) * r + f[4]) * r + f[3]) * r + f[2]) * r + f[1]) * r + 1.0);
		}

		result *= sgn(p - 0.5f);

		return result;
	}
}

template<typename UniformGenerator>
__device__ float randNormal(
		UniformGenerator &uniformState,
		RandStateWichura &wichuraState) {
	const float a[8] = {3.3871328727963666080e0,
			1.3314166789178437745e+2,
			1.9715909503065514427e+3,
			1.3731693765509461125e+4,
			4.5921953931549871457e+4,
			6.7265770927008700853e+4,
			3.3430575583588128105e+4,
			2.5090809287301226727e+3
	};
	const float b[8] = {0.0,
			4.2313330701600911252e+1,
			6.8718700749205790830e+2,
			5.3941960214247511077e+3,
			2.1213794301586595867e+4,
			3.9307895800092710610e+4,
			2.8729085735721942674e+4,
			5.2264952788528545610e+3
	};
	const float c[8] = {1.42343711074968357734e0,
			4.63033784615654529590e0,
			5.76949722146069140550e0,
			3.64784832476320460504e0,
			1.27045825245236838258e0,
			2.41780725177450611770e-1,
			2.27238449892691845833e-2,
			7.74545014278341407640e-4
	};
	const float d[8] = {0.0,
			2.05319162663775882187e0,
			1.67638483018380384940e0,
			6.89767334985100004550e-1,
			1.48103976427480074590e-1,
			1.51986665636164571966e-2,
			5.47593808499534494600e-4,
			1.05075007164441684324e-9
	};
	const float e[8] = {6.65790464350110377720e0,
			5.46378491116411436990e0,
			1.78482653991729133580e0,
			2.96560571828504891230e-1,
			2.65321895265761230930e-2,
			1.24266094738807843860e-3,
			2.71155556874348757815e-5,
			2.01033439929228813265e-7
	};
	const float f[8] = {0.0,
			5.99832206555887937690e-1,
			1.36929880922735805310e-1,
			1.48753612908506148525e-2,
			7.86869131145613259100e-4,
			1.84631831751005468180e-5,
			1.42151175831644588870e-7,
			2.04426310338993978564e-15
	};

	float p, r1, r2, result;

	p  = toFloat1(randUniform(uniformState));
	r1 = 0.180625f - (p - 0.5f) * (p - 0.5f);
	r2 = sqrtf(-logf(fabs((p < 0.5f) * 1.0f - p)));
	result = (fabs(p - 0.5f) <= 0.425f) *
				((p - 0.5f) * (((((((a[7] * r1 + a[6]) * r1 + a[5]) * r1 + a[4]) * r1 + a[3]) * r1 + a[2]) * r1 + a[1]) * r1 + a[0]) /
				(((((((b[7] * r1 + b[6]) * r1 + b[5]) * r1 + b[4]) * r1 + b[3]) * r1 + b[2]) * r1 + b[1]) * r1 + 1.0f)) +
			(fabs(p - 0.5f) > 0.425f) *
				((sgn(p - 0.5f)) *
				(((r2 <= 5.0f) *
					(((((((c[7] * (r2 - 1.6f) + c[6]) * (r2 - 1.6f) + c[5]) * (r2 - 1.6f) + c[4]) * (r2 - 1.6f) + c[3]) * (r2 - 1.6f) + c[2]) * (r2 - 1.6f) + c[1]) * (r2 - 1.6f) + c[0]) /
					(((((((d[7] * (r2 - 1.6f) + d[6]) * (r2 - 1.6f) + d[5]) * (r2 - 1.6f) + d[4]) * (r2 - 1.6f) + d[3]) * (r2 - 1.6f) + d[2]) * (r2 - 1.6f) + d[1]) * (r2 - 1.6f) + 1.0f)) +
				((r2 > 5.0f) *
					(((((((e[7] * (r2 - 5.0f) + e[6]) * (r2 - 5.0f) + e[5]) * (r2 - 5.0f) + e[4]) * (r2 - 5.0f) + e[3]) * (r2 - 5.0f) + e[2]) * (r2 - 5.0f) + e[1]) * (r2 - 5.0f) + e[0]) /
					(((((((f[7] * (r2 - 5.0f) + f[6]) * (r2 - 5.0f) + f[5]) * (r2 - 5.0f) + f[4]) * (r2 - 5.0f) + f[3]) * (r2 - 5.0f) + f[2]) * (r2 - 5.0f) + f[1]) * (r2 - 5.0f) + 1.0f))));

	return result;
}

template<typename UniformGenerator>
__device__ float randNormal(
		UniformGenerator &uniformState,
		RandStateZiggurat &zigguratState) {
	unsigned int uniformRN, uniformRNidx;
	float x_i, x_ip1;

	uniformRN    = randUniform(uniformState);
	uniformRNidx = uniformRN & (ZIGGURAT_NUM_OF_STRIPS - 1);
	x_i          = zigguratState.x[uniformRNidx];
	x_ip1        = zigguratState.x[uniformRNidx+1];

	if(toFloat1(uniformRN) < (x_i / x_ip1)) {
		/*
		 * Central region.
		 * Due to clever setup of zigguratState.x, also a part of the base strip
		 * is covered.
		 */
		return (toFloat2(uniformRN) * x_ip1);
	} else {
		float x, y;

		while(true) {
			if(uniformRNidx == (ZIGGURAT_NUM_OF_STRIPS - 1)) {
				/*
				 * Normal tail of base strip.
				 */
				do {
					x = -logf(toFloat1(randUniform(uniformState))) * (1.0f / ZIGGURAT_R);
					y = -logf(toFloat1(randUniform(uniformState)));
				} while(x * x > y + y);

				return ((uniformRN < 0x80000000) ? (ZIGGURAT_R + x) : (-ZIGGURAT_R - x));
			} else {
				x = toFloat2(uniformRN) * x_ip1;

				if(pdf(x_ip1) + toFloat1(randUniform(uniformState)) * (pdf(x_i) - pdf(x_ip1)) < expf(-0.5f * x * x)) {
					/*
					 * Tail region.
					 */
					return x;
				} else {
					/*
					 * Cap region.
					 */
					uniformRN    = randUniform(uniformState);
					uniformRNidx = uniformRN & (ZIGGURAT_NUM_OF_STRIPS - 1);
					x_i          = zigguratState.x[uniformRNidx];
					x_ip1        = zigguratState.x[uniformRNidx+1];

					if(toFloat1(uniformRN) < (x_i / x_ip1)) {
						return (toFloat2(uniformRN) * x_ip1);
					}
				}
			}
		}
	}
}

#include "randNormalTemplates"
