#pragma once

#include "../helper.h"
#include "randHelper.h"

__device__ void randInitUniform(unsigned long long seed, curandStateMRG32k3a_t &mrgState);
__device__ void randInitUniform(unsigned long long seed, curandStatePhilox4_32_10_t &philoxState);
__device__ void randInitUniform(unsigned long long seed, curandStateXORWOW_t &xorwowState);

__device__ unsigned int randUniform(curandStateMRG32k3a_t &mrgState);
__device__ unsigned int randUniform(curandStatePhilox4_32_10_t &philoxState);
__device__ unsigned int randUniform(curandStateXORWOW_t &xorwowState);
