#pragma once

#include "../helper.h"
#include "randHelper.h"
#include "randNormal.cuh"
#include "randNormalParallel.cuh"
#include "randUniform.cuh"
#include "randUniformParallel.cuh"

template<typename UniformGenerator, typename NormalTransformator>
struct UniformSerialNormalSerialConfiguration {
	UniformGenerator uniformStates[RAND_NUM_OF_THREADS];
	NormalTransformator normalStates[RAND_NUM_OF_THREADS];
};

template<typename UniformGenerator, typename NormalTransformator>
struct UniformSerialNormalSharedConfiguration {
	UniformGenerator uniformStates[RAND_NUM_OF_THREADS];
	NormalTransformator normalState;
};

template<typename UniformGenerator, typename NormalGenerator>
struct UniformSerialNormalParallelConfiguration {
	UniformGenerator uniformStates[RAND_NUM_OF_THREADS];
	NormalGenerator normalState;
};

template<typename UniformGenerator, typename NormalTransformator>
struct UniformParallelNormalSerialConfiguration {
	UniformGenerator uniformState;
	NormalTransformator normalStates[RAND_NUM_OF_THREADS];
};

/*
template<typename UniformGenerator, typename NormalTransformator>
struct UniformParallelNormalSharedConfiguration {
	UniformGenerator uniformState;
	NormalTransformator normalState;
};
*/

/*
template<typename UniformGenerator, typename NormalGenerator>
struct UniformParallelNormalParallelConfiguration {
	UniformGenerator uniformState;
	NormalGenerator normalState;
};
*/

template<typename UniformGenerator>
struct UniformSerialConfiguration {
	UniformGenerator uniformStates[RAND_NUM_OF_THREADS];
};

template<typename UniformGenerator>
struct UniformParallelConfiguration {
	UniformGenerator uniformState;
};

template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator>
void initStatesNormal_D(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator, NormalGenerator> *dConfigurations,
		cudaStream_t &stream);
template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator>
void initStatesNormal_H(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator, NormalGenerator> *hConfigurations,
		cudaStream_t &stream);
template<template<typename> class Configuration, typename UniformGenerator>
void initStatesUniform_D(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator> *dConfigurations,
		cudaStream_t &stream);
template<template<typename> class Configuration, typename UniformGenerator>
void initStatesUniform_H(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator> *hConfigurations,
		cudaStream_t &stream);
template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator, typename T>
void getRandomNumbersNormal_D(
		Configuration<UniformGenerator, NormalGenerator> *dConfigurations,
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		T *dNormalRandomNumbers,
		cudaStream_t &stream);
template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator, typename T>
void getRandomNumbersNormal_H(
		Configuration<UniformGenerator, NormalGenerator> *dConfigurations,
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		T *hNormalRandomNumbers,
		cudaStream_t &stream);
template<template<typename> class Configuration, typename UniformGenerator>
void getRandomNumbersUniform_D(
		Configuration<UniformGenerator> *dConfigurations,
		const unsigned long long numOfUniformRandomNumbers,
		const unsigned int numOfUniformRandomNumbersPerThread,
		unsigned int *dUniformRandomNumbers,
		cudaStream_t &stream);
template<template<typename> class Configuration, typename UniformGenerator>
void getRandomNumbersUniform_H(
		Configuration<UniformGenerator> *dConfigurations,
		const unsigned long long numOfUniformRandomNumbers,
		const unsigned int numOfUniformRandomNumbersPerThread,
		unsigned int *hUniformRandomNumbers,
		cudaStream_t &stream);
