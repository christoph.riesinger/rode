#pragma once

#define RAND_NUM_OF_BLOCKS_LOG			10
#define RAND_NUM_OF_BLOCKS				(1<<RAND_NUM_OF_BLOCKS_LOG)

#define RAND_NUM_OF_THREADS_LOG			6
#define RAND_NUM_OF_THREADS				(1<<RAND_NUM_OF_THREADS_LOG)

#define RAND_NUM_OF_NUMBERS_PER_THREAD	(1<<12)

// #define RAND_UNIFORM_LOCAL
#define RAND_UNIFORM_SHARED

#define ZIGGURAT_1024

#if defined ZIGGURAT_2
	#define ZIGGURAT_NUM_OF_STRIPS		2
	#define ZIGGURAT_R					1.3292331281103218
	#define ZIGGURAT_V					7.797780032623924E-01
#elif defined ZIGGURAT_4
	#define ZIGGURAT_NUM_OF_STRIPS		4
	#define ZIGGURAT_R					1.914928263803744
	#define ZIGGURAT_V					3.756758421647678E-01
#elif defined ZIGGURAT_8
	#define ZIGGURAT_NUM_OF_STRIPS		8
	#define ZIGGURAT_R					2.3383716982472524
	#define ZIGGURAT_V					1.7617364011877756E-01
#elif defined ZIGGURAT_16
	#define ZIGGURAT_NUM_OF_STRIPS		16
	#define ZIGGURAT_R					2.675536765737614
	#define ZIGGURAT_V					8.39894637478272E-02
#elif defined ZIGGURAT_32
	#define ZIGGURAT_NUM_OF_STRIPS		32
	#define ZIGGURAT_R					2.961300121264019
	#define ZIGGURAT_V					4.075874443221993E-02
#elif defined ZIGGURAT_64
	#define ZIGGURAT_NUM_OF_STRIPS		64
	#define ZIGGURAT_R					3.213657627158896
	#define ZIGGURAT_V					2.0024457157351665E-02
#elif defined ZIGGURAT_128
	#define ZIGGURAT_NUM_OF_STRIPS		128
	#define ZIGGURAT_R					3.442619855896652
	#define ZIGGURAT_V					9.91256303533647E-03
#elif defined ZIGGURAT_256
	#define ZIGGURAT_NUM_OF_STRIPS		256
	#define ZIGGURAT_R					3.6541528853610084
	#define ZIGGURAT_V					4.92867323397466E-03
#elif defined ZIGGURAT_512
	#define ZIGGURAT_NUM_OF_STRIPS		512
	#define ZIGGURAT_R					3.852046150368391
	#define ZIGGURAT_V					2.4567663515413568E-03
#elif defined ZIGGURAT_1024
	#define ZIGGURAT_NUM_OF_STRIPS		1024
	#define ZIGGURAT_R					4.038849846109505
	#define ZIGGURAT_V					1.2263246463530852E-03
#elif defined ZIGGURAT_2048
	#define ZIGGURAT_NUM_OF_STRIPS		2048
	#define ZIGGURAT_R					4.216370409511898
	#define ZIGGURAT_V					6.126065176240442E-04
#elif defined ZIGGURAT_4096
	#define ZIGGURAT_NUM_OF_STRIPS		4096
	#define ZIGGURAT_R 					4.385945034871305
	#define ZIGGURAT_V					3.061541032784638E-04
#elif defined ZIGGURAT_8192
	#define ZIGGURAT_NUM_OF_STRIPS		8192
	#define ZIGGURAT_R					4.548600609949139
	#define ZIGGURAT_V					1.5303723494629912E-04
#else
	#define ZIGGURAT_NUM_OF_STRIPS		1024
	#define ZIGGURAT_R					4.038849846109505
	#define ZIGGURAT_V					1.2263246463530852E-03
#endif

#define ZIGGURAT_WORKAROUND

#define WALLACE_L_LOG RAND_NUM_OF_THREADS_LOG
#define WALLACE_L     (1 << WALLACE_L_LOG)
#define WALLACE_K     4
