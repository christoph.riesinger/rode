#pragma once

#include "../helper.h"
#include "randHelper.h"

struct RandStatePhilox {
	uint2 key;								// 8 Bytes
	uint4 elements[RAND_NUM_OF_THREADS];	// 4 Bytes * RAND_NUM_OF_THREADS
	unsigned int numOfRemainingElems;		// 4 Bytes
	unsigned long long iteration;			// 8 Bytes
};

__device__ void randInitUniform(unsigned long long seed, RandStatePhilox &philoxState);

__device__ unsigned int randUniform(RandStatePhilox &philoxState);
