#include "randUniform.cuh"

__device__ void randInitUniform(unsigned long long seed, curandStateMRG32k3a_t &mrgState) {
	curand_init(seed, blockIdx.x * blockDim.x + threadIdx.x, 0ull, &mrgState);
}

__device__ void randInitUniform(unsigned long long seed, curandStatePhilox4_32_10_t &philoxState) {
	curand_init(seed, blockIdx.x * blockDim.x + threadIdx.x, 0ull, &philoxState);
}

__device__ void randInitUniform(unsigned long long seed, curandStateXORWOW_t &xorwowState) {
	curand_init(seed, blockIdx.x * blockDim.x + threadIdx.x, 0ull, &xorwowState);
}

__device__ unsigned int randUniform(curandStateMRG32k3a_t &mrgState) {
	return curand(&mrgState);
}

__device__ unsigned int randUniform(curandStatePhilox4_32_10_t &philoxState) {
	return curand(&philoxState);
}

__device__ unsigned int randUniform(curandStateXORWOW_t &xorwowState) {
	return curand(&xorwowState);
}
