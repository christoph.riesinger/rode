#include "randInterface.cuh"
#include "randNormal.cuh"
#include "randNormalParallel.cuh"
#include "randUniform.cuh"
#include "randUniformParallel.cuh"

template<typename UniformGenerator, typename NormalGenerator>
__global__ void initStatesNormalKernel(
		const unsigned long long seed,
		UniformSerialNormalSerialConfiguration<UniformGenerator, NormalGenerator> *configurations) {
	UniformGenerator localUniformState;
	NormalGenerator localNormalState;

	__syncthreads();

	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];
	localNormalState = configurations[blockIdx.x].normalStates[threadIdx.x];

	__syncthreads();

	randInitUniform(seed, localUniformState);
	randInitNormal(seed, localNormalState);

	__syncthreads();

	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;
	configurations[blockIdx.x].normalStates[threadIdx.x] = localNormalState;

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator>
__global__ void initStatesNormalKernel(
		const unsigned long long seed,
		UniformSerialNormalSharedConfiguration<UniformGenerator, NormalGenerator> *configurations) {
	__shared__ NormalGenerator sharedNormalState;
	UniformGenerator localUniformState;

	__syncthreads();

	if(threadIdx.x == 0) {
		sharedNormalState = configurations[blockIdx.x].normalState;
	}
	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];

	__syncthreads();

	randInitUniform(seed, localUniformState);
	randInitNormal(seed, sharedNormalState);

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x].normalState = sharedNormalState;
	}
	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator>
__global__ void initStatesNormalKernel(
		const unsigned long long seed,
		UniformSerialNormalParallelConfiguration<UniformGenerator, NormalGenerator> *configurations) {
	#if defined RAND_UNIFORM_SHARED
	__shared__ UniformSerialNormalParallelConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;
	#elif defined RAND_UNIFORM_LOCAL
	__shared__ NormalGenerator parallelNormalState;
	UniformGenerator localUniformState;
	#else
	__shared__ UniformSerialNormalParallelConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;
	#endif

	__syncthreads();

#if defined RAND_UNIFORM_SHARED
	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}
#elif defined RAND_UNIFORM_LOCAL
	if(threadIdx.x == 0) {
		parallelNormalState = configurations[blockIdx.x].normalState;
	}
	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];
#else
	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}
#endif

	__syncthreads();
#if defined RAND_UNIFORM_SHARED
	randInitUniform(seed, blockConfiguration.uniformStates[threadIdx.x]);
	randInitNormal(seed, blockConfiguration.normalState);
#elif defined RAND_UNIFORM_LOCAL
	randInitUniform(seed, localUniformState);
	randInitNormal(seed, parallelNormalState);
#else

	randInitUniform(seed, blockConfiguration.uniformStates[threadIdx.x]);
	randInitNormal(seed, blockConfiguration.normalState);
#endif

	__syncthreads();

#if defined RAND_UNIFORM_SHARED
	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}
#elif defined RAND_UNIFORM_LOCAL
	if(threadIdx.x == 0) {
		configurations[blockIdx.x].normalState = parallelNormalState;
	}
	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;
#else
	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}
#endif

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator>
__global__ void initStatesNormalKernel(
		const unsigned long long seed,
		UniformParallelNormalSerialConfiguration<UniformGenerator, NormalGenerator> *configurations) {
	__shared__ UniformParallelNormalSerialConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	randInitUniform(seed, blockConfiguration.uniformState);
	randInitNormal(seed, blockConfiguration.normalStates[threadIdx.x]);

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}

/*
template<typename UniformGenerator, typename NormalGenerator>
__global__ void initStatesNormalKernel(
		const unsigned long long seed,
		UniformParallelNormalSharedConfiguration<UniformGenerator, NormalGenerator> *configurations) {
	__shared__ UniformParallelNormalSharedConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	randInitUniform(seed, blockConfiguration.uniformState);
	randInitNormal(seed, blockConfiguration.normalState);

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}
*/

/*
template<typename UniformGenerator, typename NormalGenerator>
__global__ void initStatesNormalKernel(
		const unsigned long long seed,
		UniformParallelNormalParallelConfiguration<UniformGenerator, NormalGenerator> *configurations) {
	__shared__ UniformParallelNormalParallelConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	randInitUniform(seed, blockConfiguration.uniformState);
	randInitNormal(seed, blockConfiguration.normalState);

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}
*/

template<typename UniformGenerator>
__global__ void initStatesUniformKernel(
		const unsigned long long seed,
		UniformSerialConfiguration<UniformGenerator> *configurations) {
	UniformGenerator localUniformState;

	__syncthreads();

	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];

	__syncthreads();

	randInitUniform(seed, localUniformState);

	__syncthreads();

	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;

	__syncthreads();
}

template<typename UniformGenerator>
__global__ void initStatesUniformKernel(
		const unsigned long long seed,
		UniformParallelConfiguration<UniformGenerator> *configurations) {
	__shared__ UniformParallelConfiguration<UniformGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	randInitUniform(seed, blockConfiguration.uniformState);

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator, typename T>
__global__ void getRandomNumbersNormalKernel(
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		UniformSerialNormalSerialConfiguration<UniformGenerator, NormalGenerator> *configurations,
		T *dArray) {
	__shared__ UniformSerialNormalSerialConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	for(unsigned int i = 0; i < numOfNormalRandomNumbersPerThread; i++) {
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
				(T) randNormal<UniformGenerator>(
						blockConfiguration.uniformStates[threadIdx.x],
						blockConfiguration.normalStates[threadIdx.x]);
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator, typename T>
__global__ void getRandomNumbersNormalKernel(
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		UniformSerialNormalSharedConfiguration<UniformGenerator, NormalGenerator> *configurations,
		T *dArray) {
#if defined RAND_UNIFORM_SHARED
	__shared__ UniformSerialNormalSharedConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;
#elif defined RAND_UNIFORM_LOCAL
	__shared__ NormalGenerator sharedNormalState;
	UniformGenerator localUniformState;
#else
	__shared__ UniformSerialNormalSharedConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;
#endif

	__syncthreads();

#if defined RAND_UNIFORM_SHARED
	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}
#elif defined RAND_UNIFORM_LOCAL
	if(threadIdx.x == 0) {
		sharedNormalState = configurations[blockIdx.x].normalState;
	}
	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];
#else
	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}
#endif

	__syncthreads();

	for(unsigned int i = 0; i < numOfNormalRandomNumbersPerThread; i++) {
#if defined RAND_UNIFORM_SHARED
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
				(T) randNormal<UniformGenerator>(
						blockConfiguration.uniformStates[threadIdx.x],
						blockConfiguration.normalState);
#elif defined RAND_UNIFORM_LOCAL
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
				(T) randNormal<UniformGenerator>(
						localUniformState,
						sharedNormalState);
#else
	dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
			(T) randNormal<UniformGenerator>(
					blockConfiguration.uniformStates[threadIdx.x],
					blockConfiguration.normalState);
#endif
	}

	__syncthreads();

#if defined RAND_UNIFORM_SHARED
	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}
#elif defined RAND_UNIFORM_LOCAL
	if(threadIdx.x == 0) {
		configurations[blockIdx.x].normalState = sharedNormalState;
	}
	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;
#else
	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}
#endif

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator, typename T>
__global__ void getRandomNumbersNormalKernel(
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		UniformSerialNormalParallelConfiguration<UniformGenerator, NormalGenerator> *configurations,
		T *dArray) {
	#if defined RAND_UNIFORM_SHARED
	__shared__ UniformSerialNormalParallelConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;
#elif defined RAND_UNIFORM_LOCAL
	__shared__ NormalGenerator parallelNormalState;
	UniformGenerator localUniformState;
#else
	__shared__ UniformSerialNormalParallelConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;
#endif

	__syncthreads();

#if defined RAND_UNIFORM_SHARED
	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}
#elif defined RAND_UNIFORM_LOCAL
	if(threadIdx.x == 0) {
		parallelNormalState = configurations[blockIdx.x].normalState;
	}
	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];
#else
	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}
#endif

	__syncthreads();

	for(unsigned int i = 0; i < numOfNormalRandomNumbersPerThread; i++) {
#if defined RAND_UNIFORM_SHARED
		dArray[blockIdx.x * numOfNormalRandomNumbersPerThread * blockDim.x + i * blockDim.x + threadIdx.x] =
				(T) randNormal<UniformGenerator>(
						blockConfiguration.uniformStates[threadIdx.x],
						// *((UniformGenerator*) blockConfiguration.normalState.elements),
						blockConfiguration.normalState);
#elif defined RAND_UNIFORM_LOCAL
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
				(T) randNormal<UniformGenerator>(
						localUniformState,
						// *((UniformGenerator*) blockConfiguration.normalState.elements),
						parallelNormalState);
#else
		dArray[blockIdx.x * numOfNormalRandomNumbersPerThread * blockDim.x + i * blockDim.x + threadIdx.x] =
				(T) randNormal<UniformGenerator>(
						blockConfiguration.uniformStates[threadIdx.x],
						// *((UniformGenerator*) blockConfiguration.normalState.elements),
						blockConfiguration.normalState);
#endif
	}

	__syncthreads();

#if defined RAND_UNIFORM_SHARED
	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}
#elif defined RAND_UNIFORM_LOCAL
	if(threadIdx.x == 0) {
		configurations[blockIdx.x].normalState = parallelNormalState;
	}
	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;
#else
	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}
#endif

	__syncthreads();
}

template<typename UniformGenerator, typename NormalGenerator, typename T>
__global__ void getRandomNumbersNormalKernel(
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		UniformParallelNormalSerialConfiguration<UniformGenerator, NormalGenerator> *configurations,
		T *dArray) {
	__shared__ UniformParallelNormalSerialConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	for(unsigned int i = 0; i < numOfNormalRandomNumbersPerThread; i++) {
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
				(T) randNormal<UniformGenerator>(
						blockConfiguration.uniformState,
						blockConfiguration.normalStates[threadIdx.x]);
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}

/*
template<typename UniformGenerator, typename NormalGenerator, typename T>
__global__ void getRandomNumbersNormalKernel(
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		UniformParallelNormalSharedConfiguration<UniformGenerator, NormalGenerator> *configurations,
		T *dArray) {
	__shared__ UniformParallelNormalSharedConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	for(unsigned int i = 0; i < numOfNormalRandomNumbersPerThread; i++) {
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfNormalRandomNumbersPerThread + i] =
				randNormal<UniformGenerator>(
						blockConfiguration.uniformState,
						blockConfiguration.normalState);
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}
*/

/*
template<typename UniformGenerator, typename NormalGenerator, typename T>
__global__ void getRandomNumbersNormalKernel(
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		UniformParallelNormalParallelConfiguration<UniformGenerator, NormalGenerator> *configurations,
		T *dArray) {
	__shared__ UniformParallelNormalParallelConfiguration<UniformGenerator, NormalGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	for(unsigned int i = 0; i < numOfNormalRandomNumbersPerThread; i++) {
		dArray[blockIdx.x * numOfNormalRandomNumbersPerThread * blockDim.x + i * blockDim.x + threadIdx.x] =
				randNormal<UniformGenerator>(
						blockConfiguration.uniformState,
						blockConfiguration.normalState);
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}
*/

template<typename UniformGenerator>
__global__ void getRandomNumbersUniformKernel(
		const unsigned long long numOfUniformRandomNumbers,
		const unsigned int numOfUniformRandomNumbersPerThread,
		UniformSerialConfiguration<UniformGenerator> *configurations,
		unsigned int *dArray) {
	UniformGenerator localUniformState;

	__syncthreads();

	localUniformState = configurations[blockIdx.x].uniformStates[threadIdx.x];

	__syncthreads();

	for(unsigned int i = 0; i < numOfUniformRandomNumbersPerThread; i++) {
		dArray[(blockIdx.x * blockDim.x + threadIdx.x) * numOfUniformRandomNumbersPerThread + i] =
				randUniform(localUniformState);
	}

	__syncthreads();

	configurations[blockIdx.x].uniformStates[threadIdx.x] = localUniformState;

	__syncthreads();
}

template<typename UniformGenerator>
__global__ void getRandomNumbersUniformKernel(
		const unsigned long long numOfUniformRandomNumbers,
		const unsigned int numOfUniformRandomNumbersPerThread,
		UniformParallelConfiguration<UniformGenerator> *configurations,
		unsigned int *dArray) {
	__shared__ UniformParallelConfiguration<UniformGenerator> blockConfiguration;

	__syncthreads();

	if(threadIdx.x == 0) {
		blockConfiguration = configurations[blockIdx.x];
	}

	__syncthreads();

	for(unsigned int i = 0; i < numOfUniformRandomNumbersPerThread; i++) {
		dArray[blockIdx.x * numOfUniformRandomNumbersPerThread * blockDim.x + i * blockDim.x + threadIdx.x] =
				randUniform(blockConfiguration.uniformState);
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		configurations[blockIdx.x] = blockConfiguration;
	}

	__syncthreads();
}

template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator>
void initStatesNormal_D(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator, NormalGenerator> *dConfigurations,
		cudaStream_t &stream) {
	initStatesNormalKernel<UniformGenerator, NormalGenerator><<<numOfConfigurations, RAND_NUM_OF_THREADS, 0, stream>>>(
			seed,
			dConfigurations);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
}

template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator>
void initStatesNormal_H(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator, NormalGenerator> *hConfigurations,
		cudaStream_t &stream) {
	Configuration<UniformGenerator, NormalGenerator> *dConfigurations;

	GPU_ERROR_CHECK(cudaMalloc(&dConfigurations, numOfConfigurations * sizeof(Configuration<UniformGenerator, NormalGenerator>)))

	initStatesNormal_D<Configuration, UniformGenerator, NormalGenerator>(
			seed,
			numOfConfigurations,
			dConfigurations,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(hConfigurations, dConfigurations, numOfConfigurations * sizeof(Configuration<UniformGenerator, NormalGenerator>), cudaMemcpyDeviceToHost, stream))

	GPU_ERROR_CHECK(cudaFree(dConfigurations))
}

template<template<typename> class Configuration, typename UniformGenerator>
void initStatesUniform_D(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator> *dConfigurations,
		cudaStream_t &stream) {
	initStatesUniformKernel<UniformGenerator><<<numOfConfigurations, RAND_NUM_OF_THREADS, 0, stream>>>(
			seed,
			dConfigurations);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
}

template<template<typename> class Configuration, typename UniformGenerator>
void initStatesUniform_H(
		const unsigned long long seed,
		const unsigned int numOfConfigurations,
		Configuration<UniformGenerator> *hConfigurations,
		cudaStream_t &stream) {
	Configuration<UniformGenerator> *dConfigurations;

	GPU_ERROR_CHECK(cudaMalloc(&dConfigurations, numOfConfigurations * sizeof(Configuration<UniformGenerator>)))

	initStatesUniform_D<Configuration, UniformGenerator>(
			seed,
			numOfConfigurations,
			dConfigurations,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(hConfigurations, dConfigurations, numOfConfigurations * sizeof(Configuration<UniformGenerator>), cudaMemcpyDeviceToHost, stream))

	GPU_ERROR_CHECK(cudaFree(dConfigurations))
}

template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator, typename T>
void getRandomNumbersNormal_D(
		Configuration<UniformGenerator, NormalGenerator> *dConfigurations,
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		T *dNormalRandomNumbers,
		cudaStream_t &stream) {
	getRandomNumbersNormalKernel<UniformGenerator, NormalGenerator, T><<<divUp(numOfNormalRandomNumbers, RAND_NUM_OF_THREADS * numOfNormalRandomNumbersPerThread), RAND_NUM_OF_THREADS, 0, stream>>>(
			numOfNormalRandomNumbers,
			numOfNormalRandomNumbersPerThread,
			dConfigurations,
			dNormalRandomNumbers);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
}

template<template<typename, typename> class Configuration, typename UniformGenerator, typename NormalGenerator, typename T>
void getRandomNumbersNormal_H(
		Configuration<UniformGenerator, NormalGenerator> *dConfigurations,
		const unsigned long long numOfNormalRandomNumbers,
		const unsigned int numOfNormalRandomNumbersPerThread,
		T *hNormalRandomNumbers,
		cudaStream_t &stream) {
	T *dNormalRandomNumbers;

	GPU_ERROR_CHECK(cudaMalloc(&dNormalRandomNumbers, numOfNormalRandomNumbers * sizeof(T)))

	getRandomNumbersNormal_D<Configuration, UniformGenerator, NormalGenerator, T>(
			dConfigurations,
			numOfNormalRandomNumbers,
			numOfNormalRandomNumbersPerThread,
			dNormalRandomNumbers,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(hNormalRandomNumbers, dNormalRandomNumbers, numOfNormalRandomNumbers * sizeof(T), cudaMemcpyDeviceToHost, stream))

	GPU_ERROR_CHECK(cudaFree(dNormalRandomNumbers))
}

template<template<typename> class Configuration, typename UniformGenerator>
void getRandomNumbersUniform_D(
		Configuration<UniformGenerator> *dConfigurations,
		const unsigned long long numOfUniformRandomNumbers,
		const unsigned int numOfUniformRandomNumbersPerThread,
		unsigned int *dUniformRandomNumbers,
		cudaStream_t &stream) {
	getRandomNumbersUniformKernel<UniformGenerator><<<divUp(numOfUniformRandomNumbers, RAND_NUM_OF_THREADS * numOfUniformRandomNumbersPerThread), RAND_NUM_OF_THREADS, 0, stream>>>(
			numOfUniformRandomNumbers,
			numOfUniformRandomNumbersPerThread,
			dConfigurations,
			dUniformRandomNumbers);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
}

template<template<typename> class Configuration, typename UniformGenerator>
void getRandomNumbersUniform_H(
		Configuration<UniformGenerator> *dConfigurations,
		const unsigned long long numOfUniformRandomNumbers,
		const unsigned int numOfUniformRandomNumbersPerThread,
		unsigned int *hUniformRandomNumbers,
		cudaStream_t &stream) {
	unsigned int *dUniformRandomNumbers;

	GPU_ERROR_CHECK(cudaMalloc(&dUniformRandomNumbers, numOfUniformRandomNumbers * sizeof(unsigned int)))

	getRandomNumbersUniform_D<Configuration, UniformGenerator>(
			dConfigurations,
			numOfUniformRandomNumbers,
			numOfUniformRandomNumbersPerThread,
			dUniformRandomNumbers,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(hUniformRandomNumbers, dUniformRandomNumbers, numOfUniformRandomNumbers * sizeof(unsigned int), cudaMemcpyDeviceToHost, stream))

	GPU_ERROR_CHECK(cudaFree(dUniformRandomNumbers))
}

#include "randInterfaceTemplates"
