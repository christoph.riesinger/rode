#include "randUniformParallel.cuh"

__device__ void randInitUniform(unsigned long long seed, RandStatePhilox &philoxState) {
	if(threadIdx.x == 0) {
		unsigned long long key;

		key = seed + blockIdx.x;
		philoxState.key.x = (unsigned int) key;
		philoxState.key.y = (unsigned int) (key>>32);
		philoxState.numOfRemainingElems = 0u;
		philoxState.iteration = 0ull;
	}

	__syncthreads();
}

__device__ uint4 _getPhiloxCtr(RandStatePhilox &philoxState) {
	uint4 ctr;
	unsigned long long hi, lo;

	mult(philoxState.iteration, blockDim.x, hi, lo);
	add(hi, lo, threadIdx.x, hi, lo);

	ctr.x = (unsigned int) lo;
    ctr.y = (unsigned int) (lo>>32);
    ctr.z = (unsigned int) hi;
    ctr.w = (unsigned int) (hi>>32);

    return ctr;
}

__device__ void _updatePhilox(RandStatePhilox &philoxState) {
	uint4 ctr;

	ctr = _getPhiloxCtr(philoxState);
	philoxState.elements[threadIdx.x] = curand_Philox4x32_10(ctr, philoxState.key);

	__syncthreads();

	if(threadIdx.x == 0) {
		philoxState.iteration++;
		philoxState.numOfRemainingElems = 4;
	}

	__syncthreads();
}

__device__ unsigned int randUniform(RandStatePhilox &philoxState) {
	unsigned int result;

	if(philoxState.numOfRemainingElems == 0) {
		_updatePhilox(philoxState);
	}

	__syncthreads();

	switch(philoxState.numOfRemainingElems) {
	case 4:
		result = philoxState.elements[threadIdx.x].x;
		break;
	case 3:
		result = philoxState.elements[threadIdx.x].y;
		break;
	case 2:
		result = philoxState.elements[threadIdx.x].z;
		break;
	case 1:
		result = philoxState.elements[threadIdx.x].w;
		break;
	default:
		result = 0u;
	}

	__syncthreads();

	if(threadIdx.x == 0) {
		philoxState.numOfRemainingElems--;
	}

	__syncthreads();

	return result;
}
