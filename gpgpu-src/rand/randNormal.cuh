#pragma once

#include "../helper.h"
#include "randHelper.h"

struct RandStateBoxMuller {
	float elements[1];
	int numOfRemainingElems;
};

struct RandStateInvCDF {
};

struct RandStateWichura {
};

struct RandStateZiggurat {
	float x[ZIGGURAT_NUM_OF_STRIPS+1];
};

__host__ __device__ inline float pdf(float x) {
	return (expf(-0.5f * x * x));
}

__device__ void randInitNormal(unsigned long long seed, RandStateBoxMuller &boxMullerState);
__device__ void randInitNormal(unsigned long long seed, RandStateInvCDF &invCDFState);
__device__ void randInitNormal(unsigned long long seed, RandStateWichura &wichuraState);
__device__ void randInitNormal(unsigned long long seed, RandStateZiggurat &zigguratState);

template<typename UniformGenerator>
__device__ float randNormal(UniformGenerator &uniformState, RandStateBoxMuller &boxMullerState);
template<typename UniformGenerator>
__device__ float randNormal(UniformGenerator &uniformState, RandStateInvCDF &invCDFState);
template<typename UniformGenerator>
__device__ float randNormal(UniformGenerator &uniformState, RandStateWichura &wichuraState);
template<typename UniformGenerator>
__device__ float randNormal(UniformGenerator &uniformState, RandStateZiggurat &zigguratState);
