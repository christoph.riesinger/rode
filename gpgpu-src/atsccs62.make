################################################################################
# paths, directories and folders
################################################################################

CUDAINSTALLPATH	:=	/usr

CCLIBDIR			:= 
CXXLIBDIR			:= 
CUDALIBDIR			:=	

CCINCLUDES			:=	
CXXINCLUDES			:=	
CUDAINCLUDES		:=	

CCLIB				:=	-lgomp
CXXLIB				:=	-lgomp
CUDALIB				:=	

COMPUTE_CAPABILITY	:=	50

################################################################################
# compilers and linkers
################################################################################

CC					:=	gcc-4.9
CXX					:=	g++-4.9
NVCC				:=	$(CUDAINSTALLPATH)/bin/nvcc
LINKER				:=	g++-4.9
NVCCLINKER			:=	$(CUDAINSTALLPATH)/bin/nvcc

################################################################################
# compiler arguments and flags
################################################################################

CCFLAGS				:=	-O3 \
						-fopenmp
CXXFLAGS			:=	-O3 \
						-fopenmp

# arch: specifies the compatibility from source code to PTX stage. Can be a
#       virtual (compute_*) or real (sm_*) compatibility.
# code: specifies the compatibility from PTX stage to binary code. Can only be
#       real (sm_*). Code has to be >= arch.
# -rdc: -rdc is short for --relocatable-device-code which generates relocatable
#       device code. This is necessary to generate multiple CUDA object files
#       which can then be linked together.
NVCCFLAGS			:=	-ccbin=$(CXX) \
						-O3 \
						-gencode arch=compute_$(COMPUTE_CAPABILITY),code=sm_$(COMPUTE_CAPABILITY) \

################################################################################
# linker arguments and flags
################################################################################

LINKERFLAGS			:=	

# -dlink: Necessary linker option to link multiple CUDA object files together.
NVCCLINKERFLAGS		:=	-arch=sm_$(COMPUTE_CAPABILITY)

include common.make
