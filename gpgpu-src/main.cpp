#include <limits>
#include <typeinfo>

#include "ou/ouInterface.cuh"
#include "rand/randInterface.cuh"
#include "solver/solverInterface.cuh"
#include "tests.h"

template<typename T>
inline void computeExpected(const unsigned int numOfTotalCoarseSteps, const unsigned int numOfMonteCarloSamplesPerRank, const unsigned int numOfRanks, T *gathered, T *expected) {
#pragma omp parallel for default(none) firstprivate(numOfTotalCoarseSteps, numOfMonteCarloSamplesPerRank, numOfRanks) shared(gathered, expected)
	for(unsigned int coarseStep = 0; coarseStep < numOfTotalCoarseSteps; coarseStep++) {
		T accumulator = (T)0;
		unsigned int index = coarseStep;

		for(unsigned int path = 0; path < numOfRanks * numOfMonteCarloSamplesPerRank; path++) {
			accumulator += gathered[index];
			index += numOfTotalCoarseSteps;
		}

		expected[coarseStep] = accumulator / (numOfRanks * numOfMonteCarloSamplesPerRank);
	}
}

template<typename T>
inline void computeVariance(const unsigned int numOfTotalCoarseSteps, const unsigned int numOfMonteCarloSamplesPerRank, const unsigned int numOfRanks, T *gathered, T *expected, T *variance) {
#pragma omp parallel for default(none) firstprivate(numOfTotalCoarseSteps, numOfMonteCarloSamplesPerRank, numOfRanks) shared(gathered, expected, variance)
	for(unsigned int coarseStep = 0; coarseStep < numOfTotalCoarseSteps; coarseStep++) {
		T difference;
		T accumulator = (T)0;
		T expectedCurrent = expected[coarseStep];
		unsigned int index = coarseStep;

		for(unsigned int path = 0; path < numOfRanks * numOfMonteCarloSamplesPerRank; path++) {
			difference = gathered[index] - expectedCurrent;
			accumulator += difference * difference;
			index += numOfTotalCoarseSteps;
		}

		variance[coarseStep] = accumulator / (numOfRanks * numOfMonteCarloSamplesPerRank);
	}
}

int main(int argc, char **argv) {
	// printConstants();
	// testRand();
	// testScan();
	// testOU();
	// testSingleAverage();
	// testDoubleAverage();
	// testTridiagIntegralApproximation();
	// testAveragedEuler();
	// testAveragedHeun();
	// testTridiagIntegralApproximation();

#if 1
#ifdef USE_MPI
	int rank, numOfRanks, nodeNameLength;
	char nodeName[MPI_MAX_PROCESSOR_NAME];
    int numOfGPUsPerNode;
#endif
	cudaStream_t stream;
	UniformSerialNormalSharedConfiguration<curandStateXORWOW_t, RandStateZiggurat> *dConfigurations;
	TYPE *dRnOu, *dOuSingleAverages, *dOuLineIntegrals, *dOuTriaIntegrals, *dOuTetrahIntegrals, *dZ1Init, *dZ2Init, *dZ1, *dZ2, *dDdu;
	TYPE hZ1Init, hZ2Init;
	TYPE *hZ1, *hZ2, *hDdu;
	TYPE *z1Gathered, *z2Gathered, *dduGathered;
	TYPE *z1Expected, *z2Expected, *dduExpected;
	TYPE *z1Variance, *z2Variance, *dduVariance;

	/*
	 * Set up the MPI environment and select the correct GPU (if using MPI,this
	 * is done according to the local rank number).
	 */
#ifdef USE_MPI
	MPI_Init(&argc, &argv);
	nodeNameLength = MPI_MAX_PROCESSOR_NAME;
	MPI_Comm_size(MPI_COMM_WORLD, &numOfRanks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Get_processor_name(nodeName, &nodeNameLength);
    GPU_ERROR_CHECK(cudaGetDeviceCount(&numOfGPUsPerNode))
    GPU_ERROR_CHECK(cudaSetDevice(rank % numOfGPUsPerNode))
#else
	GPU_ERROR_CHECK(cudaSetDevice(0))
#endif
	/*
	 * Allocate host and device memory for calculations.
	 */
	GPU_ERROR_CHECK(cudaStreamCreate(&stream))
	GPU_ERROR_CHECK(cudaMalloc(&dConfigurations, divUp(NUM_OF_TOTAL_FINE_STEPS, RAND_NUM_OF_NUMBERS_PER_THREAD * RAND_NUM_OF_THREADS) * sizeof(UniformSerialNormalSharedConfiguration<curandStateXORWOW_t, RandStateZiggurat>)))
	GPU_ERROR_CHECK(cudaMalloc(&dRnOu, NUM_OF_TOTAL_FINE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuSingleAverages, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuLineIntegrals, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuTriaIntegrals, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuTetrahIntegrals, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ1Init, sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2Init, sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ1, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	GPU_ERROR_CHECK(cudaMalloc(&dDdu, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE)))
	hZ1Init = hZ2Init = (TYPE) 0;
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ1Init,
			&hZ1Init,
			sizeof(TYPE),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ2Init,
			&hZ2Init,
			sizeof(TYPE),
			cudaMemcpyHostToDevice,
			stream))
	hZ1 = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	hZ2 = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	hDdu = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];

#ifdef USE_MPI
	if(rank == 0) {
		z1Gathered = new TYPE[numOfRanks * NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK * NUM_OF_TOTAL_COARSE_STEPS];
		z2Gathered = new TYPE[numOfRanks * NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK * NUM_OF_TOTAL_COARSE_STEPS];
		dduGathered = new TYPE[numOfRanks * NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK * NUM_OF_TOTAL_COARSE_STEPS];
	}
#else
	z1Gathered = new TYPE[NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK * NUM_OF_TOTAL_COARSE_STEPS];
	z2Gathered = new TYPE[NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK * NUM_OF_TOTAL_COARSE_STEPS];
	dduGathered = new TYPE[NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK * NUM_OF_TOTAL_COARSE_STEPS];
#endif

	z1Expected = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z2Expected = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	dduExpected = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z1Variance = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	z2Variance = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];
	dduVariance = new TYPE[NUM_OF_TOTAL_COARSE_STEPS];

	/*
	 * Execute all computations on the GPU.
	 */
	for(unsigned int sample = 0; sample < NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK; sample++) {
		initStatesNormal_D<UniformSerialNormalSharedConfiguration, curandStateXORWOW_t, RandStateZiggurat>(
#ifdef USE_MPI
				23421337ull + (sample * numOfRanks) + rank,
#else
				5678ull + sample,
#endif
				divUp(NUM_OF_TOTAL_FINE_STEPS, RAND_NUM_OF_NUMBERS_PER_THREAD * RAND_NUM_OF_THREADS),
				dConfigurations,
				stream);
		getRandomNumbersNormal_D<UniformSerialNormalSharedConfiguration, curandStateXORWOW_t, RandStateZiggurat, TYPE>(
				dConfigurations,
				NUM_OF_TOTAL_FINE_STEPS,
				RAND_NUM_OF_NUMBERS_PER_THREAD,
				dRnOu,
				stream);
		realizeOUProcess_D2D<TYPE>(
				dRnOu,
				(TYPE) 0,
				(TYPE) FINE_STEP_SIZE,
				NUM_OF_TOTAL_FINE_STEPS,
				dRnOu,
				stream);
		/*
		singleAverage_D2D<TYPE>(
				dRnOu,
				NUM_OF_TOTAL_FINE_STEPS,
				NUM_OF_FINE_STEPS_PER_COARSE_STEP,
				NUM_OF_TOTAL_COARSE_STEPS,
				dOuSingleAverages,
				stream);
		averagedEuler_D2D<TYPE>(
				dZ1Init,
				dZ2Init,
				dRnOu,
				NUM_OF_FINE_STEPS_PER_COARSE_STEP,
				dOuSingleAverages,
				NUM_OF_TOTAL_COARSE_STEPS,
				DEGREE_OF_SOLVER_PARALLELISM,
				COARSE_STEP_SIZE,
				dZ1,
				dZ2,
				dDdu,
				stream);
		*/
		tridiagIntegralApproximation_D2D<TYPE>(
				dRnOu,
				(TYPE) 0,
				NUM_OF_TOTAL_FINE_STEPS,
				NUM_OF_FINE_STEPS_PER_COARSE_STEP,
				NUM_OF_TOTAL_COARSE_STEPS,
				1,
				COARSE_STEP_SIZE,
				FINE_STEP_SIZE,
				dOuLineIntegrals,
				stream);
		tridiagIntegralApproximation_D2D<TYPE>(
				dRnOu,
				(TYPE) 0,
				NUM_OF_TOTAL_FINE_STEPS,
				NUM_OF_FINE_STEPS_PER_COARSE_STEP,
				NUM_OF_TOTAL_COARSE_STEPS,
				2,
				COARSE_STEP_SIZE,
				FINE_STEP_SIZE,
				dOuTriaIntegrals,
				stream);
		tridiagIntegralApproximation_D2D<TYPE>(
				dRnOu,
				(TYPE) 0,
				NUM_OF_TOTAL_FINE_STEPS,
				NUM_OF_FINE_STEPS_PER_COARSE_STEP,
				NUM_OF_TOTAL_COARSE_STEPS,
				3,
				COARSE_STEP_SIZE,
				FINE_STEP_SIZE,
				dOuTetrahIntegrals,
				stream);
		taylor3_D2D<TYPE>(
				dZ1Init,
				dZ2Init,
				dRnOu,
				NUM_OF_FINE_STEPS_PER_COARSE_STEP,
				dOuLineIntegrals,
				dOuTriaIntegrals,
				dOuTetrahIntegrals,
				NUM_OF_TOTAL_COARSE_STEPS,
				DEGREE_OF_SOLVER_PARALLELISM,
				COARSE_STEP_SIZE,
				dZ1,
				dZ2,
				dDdu,
				stream);

		/*
		 * Copy results from the device back to the host.
		 */
		GPU_ERROR_CHECK(cudaMemcpyAsync(
				hZ1,
				dZ1,
				NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE),
				cudaMemcpyDeviceToHost,
				stream))
		GPU_ERROR_CHECK(cudaMemcpyAsync(
				hZ2,
				dZ2,
				NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE),
				cudaMemcpyDeviceToHost,
				stream))
		GPU_ERROR_CHECK(cudaMemcpyAsync(
				hDdu,
				dDdu,
				NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE),
				cudaMemcpyDeviceToHost,
				stream))
#ifdef USE_MPI
		/*
		 * Send results from every rank to bootstrap node.
		 */
		MPI_Gather(
				hZ1,
				NUM_OF_TOTAL_COARSE_STEPS,
				((typeid(TYPE) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
				&z1Gathered[sample * numOfRanks * NUM_OF_TOTAL_COARSE_STEPS],
				NUM_OF_TOTAL_COARSE_STEPS,
				((typeid(TYPE) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
				0,
				MPI_COMM_WORLD);
		MPI_Gather(
				hZ2,
				NUM_OF_TOTAL_COARSE_STEPS,
				((typeid(TYPE) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
				&z2Gathered[sample * numOfRanks * NUM_OF_TOTAL_COARSE_STEPS],
				NUM_OF_TOTAL_COARSE_STEPS,
				((typeid(TYPE) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
				0,
				MPI_COMM_WORLD);
		MPI_Gather(
				hDdu,
				NUM_OF_TOTAL_COARSE_STEPS,
				((typeid(TYPE) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
				&dduGathered[sample * numOfRanks * NUM_OF_TOTAL_COARSE_STEPS],
				NUM_OF_TOTAL_COARSE_STEPS,
				((typeid(TYPE) == typeid(float)) ? MPI_FLOAT : MPI_DOUBLE),
				0,
				MPI_COMM_WORLD);
#else
		memcpy(&z1Gathered[sample * NUM_OF_TOTAL_COARSE_STEPS], hZ1, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE));
		memcpy(&z2Gathered[sample * NUM_OF_TOTAL_COARSE_STEPS], hZ2, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE));
		memcpy(&dduGathered[sample * NUM_OF_TOTAL_COARSE_STEPS], hDdu, NUM_OF_TOTAL_COARSE_STEPS * sizeof(TYPE));
#endif
	}

	/*
	 * Compute variance and expected value.
	 */
#ifdef USE_MPI
	if(rank == 0) {
		computeExpected(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, numOfRanks, z1Gathered, z1Expected);
		computeExpected(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, numOfRanks, z2Gathered, z2Expected);
		computeExpected(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, numOfRanks, dduGathered, dduExpected);
		computeVariance(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, numOfRanks, z1Gathered, z1Expected, z1Variance);
		computeVariance(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, numOfRanks, z2Gathered, z2Expected, z2Variance);
		computeVariance(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, numOfRanks, dduGathered, dduExpected, dduVariance);
	}
#else
	computeExpected(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, 1, z1Gathered, z1Expected);
	computeExpected(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, 1, z2Gathered, z2Expected);
	computeExpected(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, 1, dduGathered, dduExpected);
	computeVariance(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, 1, z1Gathered, z1Expected, z1Variance);
	computeVariance(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, 1, z2Gathered, z2Expected, z2Variance);
	computeVariance(NUM_OF_TOTAL_COARSE_STEPS, NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK, 1, dduGathered, dduExpected, dduVariance);
#endif

	/*
	 * Print results.
	 */
#ifdef USE_MPI
	if(rank == 0) {
#endif
		/*
		for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
			std::cout << std::fixed << std::setprecision(std::numeric_limits<TYPE>::digits10 + 1) << z1Expected[i] << std::endl;
		}
		std::cout << std::endl;
		for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
			std::cout << std::fixed << std::setprecision(std::numeric_limits<TYPE>::digits10 + 1) << z1Variance[i] << std::endl;
		}
		std::cout << std::endl;
		*/
		/*
		for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
			std::cout << std::fixed << std::setprecision(std::numeric_limits<TYPE>::digits10 + 1) << z2Expected[i] << std::endl;
		}
		std::cout << std::endl;
		for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
			std::cout << std::fixed << std::setprecision(std::numeric_limits<TYPE>::digits10 + 1) << z2Variance[i] << std::endl;
		}
		std::cout << std::endl;
		*/
		for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
			std::cout << std::fixed << std::setprecision(std::numeric_limits<TYPE>::digits10 + 1) << dduExpected[i] << std::endl;
		}
		std::cout << std::endl;
		for(unsigned int i = 0; i < NUM_OF_TOTAL_COARSE_STEPS; i++) {
			std::cout << std::fixed << std::setprecision(std::numeric_limits<TYPE>::digits10 + 1) << dduVariance[i] << std::endl;
		}
		std::cout << std::endl;
#ifdef USE_MPI
	}
#endif

	delete dduVariance;
	delete z2Variance;
	delete z1Variance;
	delete dduExpected;
	delete z2Expected;
	delete z1Expected;
#ifdef USE_MPI
	if(rank == 0) {
#endif
		delete dduGathered;
		delete z2Gathered;
		delete z1Gathered;
#ifdef USE_MPI
	}
#endif
	delete hDdu;
	delete hZ2;
	delete hZ1;
	GPU_ERROR_CHECK(cudaFree(dDdu))
	GPU_ERROR_CHECK(cudaFree(dZ2))
	GPU_ERROR_CHECK(cudaFree(dZ1))
	GPU_ERROR_CHECK(cudaFree(dOuTetrahIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuTriaIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuLineIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuSingleAverages))
	GPU_ERROR_CHECK(cudaFree(dRnOu))
	GPU_ERROR_CHECK(cudaFree(dConfigurations))
	GPU_ERROR_CHECK(cudaStreamDestroy(stream));
#ifdef USE_MPI
	MPI_Finalize();
#endif
#endif
}
