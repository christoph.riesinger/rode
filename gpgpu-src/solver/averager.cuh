#pragma once

#include "solverHelper.h"

template<typename T>
__global__ void sumKernel(
		const T *elementsToSum,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerSum,
		T *sums);
template<typename T>
__global__ void singleAverageKernel(
		const T *elementsToAverage,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerSingleAverage,
		const unsigned int N,
		T *averages);
template<typename T>
__global__ void doubleAverageKernel(
		const T *elementsToAverage,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerDoubleAverage,
		const unsigned int N,
		T *averages);
template<typename T>
__global__ void tridiagIntegralApproximationKernel(
		const T *elementsToIntegrate,
		const T *initElements,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerTridiagIntegral,
		const unsigned int d,
		const T coarseStepSize,
		const T fineStepSize,
		T *tridiagIntegrals);
