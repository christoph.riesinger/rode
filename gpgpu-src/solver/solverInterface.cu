#include "solverInterface.cuh"

template<typename T>
void singleAverage_D2D(
		const T *dInput,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerSingleAverage,
		const unsigned int numOfSingleAverages,
		T *dOutput,
		cudaStream_t &stream) {
	assert((AVERAGER_SMEM_PER_BLOCK / sizeof(T)) % (AVERAGER_NUM_OF_THREADS * 2) == 0);

	unsigned int numOfThreads, numOfBlocks;

	if(numOfSingleAverages > NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT) {
		numOfThreads = AVERAGER_NUM_OF_THREADS;
		numOfBlocks = numOfSingleAverages;

		singleAverageKernel<T><<<numOfBlocks, numOfThreads, 0, stream>>>(
				dInput,
				numOfElements,
				numOfElementsPerSingleAverage,
				numOfElementsPerSingleAverage,
				dOutput);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
	} else {
		T *dIntermediate;

		GPU_ERROR_CHECK(cudaMalloc(
				&dIntermediate,
				AVERAGER_SMEM_PER_BLOCK * sizeof(T)))

		for(unsigned int i = 0; i < numOfSingleAverages; i++) {
			numOfThreads = AVERAGER_NUM_OF_THREADS;
			numOfBlocks = NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT;

			singleAverageKernel<T><<<numOfBlocks, numOfThreads, 0, stream>>>(
					&dInput[i * numOfElementsPerSingleAverage],
					min(numOfElements - i * numOfElementsPerSingleAverage, numOfElementsPerSingleAverage),
					divUp(numOfElementsPerSingleAverage, NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT),
					numOfElementsPerSingleAverage,
					dIntermediate);
			GPU_ERROR_CHECK(cudaPeekAtLastError())

			numOfThreads = (NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT>>1);
			numOfBlocks = 1;

			sumKernel<T><<<numOfBlocks, numOfThreads, 0, stream>>>(
					dIntermediate,
					NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT,
					NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT,
					&dOutput[i]);
			GPU_ERROR_CHECK(cudaPeekAtLastError())
		}

		GPU_ERROR_CHECK(cudaFree(dIntermediate))
	}
}

template<typename T>
void singleAverage_H2H(
		const T *hInput,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerSingleAverage,
		const unsigned int numOfSingleAverages,
		T *hOutput,
		cudaStream_t &stream) {
	T *dInput, *dOutput;

	GPU_ERROR_CHECK(cudaMalloc(
			&dInput,
			numOfElements * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(
			&dOutput,
			numOfSingleAverages * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dInput,
			hInput,
			numOfElements * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))

	singleAverage_D2D<T>(
			dInput,
			numOfElements,
			numOfElementsPerSingleAverage,
			numOfSingleAverages,
			dOutput,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hOutput,
			dOutput,
			numOfSingleAverages * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dOutput))
	GPU_ERROR_CHECK(cudaFree(dInput))
}

template<typename T>
void doubleAverage_D2D(
		const T *dInput,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerDoubleAverage,
		const unsigned int numOfDoubleAverages,
		T *dOutput,
		cudaStream_t &stream) {
	assert((AVERAGER_SMEM_PER_BLOCK / sizeof(T)) % (AVERAGER_NUM_OF_THREADS * 2) == 0);

	unsigned int numOfThreads, numOfBlocks;

	if(numOfDoubleAverages > NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT) {

		numOfThreads = AVERAGER_NUM_OF_THREADS;
		numOfBlocks = numOfDoubleAverages;

		doubleAverageKernel<T><<<numOfBlocks, numOfThreads, 0, stream>>>(
				dInput,
				numOfElements,
				numOfElementsPerDoubleAverage,
				numOfElementsPerDoubleAverage,
				dOutput);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
	} else {
		T *dIntermediate;

		GPU_ERROR_CHECK(cudaMalloc(
				&dIntermediate,
				NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT * sizeof(T)))

		for(unsigned int i = 0; i < numOfDoubleAverages; i++) {
			numOfThreads = AVERAGER_NUM_OF_THREADS;
			numOfBlocks = NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT;

			doubleAverageKernel<T><<<numOfBlocks, numOfThreads, 0, stream>>>(
					&dInput[i * numOfElementsPerDoubleAverage],
					min(numOfElements - i * numOfElementsPerDoubleAverage, numOfElementsPerDoubleAverage),
					divUp(numOfElementsPerDoubleAverage, NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT),
					numOfElementsPerDoubleAverage,
					dIntermediate);
			GPU_ERROR_CHECK(cudaPeekAtLastError())

			numOfThreads = (NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT>>1);
			numOfBlocks = 1;

			sumKernel<T><<<numOfBlocks, numOfThreads, 0, stream>>>(
					dIntermediate,
					NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT,
					NUM_OF_AVERAGES_BEFORE_LOG_SERIALIZATION_LIMIT,
					&dOutput[i]);
			GPU_ERROR_CHECK(cudaPeekAtLastError())
		}

		GPU_ERROR_CHECK(cudaFree(dIntermediate))
	}
}

template<typename T>
void doubleAverage_H2H(
		const T *hInput,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerDoubleAverage,
		const unsigned int numOfDoubleAverages,
		T *hOutput,
		cudaStream_t &stream) {
	T *dInput, *dOutput;

	GPU_ERROR_CHECK(cudaMalloc(
			&dInput,
			numOfElements * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(
			&dOutput,
			numOfDoubleAverages * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dInput,
			hInput,
			numOfElements * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))

	doubleAverage_D2D<T>(
			dInput,
			numOfElements,
			numOfElementsPerDoubleAverage,
			numOfDoubleAverages,
			dOutput,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hOutput,
			dOutput,
			numOfDoubleAverages * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dOutput))
	GPU_ERROR_CHECK(cudaFree(dInput))
}

template<typename T>
void tridiagIntegralApproximation_D2D(
		const T *dInput,
		const T initElement,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerTridiagIntegral,
		const unsigned int numOfTridiagIntegrals,
		const unsigned int d,
		const T coarseStepSize,
		const T fineStepSize,
		T *dOutput,
		cudaStream_t &stream) {
	assert((AVERAGER_SMEM_PER_BLOCK / sizeof(T)) % (AVERAGER_NUM_OF_THREADS * 2) == 0);

	T *initElements;

	GPU_ERROR_CHECK(cudaMalloc(
			&initElements,
			numOfTridiagIntegrals * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			&initElements[0],
			&initElement,
			sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			&initElements[1],
			sizeof(T),
			&dInput[numOfElementsPerTridiagIntegral - 1],
			numOfElementsPerTridiagIntegral * sizeof(T),
			sizeof(T),
			numOfTridiagIntegrals - 1,
			cudaMemcpyDeviceToDevice,
			stream))

	tridiagIntegralApproximationKernel<T><<<numOfTridiagIntegrals, AVERAGER_NUM_OF_THREADS, 0, stream>>>(
			dInput,
			initElements,
			numOfElements,
			numOfElementsPerTridiagIntegral,
			d,
			coarseStepSize,
			fineStepSize,
			dOutput);
	GPU_ERROR_CHECK(cudaPeekAtLastError())


	GPU_ERROR_CHECK(cudaFree(initElements))
}

template<typename T>
void tridiagIntegralApproximation_H2H(
		const T *hInput,
		const T initElement,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerTridiagIntegral,
		const unsigned int numOfTridiagIntegrals,
		const unsigned int d,
		const T coarseStepSize,
		const T fineStepSize,
		T *hOutput,
		cudaStream_t &stream) {
	T *dInput, *dOutput;

	GPU_ERROR_CHECK(cudaMalloc(
			&dInput,
			numOfElements * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(
			&dOutput,
			numOfTridiagIntegrals * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dInput,
			hInput,
			numOfElements * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))

	tridiagIntegralApproximation_D2D<T>(
			dInput,
			initElement,
			numOfElements,
			numOfElementsPerTridiagIntegral,
			numOfTridiagIntegrals,
			d,
			coarseStepSize,
			fineStepSize,
			dOutput,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hOutput,
			dOutput,
			numOfTridiagIntegrals * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dOutput))
	GPU_ERROR_CHECK(cudaFree(dInput))
}

template<typename T>
void averagedEuler_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuSingleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream) {
	T *dOuRealizationCompact;
	unsigned int numOfThreads, numOfBlocks, numOfCoarseStepsPerThread;

	numOfThreads = KT_NUM_OF_THREADS;
	numOfBlocks = divUp(degreeOfSolverParallelism, KT_NUM_OF_THREADS);
	numOfCoarseStepsPerThread = divUp(numOfTotalCoarseSteps, degreeOfSolverParallelism);

	GPU_ERROR_CHECK(cudaMalloc(&dOuRealizationCompact, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealizationCompact,
			sizeof(T),
			dOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyDeviceToDevice,
			stream))

	averagedEulerKernel<T><<<numOfThreads, numOfBlocks, 0, stream>>>(
			dZ1Init,
			dZ2Init,
			dOuRealizationCompact,
			dOuSingleAverages,
			numOfCoarseStepsPerThread,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu);
	GPU_ERROR_CHECK(cudaPeekAtLastError())

	GPU_ERROR_CHECK(cudaFree(dOuRealizationCompact))
}

template<typename T>
void averagedEuler_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuSingleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream) {
	T *dZ1Init, *dZ2Init, *dOuRealization, *dOuSingleAverages, *dZ1, *dZ2, *dDdu;

	GPU_ERROR_CHECK(cudaMalloc(&dZ1Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuRealization, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuSingleAverages, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ1, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dDdu, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ1Init,
			hZ1Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ2Init,
			hZ2Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice))
	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealization,
			sizeof(T),
			hOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuSingleAverages,
			hOuSingleAverages,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice))

	averagedEuler_D2D(
			dZ1Init,
			dZ2Init,
			dOuRealization,
			1,
			dOuSingleAverages,
			numOfTotalCoarseSteps,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ1,
			dZ1,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ2,
			dZ2,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hDdu,
			dDdu,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dDdu))
	GPU_ERROR_CHECK(cudaFree(dZ2))
	GPU_ERROR_CHECK(cudaFree(dZ1))
	GPU_ERROR_CHECK(cudaFree(dOuSingleAverages))
	GPU_ERROR_CHECK(cudaFree(dOuRealization))
	GPU_ERROR_CHECK(cudaFree(dZ2Init))
	GPU_ERROR_CHECK(cudaFree(dZ1Init))
}

template<typename T>
void averagedHeun_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuSingleAverages,
		const T *dOuDoubleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream) {
	T *dOuRealizationCompact;
	unsigned int numOfThreads, numOfBlocks, numOfCoarseStepsPerThread;

	numOfThreads = KT_NUM_OF_THREADS;
	numOfBlocks = divUp(degreeOfSolverParallelism, KT_NUM_OF_THREADS);
	numOfCoarseStepsPerThread = divUp(numOfTotalCoarseSteps, degreeOfSolverParallelism);

	GPU_ERROR_CHECK(cudaMalloc(&dOuRealizationCompact, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealizationCompact,
			sizeof(T),
			dOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyDeviceToDevice,
			stream))

	averagedHeunKernel<T><<<numOfThreads, numOfBlocks, 0, stream>>>(
			dZ1Init,
			dZ2Init,
			dOuRealizationCompact,
			dOuSingleAverages,
			dOuDoubleAverages,
			numOfCoarseStepsPerThread,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu);
	GPU_ERROR_CHECK(cudaPeekAtLastError())

	GPU_ERROR_CHECK(cudaFree(dOuRealizationCompact))
}

template<typename T>
void averagedHeun_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuSingleAverages,
		const T *hOuDoubleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream) {
	T *dZ1Init, *dZ2Init, *dOuRealization, *dOuSingleAverages, *dOuDoubleAverages, *dZ1, *dZ2, *dDdu;

	GPU_ERROR_CHECK(cudaMalloc(&dZ1Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuRealization, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuSingleAverages, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuDoubleAverages, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ1, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dDdu, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ1Init,
			hZ1Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ2Init,
			hZ2Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealization,
			sizeof(T),
			hOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuSingleAverages,
			hOuSingleAverages,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuDoubleAverages,
			hOuDoubleAverages,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))

	averagedHeun_D2D(
			dZ1Init,
			dZ2Init,
			dOuRealization,
			1,
			dOuSingleAverages,
			dOuDoubleAverages,
			numOfTotalCoarseSteps,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ1,
			dZ1,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ2,
			dZ2,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hDdu,
			dDdu,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dDdu))
	GPU_ERROR_CHECK(cudaFree(dZ2))
	GPU_ERROR_CHECK(cudaFree(dZ1))
	GPU_ERROR_CHECK(cudaFree(dOuDoubleAverages))
	GPU_ERROR_CHECK(cudaFree(dOuSingleAverages))
	GPU_ERROR_CHECK(cudaFree(dOuRealization))
	GPU_ERROR_CHECK(cudaFree(dZ2Init))
	GPU_ERROR_CHECK(cudaFree(dZ1Init))
}

template<typename T>
void taylor3_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuLineIntegrals,
		const T *dOuTriaIntegrals,
		const T *dOuTetrahIntegrals,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream) {
	T *dOuRealizationCompact;
	unsigned int numOfThreads, numOfBlocks, numOfCoarseStepsPerThread;

	numOfThreads = KT_NUM_OF_THREADS;
	numOfBlocks = divUp(degreeOfSolverParallelism, KT_NUM_OF_THREADS);
	numOfCoarseStepsPerThread = divUp(numOfTotalCoarseSteps, degreeOfSolverParallelism);

	GPU_ERROR_CHECK(cudaMalloc(&dOuRealizationCompact, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealizationCompact,
			sizeof(T),
			dOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyDeviceToDevice,
			stream))

	taylor3Kernel<T><<<numOfThreads, numOfBlocks, 0, stream>>>(
			dZ1Init,
			dZ2Init,
			dOuRealizationCompact,
			dOuLineIntegrals,
			dOuTriaIntegrals,
			dOuTetrahIntegrals,
			numOfCoarseStepsPerThread,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu);
	GPU_ERROR_CHECK(cudaPeekAtLastError())

	GPU_ERROR_CHECK(cudaFree(dOuRealizationCompact))
}

template<typename T>
void taylor3_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuLineIntegrals,
		const T *hOuTriaIntegrals,
		const T *hOuTetrahIntegrals,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream) {
	T *dZ1Init, *dZ2Init, *dOuRealization, *dOuLineIntegrals, *dOuTriaIntegrals, *dOuTetrahIntegrals, *dZ1, *dZ2, *dDdu;

	GPU_ERROR_CHECK(cudaMalloc(&dZ1Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuRealization, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuLineIntegrals, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuTriaIntegrals, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuTetrahIntegrals, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ1, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dDdu, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ1Init,
			hZ1Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ2Init,
			hZ2Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealization,
			sizeof(T),
			hOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuLineIntegrals,
			hOuLineIntegrals,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuTriaIntegrals,
			hOuTriaIntegrals,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuTetrahIntegrals,
			hOuTetrahIntegrals,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))

	taylor3_D2D(
			dZ1Init,
			dZ2Init,
			dOuRealization,
			1,
			dOuLineIntegrals,
			dOuTriaIntegrals,
			dOuTetrahIntegrals,
			numOfTotalCoarseSteps,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ1,
			dZ1,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ2,
			dZ2,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hDdu,
			dDdu,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dDdu))
	GPU_ERROR_CHECK(cudaFree(dZ2))
	GPU_ERROR_CHECK(cudaFree(dZ1))
	GPU_ERROR_CHECK(cudaFree(dOuTetrahIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuTriaIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuLineIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuRealization))
	GPU_ERROR_CHECK(cudaFree(dZ2Init))
	GPU_ERROR_CHECK(cudaFree(dZ1Init))
}

template<typename T>
void taylor4_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuLineIntegrals,
		const T *dOuTriaIntegrals,
		const T *dOuTetrahIntegrals,
		const T *dOuPentacIntegral,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream) {
	T *dOuRealizationCompact;
	unsigned int numOfThreads, numOfBlocks, numOfCoarseStepsPerThread;

	numOfThreads = KT_NUM_OF_THREADS;
	numOfBlocks = divUp(degreeOfSolverParallelism, KT_NUM_OF_THREADS);
	numOfCoarseStepsPerThread = divUp(numOfTotalCoarseSteps, degreeOfSolverParallelism);

	GPU_ERROR_CHECK(cudaMalloc(&dOuRealizationCompact, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealizationCompact,
			sizeof(T),
			dOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyDeviceToDevice,
			stream))

	taylor4Kernel<T><<<numOfThreads, numOfBlocks, 0, stream>>>(
			dZ1Init,
			dZ2Init,
			dOuRealizationCompact,
			dOuLineIntegrals,
			dOuTriaIntegrals,
			dOuTetrahIntegrals,
			dOuPentacIntegral,
			numOfCoarseStepsPerThread,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu);
	GPU_ERROR_CHECK(cudaPeekAtLastError())

	GPU_ERROR_CHECK(cudaFree(dOuRealizationCompact))
}

template<typename T>
void taylor4_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuLineIntegrals,
		const T *hOuTriaIntegrals,
		const T *hOuTetrahIntegrals,
		const T *hOuPentacIntegral,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream) {
	T *dZ1Init, *dZ2Init, *dOuRealization, *dOuLineIntegrals, *dOuTriaIntegrals, *dOuTetrahIntegrals, *dOuPentacIntegral, *dZ1, *dZ2, *dDdu;

	GPU_ERROR_CHECK(cudaMalloc(&dZ1Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2Init, degreeOfSolverParallelism * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuRealization, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuLineIntegrals, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuTriaIntegrals, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuTetrahIntegrals, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOuPentacIntegral, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ1, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dZ2, numOfTotalCoarseSteps * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dDdu, numOfTotalCoarseSteps * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ1Init,
			hZ1Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dZ2Init,
			hZ2Init,
			degreeOfSolverParallelism * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpy2DAsync(
			dOuRealization,
			sizeof(T),
			hOuRealization,
			ouStride * sizeof(T),
			sizeof(T),
			numOfTotalCoarseSteps,
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuLineIntegrals,
			hOuLineIntegrals,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuTriaIntegrals,
			hOuTriaIntegrals,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuTetrahIntegrals,
			hOuTetrahIntegrals,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			dOuPentacIntegral,
			hOuPentacIntegral,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyHostToDevice,
			stream))

	taylor4_D2D(
			dZ1Init,
			dZ2Init,
			dOuRealization,
			1,
			dOuLineIntegrals,
			dOuTriaIntegrals,
			dOuTetrahIntegrals,
			dOuPentacIntegral,
			numOfTotalCoarseSteps,
			degreeOfSolverParallelism,
			coarseStepSize,
			dZ1,
			dZ2,
			dDdu,
			stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ1,
			dZ1,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hZ2,
			dZ2,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))
	GPU_ERROR_CHECK(cudaMemcpyAsync(
			hDdu,
			dDdu,
			numOfTotalCoarseSteps * sizeof(T),
			cudaMemcpyDeviceToHost,
			stream))

	GPU_ERROR_CHECK(cudaFree(dDdu))
	GPU_ERROR_CHECK(cudaFree(dZ2))
	GPU_ERROR_CHECK(cudaFree(dZ1))
	GPU_ERROR_CHECK(cudaFree(dOuPentacIntegral))
	GPU_ERROR_CHECK(cudaFree(dOuTetrahIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuTriaIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuLineIntegrals))
	GPU_ERROR_CHECK(cudaFree(dOuRealization))
	GPU_ERROR_CHECK(cudaFree(dZ2Init))
	GPU_ERROR_CHECK(cudaFree(dZ1Init))
}

#include "solverInterfaceTemplates"
