#include "averager.cuh"

template<typename T>
__device__ void sum(
		T *workingArray,
		const unsigned int numOfElements) {
	const unsigned int numOfElementsPerThread =
			numOfElements / blockDim.x;

	unsigned int idx;
	T sum1, sum2;

	sum1 = sum2 = (T) 0;

	/*
	 * First phase:
	 *
	 */
	for(unsigned int i = 0; i < (numOfElementsPerThread>>1); i++) {
		sum1 += workingArray[i * blockDim.x + LOCAL_THREAD_ID_1D];
		sum2 += workingArray[(i + (numOfElementsPerThread>>1)) * blockDim.x + LOCAL_THREAD_ID_1D];
	}

	__syncthreads();

	workingArray[LOCAL_THREAD_ID_1D] = sum1;
	workingArray[LOCAL_THREAD_ID_1D + blockDim.x] = sum2;

	__syncthreads();

	/*
	 * Second phase:
	 *
	 */
	for(unsigned int stride = 1; stride < (blockDim.x * 2); stride <<= 1) {
		idx = LOCAL_THREAD_ID_1D * stride * 2;

		if(idx < 2 * blockDim.x) {
			workingArray[idx] += (idx + stride < 2 * blockDim.x) ?
					workingArray[idx + stride] :
					(T) 0;
		}
		__syncthreads();
	}
}

template<typename T>
__global__ void sumKernel(
		const T *elementsToSum,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerSum,
		T *sums) {
	const unsigned int numOfSMemSlots =
			AVERAGER_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfChunks =
			numOfSMemSlots / blockDim.x;
	const unsigned int numOfSummandsPerThreadPerChunk =
			divUp(numOfElementsPerSum, numOfSMemSlots);
	const unsigned int start =
			blockIdx.x * numOfElementsPerSum;

	__shared__ T workingArray[numOfSMemSlots];

	unsigned int idx;
	T chunkSum;

	for(unsigned int i = 0; i < numOfChunks; i++) {
		chunkSum = (T) 0;

		for(unsigned int j = 0; j < numOfSummandsPerThreadPerChunk; j++) {
			idx = i * blockDim.x * numOfSummandsPerThreadPerChunk +
					j * blockDim.x +
					LOCAL_THREAD_ID_1D;
			chunkSum += ((idx < numOfElementsPerSum) & (start + idx < numOfElements)) ?
					elementsToSum[start + idx] :
					(T) 0;
		}

		workingArray[i * blockDim.x + LOCAL_THREAD_ID_1D] = chunkSum;
	}

	__syncthreads();

	sum(workingArray, numOfSMemSlots);

	__syncthreads();

	if(LOCAL_THREAD_ID_1D == 0) {
		sums[blockIdx.x] = workingArray[0];
	}
}

template<typename T>
__global__ void singleAverageKernel(
		const T *elementsToAverage,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerSingleAverage,
		const unsigned int N,
		T *averages) {
	const unsigned int numOfSMemSlots =
			AVERAGER_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfChunks =
			numOfSMemSlots / blockDim.x;
	const unsigned int numOfSummandsPerThreadPerChunk =
			divUp(numOfElementsPerSingleAverage, numOfSMemSlots);
	const unsigned int start =
			blockIdx.x * numOfElementsPerSingleAverage;

	__shared__ T workingArray[numOfSMemSlots];

	unsigned int idx;
	T chunkSum;

	for(unsigned int i = 0; i < numOfChunks; i++) {
		chunkSum = (T) 0;

		for(unsigned int j = 0; j < numOfSummandsPerThreadPerChunk; j++) {
			idx = i * blockDim.x * numOfSummandsPerThreadPerChunk +
					j * blockDim.x +
					LOCAL_THREAD_ID_1D;
			chunkSum += ((idx < numOfElementsPerSingleAverage) & (start + idx < numOfElements)) ?
					elementsToAverage[start + idx] :
					(T) 0;
		}

		workingArray[i * blockDim.x + LOCAL_THREAD_ID_1D] = chunkSum;
	}

	__syncthreads();

	sum(workingArray, numOfSMemSlots);

	__syncthreads();

	if(LOCAL_THREAD_ID_1D == 0) {
		averages[blockIdx.x] = ((T) 1 / N) * workingArray[0];
	}
}

template<typename T>
__global__ void doubleAverageKernel(
		const T *elementsToAverage,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerDoubleAverage,
		const unsigned int N,
		T *averages) {
	const unsigned int numOfSMemSlots =
			AVERAGER_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfChunks =
			numOfSMemSlots / blockDim.x;
	const unsigned int numOfSummandsPerThreadPerChunk =
			divUp(numOfElementsPerDoubleAverage, numOfSMemSlots);
	const unsigned int start =
			blockIdx.x * numOfElementsPerDoubleAverage;
	const bool logSerialization =
			(numOfElementsPerDoubleAverage != N);

	__shared__ T workingArray[numOfSMemSlots];

	unsigned int idx;
	T chunkSum;

	for(unsigned int i = 0; i < numOfChunks; i++) {
		chunkSum = (T) 0;

		for(unsigned int j = 0; j < numOfSummandsPerThreadPerChunk; j++) {
			idx = i * blockDim.x * numOfSummandsPerThreadPerChunk +
					j * blockDim.x +
					LOCAL_THREAD_ID_1D;
			chunkSum += ((idx < numOfElementsPerDoubleAverage) & (start + idx < numOfElements)) ?
					(N - idx - logSerialization * start) * elementsToAverage[start + idx] :
					(T) 0;
		}

		workingArray[i * blockDim.x + LOCAL_THREAD_ID_1D] = chunkSum;
	}

	__syncthreads();

	sum(workingArray, numOfSMemSlots);

	__syncthreads();

	if(LOCAL_THREAD_ID_1D == 0) {
		averages[blockIdx.x] = ((T) 2 / (N * N)) * workingArray[0];
	}
}

template<typename T>
__global__ void tridiagIntegralApproximationKernel(
		const T *elementsToIntegrate,
		const T *initElements,
		const unsigned int numOfElements,
		const unsigned int numOfElementsPerTridiagIntegral,
		const unsigned int d,
		const T coarseStepSize,
		const T fineStepSize,
		T *tridiagIntegrals) {
	const unsigned int numOfSMemSlots =
			AVERAGER_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfChunks =
			numOfSMemSlots / blockDim.x;
	const unsigned int numOfSummandsPerThreadPerChunk =
			divUp(numOfElementsPerTridiagIntegral, numOfSMemSlots);
	const unsigned int start =
			blockIdx.x * numOfElementsPerTridiagIntegral;

	__shared__ T workingArray[numOfSMemSlots];

	unsigned int idx;
	T initElement, chunkSum;

	initElement = initElements[blockIdx.x];

	for(unsigned int i = 0; i < numOfChunks; i++) {
		chunkSum = (T) 0;

		for(unsigned int j = 0; j < numOfSummandsPerThreadPerChunk; j++) {
			idx = i * blockDim.x * numOfSummandsPerThreadPerChunk +
					j * blockDim.x +
					LOCAL_THREAD_ID_1D;

			if((idx < numOfElementsPerTridiagIntegral) & (start + idx < numOfElements)) {
				chunkSum +=
						power(coarseStepSize - (idx + 1) * fineStepSize, (T) d) *
						(initElement - elementsToIntegrate[start + idx]);
			}
		}

		workingArray[i * blockDim.x + LOCAL_THREAD_ID_1D] = chunkSum;
	}

	__syncthreads();

	sum(workingArray, numOfSMemSlots);

	__syncthreads();

	if(LOCAL_THREAD_ID_1D == 0) {
		tridiagIntegrals[blockIdx.x] = (fineStepSize / fac(d)) * workingArray[0];
	}
}

#include "averagerTemplates"
