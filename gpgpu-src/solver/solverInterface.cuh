#pragma once

#include "averager.cuh"
#include "kt.cuh"
#include "solverHelper.h"

template<typename T>
void singleAverage_D2D(
		const T *dInput,
		const unsigned int numOfAllElements,
		const unsigned int numOfElementsPerSingleAverage,
		const unsigned int numOfSingleAverages,
		T *dOutput,
		cudaStream_t &stream);
template<typename T>
void singleAverage_H2H(
		const T *hInput,
		const unsigned int numOfAllElements,
		const unsigned int numOfElementsPerSingleAverage,
		const unsigned int numOfSingleAverages,
		T *hOutput,
		cudaStream_t &stream);
template<typename T>
void doubleAverage_D2D(
		const T *dInput,
		const unsigned int numOfAllElements,
		const unsigned int numOfElementsPerDoubleAverage,
		const unsigned int numOfDoubleAverages,
		T *dOutput,
		cudaStream_t &stream);
template<typename T>
void doubleAverage_H2H(
		const T *hInput,
		const unsigned int numOfAllElements,
		const unsigned int numOfElementsPerDoubleAverage,
		const unsigned int numOfDoubleAverages,
		T *hOutput,
		cudaStream_t &stream);
template<typename T>
void tridiagIntegralApproximation_D2D(
		const T *dInput,
		const T initElement,
		const unsigned int numOfAllElements,
		const unsigned int numOfElementsPerTridiagIntegral,
		const unsigned int numOfTridiagIntegrals,
		const unsigned int d,
		const T coarseStepSize,
		const T fineStepSize,
		T *dOutput,
		cudaStream_t &stream);
template<typename T>
void tridiagIntegralApproximation_H2H(
		const T *hInput,
		const T initElement,
		const unsigned int numOfAllElements,
		const unsigned int numOfElementsPerTridiagIntegral,
		const unsigned int numOfTridiagIntegrals,
		const unsigned int d,
		const T coarseStepSize,
		const T fineStepSize,
		T *hOutput,
		cudaStream_t &stream);
template<typename T>
void averagedEuler_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuSingleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream);
template<typename T>
void averagedEuler_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuSingleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream);
template<typename T>
void averagedHeun_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuSingleAverages,
		const T *dOuDoubleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream);
template<typename T>
void averagedHeun_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuSingleAverages,
		const T *hOuDoubleAverages,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream);
template<typename T>
void taylor3_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuLineIntegrals,
		const T *dOuTriaIntegrals,
		const T *dOuTetrahIntegrals,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream);
template<typename T>
void taylor3_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuLineIntegrals,
		const T *hOuTriaIntegrals,
		const T *hOuTetrahIntegrals,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream);
template<typename T>
void taylor4_D2D(
		const T *dZ1Init,
		const T *dZ2Init,
		const T *dOuRealization,
		const unsigned int ouStride,
		const T *dOuLineIntegrals,
		const T *dOuTriaIntegrals,
		const T *dOuTetrahIntegrals,
		const T *dOuPentacIntegral,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *dZ1,
		T *dZ2,
		T *dDdu,
		cudaStream_t &stream);
template<typename T>
void taylor4_H2H(
		const T *hZ1Init,
		const T *hZ2Init,
		const T *hOuRealization,
		const unsigned int ouStride,
		const T *hOuLineIntegrals,
		const T *hOuTriaIntegrals,
		const T *hOuTetrahIntegrals,
		const T *hOuPentacIntegral,
		const unsigned int numOfTotalCoarseSteps,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *hZ1,
		T *hZ2,
		T *hDdu,
		cudaStream_t &stream);
