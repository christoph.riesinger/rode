#include "kt.cuh"

template<typename T>
__device__ void computeEulerDDU(
		const T zeta,
		const T omega,
		const T ouSingleAverage,
		const T O_t,
		const T coarseStepSize,
		T &z1,
		T &z2,
		T &ddu) {
	T G1, G2, H1, H2, u, du;

	// 1 FLOPs
	G1 = -ouSingleAverage;
	// 3 FLOPs (with FMA), 4 FLOPs (without FMA)
	G2 = ((T) 1.0 - (T) 2.0 * zeta * omega) * ouSingleAverage;
	// 1 FLOPs
	H1 = -z2;
	// 6 FLOPs
	H2 = (omega * omega * z1) - ((T) 2.0 * zeta * omega * z2);

	// 2 FLOPs (with FMA), 3 FLOPs (without FMA)
	z1 += coarseStepSize * (G1 + H1);
	// 2 FLOPs (with FMA), 3 FLOPs (without FMA)
	z2 += coarseStepSize * (G2 + H2);

	// 0 FLOP
	u = z1;
	// 1 FLOP (with FMA), 2 FLOPs (without FMA)
	du = -z2 - O_t;

	// 6 FLOPs
	ddu = ((T) -2.0 * zeta * omega * du) - (omega * omega * u);

	// => total 22 FLOPs (with FMA), 26 FLOPs (without FMA)
}

template<typename T>
__device__ void computeHeunDDU(
		const T zeta,
		const T omega,
		const T ouSingleAverage,
		const T ouDoubleAverage,
		const T O_t,
		const T coarseStepSize,
		T &z1,
		T &z2,
		T &ddu) {
	T G11, G12, G21, G22, H11, H12, H21, H22, u, du;

	// 1 FLOPs
	G11 = -ouSingleAverage;
	// 3 FLOPs (with FMA), 4 FLOPs (without FMA)
	G12 = ((T) 1.0 - (T) 2.0 * zeta * omega) * ouSingleAverage;
	// 1 FLOPs
	G21 = -ouDoubleAverage;
	// 3 FLOPs (with FMA), 4 FLOPs (without FMA)
	G22 = ((T) 1.0 - (T) 2.0 * zeta * omega) * ouDoubleAverage;
	// 1 FLOPs
	H11 = -z2;
	// 6 FLOPs
	H12 = (omega * omega * z1) - ((T) 2.0 * zeta * omega * z2);
	// 3 FLOPs (with FMA), 4 FLOPs (without FMA)
	H21 = -(z2 + coarseStepSize * (G22 + H12));
	// 9 FLOPs (with FMA), 12 FLOPs (without FMA)
    H22 = omega * omega * (z1 + coarseStepSize * (G21 + H11)) -
    		(T) 2.0 * zeta * omega * (z2 + coarseStepSize * (G22 + H12));

    // 3 FLOPs (with FMA), 4 FLOPs (without FMA)
	z1 += coarseStepSize * (G11 + (T) 0.5 * (H11 + H21));
	// 3 FLOPs (with FMA), 4 FLOPs (without FMA)
	z2 += coarseStepSize * (G12 + (T) 0.5 * (H12 + H22));

	// 0 FLOP
	u = z1;
	// 1 FLOP (with FMA), 2 FLOPs (without FMA)
	du = -z2 - O_t;

	// 6 FLOPs
	ddu = ((T) -2.0 * zeta * omega * du) - (omega * omega * u);

	// => total 40 FLOPs (with FMA), 49 FLOPs (without FMA)
}

template<typename T>
__device__ void computeTaylor3DDU(
		const T zeta,
		const T omega,
		const T ouLineIntegral,
		const T ouTriaIntegral,
		const T ouTetrahIntegral,
		const T O_t,
		const T coarseStepSize,
		T &z1,
		T &z2,
		T &ddu) {
	T f1, f2, x_12, x_21, x_22, x_31, x_32, x_41, x_42, u, du;

	// 2 FLOPs (with FMA), 2 FLOPs (without FMA)
	f1 = -(z2 + O_t);
	// 5 FLOPs (with FMA), 7 FLOPs (without FMA)
	f2 = O_t + omega * (omega * z1 - (T) 2.0 * zeta * (z2 + O_t));
	// 4 FLOPs (with FMA), 5 FLOPs (without FMA)
	x_12 = omega * (omega * f1 - (T) 2.0 * zeta * f2);
	// 2 FLOPs (with FMA), 3 FLOPs (without FMA)
	x_21 = (T) 2.0 * zeta * omega - (T) 1.0;
	// 4 FLOPs (with FMA), 5 FLOPs (without FMA)
	x_22 = (T) 2.0 * zeta * omega * x_21 - (omega * omega);
	// 4 FLOPs (with FMA), 5 FLOPs (without FMA)
	x_31 = omega * ((T) 2.0 * zeta * f2 - omega * f1);
	// 8 FLOPs (with FMA), 10 FLOPs (without FMA)
	x_32 = omega * omega * (f2 * ((T) 4.0 * zeta * zeta - (T) 1.0) - (T) 2.0 * zeta * omega * f1);
	// 5 FLOPs (with FMA), 7 FLOPs (without FMA)
	x_41 = omega * (omega - (T) 2.0 * zeta * ((T) 2.0 * zeta * omega + (T) 1.0));
	// 9 FLOPs (with FMA), 12 FLOPs (without FMA)
	x_42 = omega * omega * ((T) 2.0 * zeta * omega +
			((T) 4.0 * zeta * zeta - (T) 1.0) * ((T) 1.0 - (T) 2.0 * zeta * omega));

	// 12 FLOPs (with FMA), 16 FLOPs (without FMA)
	z1 += coarseStepSize * f1 -
			ouLineIntegral -
			(T) 0.5 * coarseStepSize * coarseStepSize * f2 +
			x_21 * ouTriaIntegral +
			((T) 1.0 / (T) 6.0) * coarseStepSize * coarseStepSize * coarseStepSize * x_31 +
			x_41 * ouTetrahIntegral;
	// 15 FLOPs (with FMA), 19 FLOPs (without FMA)
	z2 += coarseStepSize * f2 -
			((T) 2.0 * zeta * omega - (T) 1.0) * ouLineIntegral +
			(T) 0.5 * coarseStepSize * coarseStepSize * x_12 +
			x_22 * ouTriaIntegral +
			((T) 1.0 / (T) 6.0) * coarseStepSize * coarseStepSize * coarseStepSize * x_32 +
			x_42 * ouTetrahIntegral;

	// 0 FLOP
	u = z1;
	// 1 FLOP (with FMA), 2 FLOPs (without FMA)
	du = -z2 - O_t;

	// 6 FLOPs
	ddu = ((T) -2.0 * zeta * omega * du) - (omega * omega * u);

	// => total 77 FLOPs (with FMA), 99 FLOPs (without FMA)
}

template<typename T>
__device__ void computeTaylor4DDU(
		const T zeta,
		const T omega,
		const T ouLineIntegral,
		const T ouTriaIntegral,
		const T ouTetrahIntegral,
		const T ouPentacIntegral,
		const T O_t,
		const T coarseStepSize,
		T &z1,
		T &z2,
		T &ddu) {
	// TODO
}

template<typename T>
__global__ void averagedEulerKernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouSingleAverages,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu) {
	T z1Local, z2Local, dduLocal;
	unsigned int idx;
//	unsigned int ouRealizationIdx;

	if(GLOBAL_THREAD_ID_1D < degreeOfSolverParallelism) {
		z1Local = z1Init[GLOBAL_THREAD_ID_1D];
		z2Local = z2Init[GLOBAL_THREAD_ID_1D];

		for(unsigned int i = 0; i < numOfCoarseStepsPerThread; i++) {
			idx = GLOBAL_THREAD_ID_1D * numOfCoarseStepsPerThread + i;
//			ouRealizationIdx = idx * ouStride;

			computeEulerDDU<T>(
					(T) KT_ZETA,
					(T) KT_OMEGA,
					ouSingleAverages[idx],
//					ouRealization[ouRealizationIdx],
					ouRealization[idx],
					coarseStepSize,
					z1Local,
					z2Local,
					dduLocal);
			z1[idx] = z1Local;
			z2[idx] = z2Local;
			ddu[idx] = dduLocal;
		}
	}
}

template<typename T>
__global__ void averagedHeunKernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouSingleAverages,
		const T *ouDoubleAverages,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu) {
	T z1Local, z2Local, dduLocal;
	unsigned int idx;
//	unsigned int ouRealizationIdx;

	if(GLOBAL_THREAD_ID_1D < degreeOfSolverParallelism) {
		z1Local = z1Init[GLOBAL_THREAD_ID_1D];
		z2Local = z2Init[GLOBAL_THREAD_ID_1D];

		for(unsigned int i = 0; i < numOfCoarseStepsPerThread; i++) {
			idx = GLOBAL_THREAD_ID_1D * numOfCoarseStepsPerThread + i;
//			ouRealizationIdx = idx * ouStride;

			computeHeunDDU<T>(
					(T) KT_ZETA,
					(T) KT_OMEGA,
					ouSingleAverages[idx],
					ouDoubleAverages[idx],
//					ouRealization[ouRealizationIdx],
					ouRealization[idx],
					coarseStepSize,
					z1Local,
					z2Local,
					dduLocal);
			z1[idx] = z1Local;
			z2[idx] = z2Local;
			ddu[idx] = dduLocal;
		}
	}
}

template<typename T>
__global__ void taylor3Kernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouLineIntegrals,
		const T *ouTriaIntegrals,
		const T *ouTetrahIntegrals,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu) {
	T z1Local, z2Local, dduLocal;
	unsigned int idx;
//	unsigned int ouRealizationIdx;

	if(GLOBAL_THREAD_ID_1D < degreeOfSolverParallelism) {
		z1Local = z1Init[GLOBAL_THREAD_ID_1D];
		z2Local = z2Init[GLOBAL_THREAD_ID_1D];

		for(unsigned int i = 0; i < numOfCoarseStepsPerThread; i++) {
			idx = GLOBAL_THREAD_ID_1D * numOfCoarseStepsPerThread + i;
//			ouRealizationIdx = idx * ouStride;

			computeTaylor3DDU<T>(
					(T) KT_ZETA,
					(T) KT_OMEGA,
					ouLineIntegrals[idx],
					ouTriaIntegrals[idx],
					ouTetrahIntegrals[idx],
//					ouRealization[ouRealizationIdx],
					ouRealization[idx],
					coarseStepSize,
					z1Local,
					z2Local,
					dduLocal);
			z1[idx] = z1Local;
			z2[idx] = z2Local;
			ddu[idx] = dduLocal;
		}
	}
}

template<typename T>
__global__ void taylor4Kernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouLineIntegrals,
		const T *ouTriaIntegrals,
		const T *ouTetrahIntegrals,
		const T *ouPentacIntegrals,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu) {
	T z1Local, z2Local, dduLocal;
	unsigned int idx;
//	unsigned int ouRealizationIdx;

	if(GLOBAL_THREAD_ID_1D < degreeOfSolverParallelism) {
		z1Local = z1Init[GLOBAL_THREAD_ID_1D];
		z2Local = z2Init[GLOBAL_THREAD_ID_1D];

		for(unsigned int i = 0; i < numOfCoarseStepsPerThread; i++) {
			idx = GLOBAL_THREAD_ID_1D * numOfCoarseStepsPerThread + i;
//			ouRealizationIdx = idx * ouStride;

			computeTaylor4DDU<T>(
					(T) KT_ZETA,
					(T) KT_OMEGA,
					ouLineIntegrals[idx],
					ouTriaIntegrals[idx],
					ouTetrahIntegrals[idx],
					ouPentacIntegrals[idx],
//					ouRealization[ouRealizationIdx],
					ouRealization[idx],
					coarseStepSize,
					z1Local,
					z2Local,
					dduLocal);
			z1[idx] = z1Local;
			z2[idx] = z2Local;
			ddu[idx] = dduLocal;
		}
	}
}

#include "ktTemplates"
