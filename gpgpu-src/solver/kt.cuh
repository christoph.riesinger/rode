#pragma once

#include "solverHelper.h"

#define KT_ZETA		0.64
#define KT_OMEGA	15.56

template<typename T>
__global__ void averagedEulerKernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouSingleAverages,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu);
template<typename T>
__global__ void averagedHeunKernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouSingleAverages,
		const T *ouDoubleAverages,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu);
template<typename T>
__global__ void taylor3Kernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouLineIntegrals,
		const T *ouTriaIntegrals,
		const T *ouTetrahIntegrals,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu);
template<typename T>
__global__ void taylor4Kernel(
		const T *z1Init,
		const T *z2Init,
		const T *ouRealization,
//		const unsigned int ouStride,
		const T *ouLineIntegrals,
		const T *ouTriaIntegrals,
		const T *ouTetrahIntegrals,
		const T *ouPentacIntegral,
		const unsigned int numOfCoarseStepsPerThread,
		const unsigned int degreeOfSolverParallelism,
		const T coarseStepSize,
		T *z1,
		T *z2,
		T *ddu);
