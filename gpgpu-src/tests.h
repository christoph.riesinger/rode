#include "ou/ouInterface.cuh"
#include "rand/randInterface.cuh"
#include "solver/solverInterface.cuh"

void printConstants();
void testRand();
void testScan();
void testOU();
void testSingleAverage();
void testDoubleAverage();
void testTridiagIntegralApproximation();
void testAveragedEuler();
void testAveragedHeun();
void testTridiagIntegralApproximation();
