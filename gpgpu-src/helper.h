#pragma once

#include <algorithm>
#include <cassert>
#include <cfloat>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>

#ifdef USE_MPI
	#include <mpi.h>
#endif

#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>

#include "constants.h"

#define GPU_ERROR_CHECK(code) { gpuErrorCheck((code), __FILE__, __LINE__); }

inline void gpuErrorCheck(cudaError_t code, std::string file, int line, bool abort = true) {
	// cudaDeviceSynchronize();
	if (code != cudaSuccess) {
		std::cerr << "!!! The following CUDA API error occurred !!! " << std::endl;
		std::cerr << cudaGetErrorString(code) << std::endl;
		std::cerr << "Info: file -> " << file << ", line -> " << line << std::endl;
		if(abort) {
			exit(code);
		}
	}
}

__device__ __host__ inline void add(unsigned long long xHi, unsigned long long xLo, unsigned int y, unsigned long long &hi, unsigned long long &lo) {
	unsigned long long temp;

	temp = (((xLo & y) & 1) + (xLo >> 1) + (y >> 1)) >> 63;
	hi   = xHi + temp;
	lo   = y + xLo;
}

__device__ __host__ inline void add(unsigned long long x, unsigned long long y, unsigned long long &hi, unsigned long long &lo) {
	hi = (((x & y) & 1) + (x >> 1) + (y >> 1)) >> 63;
	lo = x + y;
}

__host__ __device__ inline unsigned int divUp(unsigned int dividend, unsigned int divisor) {
	return ((dividend % divisor) == 0) ? (dividend / divisor) : (dividend / divisor + 1);
}

__host__ __device__ inline unsigned int fac(unsigned int x) {
    unsigned int result = 1;

    while(x) {
    	result *= x--;
    }

    return result;
}

__host__ __device__ inline unsigned int modUp(unsigned int dividend, unsigned int divisor) {
	return ((dividend % divisor) == 0) ? (divisor) : (dividend % divisor);
}

__device__ __host__ inline void mult(unsigned long long x, unsigned long long y, unsigned long long &hi, unsigned long long &lo) {
	unsigned long long u1 = (x & 0xffffffff);
	unsigned long long v1 = (y & 0xffffffff);
	unsigned long long t  = (u1 * v1);
	unsigned long long w3 = (t & 0xffffffff);
	unsigned long long k  = (t >> 32);

	x >>= 32;
	t = (x * v1) + k;
	k = (t & 0xffffffff);
	unsigned long long w1 = (t >> 32);

	y >>= 32;
	t = (u1 * y) + k;
	k = (t >> 32);

	hi = (x * y) + w1 + k;
	lo = (t << 32) + w3;
}

template<typename T>
__device__ __host__ inline void power(T basis, unsigned int exp, T &result) {
    result = 1;

    while(exp) {
    	if(exp & 1) {
    		result *= basis;
    	}
    	exp >>= 1;
    	basis *= basis;
    }
}

template <typename T>
__device__ __host__ inline T power(T basis, T exp);

template <>
__device__ __host__ inline float power<float>(float basis, float exp) {
	/*
	float result;

	power(basis, exp, result);

	return result;
	*/
	return powf(basis, exp);
};

template <>
__device__ __host__ inline double power<double>(double basis, double exp) {
	double result;

	power(basis, exp, result);

	return result;
	// return pow(basis, exp);
};

__device__ __host__ inline void swap(unsigned int &x, unsigned int &y) {
	x ^= y;
	y ^= x;
	x ^= y;
}

__device__ __host__ inline void swap(unsigned long long &x, unsigned long long &y) {
	x ^= y;
	y ^= x;
	x ^= y;
}

__device__ __host__ inline float sgn(float x) {
	return ((x > 0.0f) - (x < 0.0f));
}

__device__ __host__ inline float toFloat1(unsigned int x) {
	return (0.5f + (int) x * 2.328306e-10f);
}

__device__ __host__ inline float toFloat1(int x) {
	return (0.5f + x * 2.328306e-10f);
}

__device__ __host__ inline float toFloat2(unsigned int x) {
	return ((int) x * 4.656612e-10f);
}

__device__ __host__ inline float toFloat2(int x) {
	return (x * 4.656612e-10f);
}
