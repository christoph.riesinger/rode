#pragma once

/*
 * General configuration parameters.
 */
#define TYPE 				double

/*
 * Helper constants for general GPU usage.
 */
#define GLOBAL_SIZE_X		(blockDim.x * gridDim.x)
#define GLOBAL_SIZE_Y		(blockDim.y * gridDim.y)
#define GLOBAL_SIZE_Z		(blockDim.z * gridDim.z)
#define LOCAL_OFFSET_X		(threadIdx.x)
#define LOCAL_OFFSET_Y		(threadIdx.y)
#define LOCAL_OFFSET_Z		(threadIdx.z)
#define GLOBAL_OFFSET_X		(blockIdx.x * blockDim.x + threadIdx.x)
#define GLOBAL_OFFSET_Y		(blockIdx.y * blockDim.y + threadIdx.y)
#define GLOBAL_OFFSET_Z		(blockIdx.z * blockDim.z + threadIdx.z)
#define START_THREAD_ID_1D	(blockIdx.x * blockDim.x)
#define START_THREAD_ID_2D	(blockIdx.y * GLOBAL_SIZE_X + START_THREAD_ID_1D)
#define START_THREAD_ID_3D	(blockIdx.z * GLOBAL_SIZE_Y * GLOBAL_SIZE_X + START_THREAD_ID_2D)
#define LOCAL_THREAD_ID_1D	(threadIdx.x)
#define LOCAL_THREAD_ID_2D	(blockIdx.y * blockDim.x + threadIdx.x)
#define LOCAL_THREAD_ID_3D	(threadIdx.z * blockDim.y * blockDim.x + threadIdx.y * blockDim.x + threadIdx.x)
#define GLOBAL_THREAD_ID_1D	(START_THREAD_ID_1D + threadIdx.x)
#define GLOBAL_THREAD_ID_2D	(START_THREAD_ID_2D + blockIdx.y * GLOBAL_SIZE_X + GLOBAL_OFFSET_X)
#define GLOBAL_THREAD_ID_3D	(START_THREAD_ID_3D + blockIdx.z * GLOBAL_SIZE_Y * GLOBAL_SIZE_X + blockIdx.y * GLOBAL_SIZE_X + GLOBAL_OFFSET_X)

#if defined(__CUDA_ARCH__) && (__CUDA_ARCH__ >= 500)
	#define MAX_NUM_OF_THREADS_PER_DIM_LOG	10
	#define MAX_SIZE_OF_SMEM_PER_BLOCK		(1<<16)
	#define LOG_NUM_BANKS					5
#elif defined(__CUDA_ARCH__) && (__CUDA_ARCH__ >= 200)
	#define MAX_NUM_OF_THREADS_PER_DIM_LOG	10
	#define MAX_SIZE_OF_SMEM_PER_BLOCK		(3 * (1<<14))
	#define LOG_NUM_BANKS					5
#else
	#define MAX_NUM_OF_THREADS_PER_DIM_LOG	9
	#define MAX_SIZE_OF_SMEM_PER_BLOCK		(1<<14)
	#define LOG_NUM_BANKS					4
#endif

#define MAX_NUM_OF_THREADS_PER_DIM	(1<<MAX_NUM_OF_THREADS_PER_DIM_LOG)
#define NUM_BANKS					(1<<LOG_NUM_BANKS)

/*
 * Setup of RODE scenario
 */
#define NUM_OF_MONTE_CARLO_SAMPLES_PER_RANK	(1<<11)
#define END_TIME	 						(1<<7)
// Setup for averaged Euler
/*
#define NUM_OF_COARSE_STEPS_PER_TIME_UNIT	(1<<10)
#define NUM_OF_FINE_STEPS_PER_COARSE_STEP	(NUM_OF_COARSE_STEPS_PER_TIME_UNIT)
*/
// Setup for 3-RODE-Taylor scheme
#define NUM_OF_COARSE_STEPS_PER_TIME_UNIT	(1<<5)
#define NUM_OF_FINE_STEPS_PER_COARSE_STEP	(NUM_OF_COARSE_STEPS_PER_TIME_UNIT * NUM_OF_COARSE_STEPS_PER_TIME_UNIT * NUM_OF_COARSE_STEPS_PER_TIME_UNIT)
#define NUM_OF_TOTAL_COARSE_STEPS			(END_TIME * NUM_OF_COARSE_STEPS_PER_TIME_UNIT)
#define NUM_OF_TOTAL_FINE_STEPS				(END_TIME * NUM_OF_FINE_STEPS_PER_COARSE_STEP * NUM_OF_COARSE_STEPS_PER_TIME_UNIT)
#define COARSE_STEP_SIZE					((TYPE) (1.0 / NUM_OF_COARSE_STEPS_PER_TIME_UNIT))
#define FINE_STEP_SIZE						((TYPE) (1.0 / (NUM_OF_FINE_STEPS_PER_COARSE_STEP * NUM_OF_COARSE_STEPS_PER_TIME_UNIT)))
#define DEGREE_OF_SOLVER_PARALLELISM		1
