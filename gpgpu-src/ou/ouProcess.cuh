#pragma once

#include "ouHelper.h"

template<typename T>
__global__ void realizeOUProcessKernel(
		T *prefixSums,
		const T O_0,
		const T mu,
		const T sigma,
		const unsigned int numOfRealizations,
		T *realization);
