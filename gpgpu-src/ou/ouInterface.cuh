#pragma once

#include "ouHelper.h"
#include "ouProcess.cuh"
#include "ouScan.cuh"

template<typename T>
void realizeOUProcess_D2D(
		T *dRandomNumbers,
		const T stepSize,
		const T O_0,
		const unsigned numOfRealizations,
		T *dRealization,
		cudaStream_t &stream);
template<typename T>
void realizeOUProcess_H2H(
		T *hRandomNumbers,
		const T stepSize,
		const T O_0,
		const unsigned numOfRealizations,
		T *hRealization,
		cudaStream_t &stream);
template<typename T>
void scanOU_D2D(
		T *dInput,
		const T mu,
		const unsigned int arrayLength,
		T *dOutput,
		cudaStream_t &stream);
template<typename T>
void scanOU_H2H(
		T *hInput,
		const T mu,
		const unsigned int arrayLength,
		T *hOutput,
		cudaStream_t &stream);
