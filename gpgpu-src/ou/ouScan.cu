#include "ouScan.cuh"

template<typename T>
__global__ void scanExclusiveOUKernel(
		T *input,
		T mu,
		const unsigned int arrayLength,
		T *output,
		T *offsets) {
	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfElementsPerThreadHalf =
			numOfSMemSlots / (blockDim.x * 2);

	__shared__ T workingArray[numOfSMemSlots];

	unsigned int start, stride, idx;
	T temp, fix;

	start = blockIdx.x * numOfSMemSlots;

	/*
	 * Load data in shared memory
	 */
	for(unsigned int i = 0; i < (numOfElementsPerThreadHalf * 2); i++) {
		idx = i * blockDim.x + LOCAL_THREAD_ID_1D;

		workingArray[OU_CONFLICT_FREE_OFFSET(idx)] =
				(start + idx < arrayLength) ?
				input[start + idx] : (T) 0.0;
	}

	__syncthreads();

	/*
	 * Phase 1:
	 */
	for(int i = 1; i < numOfElementsPerThreadHalf; i++) {
		idx = LOCAL_THREAD_ID_1D * numOfElementsPerThreadHalf + i;

		workingArray[OU_CONFLICT_FREE_OFFSET(idx)] +=
				mu * workingArray[OU_CONFLICT_FREE_OFFSET(idx - 1)];
		workingArray[OU_CONFLICT_FREE_OFFSET((numOfSMemSlots>>1) + idx)] +=
				mu * workingArray[OU_CONFLICT_FREE_OFFSET((numOfSMemSlots>>1) + idx - 1)];
	}

	__syncthreads();

	/*
	 * Phase 2: Going up the tree.
	 */
	for(stride = 1; stride <= blockDim.x; stride <<= 1) {
		idx = (LOCAL_THREAD_ID_1D + 1) * stride * 2 - 1;

		if (idx < 2 * blockDim.x){
			// power(mu, stride * numOfElementsPerThreadHalf, powedMu);
			workingArray[OU_CONFLICT_FREE_OFFSET((idx + 1) * numOfElementsPerThreadHalf - 1)] +=
					power(mu, (T) (stride * numOfElementsPerThreadHalf)) *
					workingArray[OU_CONFLICT_FREE_OFFSET((idx - stride) * numOfElementsPerThreadHalf + numOfElementsPerThreadHalf - 1)];
		}
		__syncthreads();
	}

	/*
	 * Clear the last element. This is essential for exclusive scan.
	 */
	if(LOCAL_THREAD_ID_1D == 0) {
		if(offsets) {
			offsets[blockIdx.x] = workingArray[OU_CONFLICT_FREE_OFFSET(numOfSMemSlots - 1)];
		}
		workingArray[OU_CONFLICT_FREE_OFFSET(numOfSMemSlots - 1)] = (T) 0;
	}
	__syncthreads();

	/*
	 * Phase 3: Going down the tree.
	 */
	for(; stride; stride >>= 1) {
		idx = (LOCAL_THREAD_ID_1D + 1) * stride * 2 - 1;

		if (idx < 2 * blockDim.x){
			// power(mu, stride * numOfElementsPerThreadHalf, powedMu);
			temp =
					workingArray[OU_CONFLICT_FREE_OFFSET(idx * numOfElementsPerThreadHalf + numOfElementsPerThreadHalf - 1)];
			workingArray[OU_CONFLICT_FREE_OFFSET(idx * numOfElementsPerThreadHalf + numOfElementsPerThreadHalf - 1)] =
					workingArray[OU_CONFLICT_FREE_OFFSET((idx - stride + 1) * numOfElementsPerThreadHalf - 1)] +
					power(mu, (T) (stride * numOfElementsPerThreadHalf)) *
					workingArray[OU_CONFLICT_FREE_OFFSET(idx * numOfElementsPerThreadHalf + numOfElementsPerThreadHalf - 1)];
			workingArray[OU_CONFLICT_FREE_OFFSET((idx - stride + 1) * numOfElementsPerThreadHalf - 1)] =
					temp;
		}
		__syncthreads();
	}

	/*
	 * Phase 4:
	 */
	idx = LOCAL_THREAD_ID_1D * numOfElementsPerThreadHalf;
	fix = workingArray[OU_CONFLICT_FREE_OFFSET(idx + numOfElementsPerThreadHalf - 1)];

	for(int i = numOfElementsPerThreadHalf - 1; i >= 1; i--) {
		// power(mu, i, powedMu);
		workingArray[OU_CONFLICT_FREE_OFFSET(idx + i)] =
				workingArray[OU_CONFLICT_FREE_OFFSET(idx + i - 1)] +
				power(mu, (T) i) * fix;
	}
	workingArray[OU_CONFLICT_FREE_OFFSET(idx)] = fix;

	idx = (numOfSMemSlots>>1) + LOCAL_THREAD_ID_1D * numOfElementsPerThreadHalf;
	fix = workingArray[OU_CONFLICT_FREE_OFFSET(idx + numOfElementsPerThreadHalf - 1)];

	for(int i = numOfElementsPerThreadHalf - 1; i >= 1; i--) {
		// power(mu, i, powedMu);
		workingArray[OU_CONFLICT_FREE_OFFSET(idx + i)] =
				workingArray[OU_CONFLICT_FREE_OFFSET(idx + i - 1)] +
				power(mu, (T) i) * fix;
	}
	workingArray[OU_CONFLICT_FREE_OFFSET(idx)] = fix;

	/*
	 * Writing back the result from shared memory to global memory.
	 */
	for(unsigned int i = 0; i < (numOfElementsPerThreadHalf * 2); i++) {
		idx = i * blockDim.x + LOCAL_THREAD_ID_1D;

		if(start + idx < arrayLength) {
			output[start + idx] = workingArray[OU_CONFLICT_FREE_OFFSET(idx)];
		}
	}
}

template<typename T>
__global__ void scanInclusiveOUKernel(
		T *input,
		T mu,
		const unsigned int arrayLength,
		T *output,
		T *offsets) {
	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfElementsPerThreadHalf =
			numOfSMemSlots / (blockDim.x * 2);

	__shared__ T workingArray[numOfSMemSlots];

	unsigned int start, stride, idx;
	T fix;

	start = blockIdx.x * numOfSMemSlots;

	/*
	 * Load data in shared memory
	 */
	for(unsigned int i = 0; i < (numOfElementsPerThreadHalf * 2); i++) {
		idx = i * blockDim.x + LOCAL_THREAD_ID_1D;

		workingArray[OU_CONFLICT_FREE_OFFSET(idx)] =
				(start + idx < arrayLength) ?
				input[start + idx] : (T) 0.0;
	}

	__syncthreads();

	/*
	 * Phase 1:
	 */
	for(int i = 1; i < numOfElementsPerThreadHalf; i++) {
		idx = LOCAL_THREAD_ID_1D * numOfElementsPerThreadHalf + i;

		workingArray[OU_CONFLICT_FREE_OFFSET(idx)] +=
				mu * workingArray[OU_CONFLICT_FREE_OFFSET(idx - 1)];
		workingArray[OU_CONFLICT_FREE_OFFSET((numOfSMemSlots>>1) + idx)] +=
				mu * workingArray[OU_CONFLICT_FREE_OFFSET((numOfSMemSlots>>1) + idx - 1)];
	}

	__syncthreads();


	/*
	 * Phase 2: Going up the tree.
	 */
	for(stride = 1; stride <= blockDim.x; stride <<= 1) {
		idx = (LOCAL_THREAD_ID_1D + 1) * stride * 2 - 1;

		if(idx < 2 * blockDim.x){
			// power(mu, stride * numOfElementsPerThreadHalf, powedMu);
			workingArray[OU_CONFLICT_FREE_OFFSET((idx + 1) * numOfElementsPerThreadHalf - 1)] +=
					power(mu, (T) (stride * numOfElementsPerThreadHalf)) *
					workingArray[OU_CONFLICT_FREE_OFFSET((idx - stride) * numOfElementsPerThreadHalf + numOfElementsPerThreadHalf - 1)];
		}
		__syncthreads();
	}

	/*
	 * Phase 3: Going down the tree.
	 */
	for(stride >>= 1; stride; stride >>= 1) {
		idx = (LOCAL_THREAD_ID_1D + 1) * stride * 2 - 1;

		if(idx + stride < 2 * blockDim.x) {
			// power(mu, stride * numOfElementsPerThreadHalf, powedMu);
			workingArray[OU_CONFLICT_FREE_OFFSET((idx + stride + 1) * numOfElementsPerThreadHalf - 1)] +=
					power(mu, (T) (stride * numOfElementsPerThreadHalf)) *
					workingArray[OU_CONFLICT_FREE_OFFSET(idx * numOfElementsPerThreadHalf + numOfElementsPerThreadHalf - 1)];
		}
		__syncthreads();
	}

	/*
	 * Phase 4:
	 */
	idx = LOCAL_THREAD_ID_1D * numOfElementsPerThreadHalf;
	fix = idx ? workingArray[OU_CONFLICT_FREE_OFFSET(idx - 1)] : (T) 0.0;

	for(int i = 0; i < numOfElementsPerThreadHalf - 1; i++) {
		// power(mu, i + 1, powedMu);
		workingArray[OU_CONFLICT_FREE_OFFSET(idx + i)] += power(mu, (T) (i + 1)) * fix;
	}

	idx = (numOfSMemSlots>>1) + LOCAL_THREAD_ID_1D * numOfElementsPerThreadHalf;
	fix = workingArray[OU_CONFLICT_FREE_OFFSET(idx - 1)];

	for(int i = 0; i < numOfElementsPerThreadHalf - 1; i++) {
		// power(mu, i + 1, powedMu);
		workingArray[OU_CONFLICT_FREE_OFFSET(idx + i)] += power(mu, (T) (i + 1)) * fix;
	}

	/*
	 * Writing back the result from shared memory to global memory.
	 */
	for(unsigned int i = 0; i < (numOfElementsPerThreadHalf * 2); i++) {
		idx = i * blockDim.x + LOCAL_THREAD_ID_1D;

		if(start + idx < arrayLength) {
			output[start + idx] = workingArray[OU_CONFLICT_FREE_OFFSET(idx)];
		}
	}

	if(LOCAL_THREAD_ID_1D == 0 && offsets) {
		offsets[blockIdx.x] = workingArray[OU_CONFLICT_FREE_OFFSET(numOfSMemSlots - 1)];
	}
}

template<typename T>
__global__ void scanOUFixKernel(
		T *input,
		T *offsets,
		const T mu,
		const unsigned int arrayLength) {
	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfElementsPerThread =
			numOfSMemSlots / blockDim.x;

	unsigned int start, idx;
	T offset;

	start = blockIdx.x * numOfSMemSlots;

	if(blockIdx.x != 0) {
		offset = offsets[blockIdx.x - 1];

		for(unsigned int i = 0; i < numOfElementsPerThread; i++) {
			idx = i * blockDim.x + LOCAL_THREAD_ID_1D;

			if((start + idx) < arrayLength) {
				// power(mu, idx + 1, powedMu);
				input[start + idx] +=
						power(mu, (T) (idx + 1)) * offset;
			}
		}
	}
}

#include "ouScanTemplates"
