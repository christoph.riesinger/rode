#pragma once

#include "../helper.h"
#include "ouConstants.h"

#ifdef AVOID_OU_BANK_CONFLICTS
	#define OU_CONFLICT_FREE_OFFSET(n) ((n) + ((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS)))
#else
	#define OU_CONFLICT_FREE_OFFSET(n) (n)
#endif

template<typename T>
__host__ __device__ inline void computeMu(const T h, T &mu) {
	mu = exp(-h);
}

template<typename T>
__host__ __device__ inline void computeSigma(const T h, T &sigma) {
	sigma = sqrt((1 - (exp(-h) * exp(-h))) * 0.5);
}
