#include "ouInterface.cuh"

template<typename T>
void realizeOUProcess_D2D(
		T *dRandomNumbers,
		const T O_0,
		const T stepSize,
		const unsigned int numOfRealizations,
		T *dRealization,
		cudaStream_t &stream) {
	assert(OU_SMEM_PER_BLOCK % sizeof(T) == 0);

	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfRealizationsPerChunk =
			numOfSMemSlots * OU_SMEM_PER_BLOCK;
	const unsigned int ouNumOfBlocks =
			divUp(numOfRealizations, numOfSMemSlots);

	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be a multiple of OU_NUM_OF_THREADS * 2 because otherwise the tree
	 * like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots % (OU_NUM_OF_THREADS * 2) == 0);
	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be at least twice the number of threads per block because
	 * otherwise the parallel load from global to shared memory does not work
	 * and the tree like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots >= (OU_NUM_OF_THREADS * 2));

	T *dPrefixSums, *dOffsets;
	unsigned int numOfChunks, lengthOfLastChunk;
	T O_current, mu, sigma;

	GPU_ERROR_CHECK(cudaMalloc(&dPrefixSums, min(numOfRealizations, numOfRealizationsPerChunk) * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOffsets, ouNumOfBlocks * sizeof(T)))

	numOfChunks = divUp(numOfRealizations, numOfRealizationsPerChunk);
	lengthOfLastChunk = modUp(numOfRealizations, numOfRealizationsPerChunk);
	O_current = O_0;
	computeMu<T>(stepSize, mu);
	computeSigma<T>(stepSize, sigma);

	/*
	std::cout << "OU_SMEM_PER_BLOCK =         " << (OU_SMEM_PER_BLOCK>>10) << "KB" << std::endl;
	std::cout << "numOfSMemSlots =            " << numOfSMemSlots << std::endl;
	std::cout << "numOfRealizations =         " << numOfRealizations << std::endl;
	std::cout << "numOfRealizationsPerChunk = " << numOfRealizationsPerChunk << std::endl;
	std::cout << "sizeOfChunk =               " << ((numOfRealizationsPerChunk * sizeof(T))>>20) << "MB" << std::endl;
	std::cout << "OU_NUM_OF_THREADS =         " << OU_NUM_OF_THREADS << std::endl;
	std::cout << "ouNumOfBlocks =             " << ouNumOfBlocks << std::endl;
	std::cout << "numOfElementsPerThread =    " << (numOfSMemSlots>>OU_NUM_OF_THREADS_LOG) << std::endl;
	std::cout << "numOfChunks =               " << numOfChunks << std::endl;
	*/

	for(unsigned int i = 0; i < numOfChunks - 1; i++) {
		scanExclusiveOUKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
				&dRandomNumbers[i * numOfRealizationsPerChunk],
				mu,
				numOfRealizationsPerChunk,
				dPrefixSums,
				dOffsets);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
		scanExclusiveOUKernel<T><<<1, OU_NUM_OF_THREADS, 0, stream>>>(
				dOffsets,
				pow(mu, numOfSMemSlots),
				ouNumOfBlocks,
				dOffsets,
				0);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
		scanOUFixKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
				dPrefixSums,
				dOffsets,
				mu,
				numOfRealizationsPerChunk);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
		realizeOUProcessKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
				dPrefixSums,
				O_current,
				mu,
				sigma,
				numOfRealizationsPerChunk,
				&dRealization[i * numOfRealizationsPerChunk]);
		GPU_ERROR_CHECK(cudaPeekAtLastError())

		GPU_ERROR_CHECK(cudaMemcpyAsync(&O_current, &dRealization[(i + 1) * numOfRealizationsPerChunk - 1], sizeof(T), cudaMemcpyDeviceToHost, stream))
	}

	scanExclusiveOUKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
			&dRandomNumbers[(numOfChunks - 1) * numOfRealizationsPerChunk],
			mu,
			lengthOfLastChunk,
			dPrefixSums,
			dOffsets);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
	scanExclusiveOUKernel<T><<<1, OU_NUM_OF_THREADS, 0, stream>>>(
			dOffsets,
			pow(mu, numOfSMemSlots),
			ouNumOfBlocks,
			dOffsets,
			0);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
	scanOUFixKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
			dPrefixSums,
			dOffsets,
			mu,
			lengthOfLastChunk);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
	realizeOUProcessKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
			dPrefixSums,
			O_current,
			mu,
			sigma,
			lengthOfLastChunk,
			&dRealization[(numOfChunks - 1) * numOfRealizationsPerChunk]);
	GPU_ERROR_CHECK(cudaPeekAtLastError())

	GPU_ERROR_CHECK(cudaFree(dOffsets))
	GPU_ERROR_CHECK(cudaFree(dPrefixSums))
}

template<typename T>
void realizeOUProcess_H2H(
		T *hRandomNumbers,
		const T O_0,
		const T stepSize,
		const unsigned int numOfRealizations,
		T *hRealization,
		cudaStream_t &stream) {
	assert(OU_SMEM_PER_BLOCK % sizeof(T) == 0);

	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);

	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be a multiple of OU_NUM_OF_THREADS * 2 because otherwise the tree
	 * like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots % (OU_NUM_OF_THREADS * 2) == 0);
	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be at least twice the number of threads per block because
	 * otherwise the parallel load from global to shared memory does not work
	 * and the tree like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots >= (OU_NUM_OF_THREADS * 2));

	T *dRandomNumbers, *dRealization;

	GPU_ERROR_CHECK(cudaMalloc(&dRandomNumbers, numOfRealizations * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dRealization, numOfRealizations * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(dRandomNumbers, hRandomNumbers, numOfRealizations * sizeof(T), cudaMemcpyHostToDevice, stream))

	realizeOUProcess_D2D<T>(dRandomNumbers, O_0, stepSize, numOfRealizations, dRealization, stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(hRealization, dRealization, numOfRealizations * sizeof(T), cudaMemcpyDeviceToHost, stream))

	GPU_ERROR_CHECK(cudaFree(dRealization))
	GPU_ERROR_CHECK(cudaFree(dRandomNumbers))
}

template<typename T>
void scanOU_D2D(
		T *dInput,
		const T mu,
		const unsigned int arrayLength,
		T *dOutput,
		cudaStream_t &stream) {
	assert(OU_SMEM_PER_BLOCK % sizeof(T) == 0);

	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int ouNumOfBlocks =
			divUp(arrayLength, numOfSMemSlots);

	/*
	 * The number of all elements which should be scanned has to be small enough
	 * so the two level scan approach works: First, partial scans are executed.
	 * Second, these partial sums are fixed by the scans of the previous partial
	 * sums.
	 */
	assert(arrayLength <= (numOfSMemSlots * OU_SMEM_PER_BLOCK));
	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be a multiple of OU_NUM_OF_THREADS * 2 because otherwise the tree
	 * like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots % (OU_NUM_OF_THREADS * 2) == 0);
	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be at least twice the number of threads per block because
	 * otherwise the parallel load from global to shared memory does not work
	 * and the tree like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots >= (OU_NUM_OF_THREADS * 2));

	T *dOffsets;

	GPU_ERROR_CHECK(cudaMalloc(&dOffsets, ouNumOfBlocks * sizeof(T)))

	scanExclusiveOUKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
			dInput,
			mu,
			arrayLength,
			dOutput,
			dOffsets);
	GPU_ERROR_CHECK(cudaPeekAtLastError())
	if(arrayLength > 2 * OU_NUM_OF_THREADS) {
		scanExclusiveOUKernel<T><<<1, OU_NUM_OF_THREADS, 0, stream>>>(
				dOffsets,
				pow(mu, numOfSMemSlots),
				ouNumOfBlocks,
				dOffsets,
				0);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
		scanOUFixKernel<T><<<ouNumOfBlocks, OU_NUM_OF_THREADS, 0, stream>>>(
				dOutput,
				dOffsets,
				mu,
				arrayLength);
		GPU_ERROR_CHECK(cudaPeekAtLastError())
	}

	GPU_ERROR_CHECK(cudaFree(dOffsets))
}

template<typename T>
void scanOU_H2H(
		T *hInput,
		const T mu,
		const unsigned int arrayLength,
		T *hOutput,
		cudaStream_t &stream) {
	assert(OU_SMEM_PER_BLOCK % sizeof(T) == 0);

	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);

	/*
	 * The number of all elements which should be scanned has to be small enough
	 * so the two level scan approach works: First, partial scans are executed.
	 * Second, these partial sums are fixed by the scans of the previous partial
	 * sums.
	 */
	assert(arrayLength <= (numOfSMemSlots * OU_SMEM_PER_BLOCK));
	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be a multiple of OU_NUM_OF_THREADS * 2 because otherwise the tree
	 * like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots % (OU_NUM_OF_THREADS * 2) == 0);
	/*
	 * The number of all elements which are stored in shared memory per block
	 * have to be at least twice the number of threads per block because
	 * otherwise the parallel load from global to shared memory does not work
	 * and the tree like parallel scan approach does not work properly.
	 */
	assert(numOfSMemSlots >= (OU_NUM_OF_THREADS * 2));

	T *dInput, *dOutput;

	GPU_ERROR_CHECK(cudaStreamCreate(&stream))
	GPU_ERROR_CHECK(cudaMalloc(&dInput, arrayLength * sizeof(T)))
	GPU_ERROR_CHECK(cudaMalloc(&dOutput, arrayLength * sizeof(T)))

	GPU_ERROR_CHECK(cudaMemcpyAsync(dInput, hInput, arrayLength * sizeof(T), cudaMemcpyHostToDevice, stream))

	scanOU_D2D<T>(dInput, mu, arrayLength, dOutput, stream);

	GPU_ERROR_CHECK(cudaMemcpyAsync(hOutput, dOutput, arrayLength * sizeof(T), cudaMemcpyDeviceToHost, stream))

	GPU_ERROR_CHECK(cudaFree(dOutput))
	GPU_ERROR_CHECK(cudaFree(dInput))
}

#include "ouInterfaceTemplates"
