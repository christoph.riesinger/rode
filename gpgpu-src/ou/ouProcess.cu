#include "ouProcess.cuh"

template<typename T>
__global__ void realizeOUProcessKernel(
		T *prefixSums,
		const T O_0,
		const T mu,
		const T sigma,
		const unsigned int numOfRealizations,
		T *realization) {
	const unsigned int numOfSMemSlots =
			OU_SMEM_PER_BLOCK / sizeof(T);
	const unsigned int numOfElementsPerThread =
			numOfSMemSlots / blockDim.x;

	unsigned int start, idx;

	start = blockIdx.x * numOfSMemSlots;

	for(unsigned int i = 0; i < numOfElementsPerThread; i++) {
		idx = i * blockDim.x + LOCAL_THREAD_ID_1D;

		if((start + idx) < numOfRealizations) {
			realization[start + idx] =
					powf(mu, start + idx) * O_0 +
					sigma * prefixSums[start + idx];
		}
	}
}

#include "ouProcessTemplates"
