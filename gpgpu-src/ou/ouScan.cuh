#pragma once

#include "ouHelper.h"

template<typename T>
__global__ void scanExclusiveOUKernel(
		T *input,
		T mu,
		const unsigned int arrayLength,
		T *output,
		T *offsetArray);
template<typename T>
__global__ void scanInclusiveOUKernel(
		T *input,
		T mu,
		const unsigned int arrayLength,
		T *output,
		T *offsetArray);
template<typename T>
__global__ void scanOUFixKernel(
		T *input,
		T *offsetArray,
		const T mu,
		const unsigned int arrayLength);
