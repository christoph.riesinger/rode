#pragma once

#define OU_NUM_OF_THREADS_LOG	9
#define OU_NUM_OF_THREADS		(1<<OU_NUM_OF_THREADS_LOG)
#define OU_SMEM_PER_BLOCK		(1<<12) * sizeof(TYPE)

// #define AVOID_OU_BANK_CONFLICTS
