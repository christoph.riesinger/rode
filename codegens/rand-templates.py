#!/usr/bin/python
datatypes                   = ["float", "double"]
# uniformSerialGenerators	= ["curandStateMRG32k3a_t", "curandStatePhilox4_32_10_t", "curandStateXORWOW_t"]
uniformSerialGenerators		= ["curandStateXORWOW_t"]
uniformParallelGenerators	= ["RandStatePhilox"]
uniformGenerators			= uniformSerialGenerators + uniformParallelGenerators
normalSerialGenerators		= ["RandStateBoxMuller", "RandStateInvCDF", "RandStateWichura"]
normalSharedGenerators		= ["RandStateZiggurat"]
normalParallelGenerators	= ["RandStateWallace"]
normalGenerators			= normalSerialGenerators + normalSharedGenerators + normalParallelGenerators
# curandGenerators			= ["curandStateMRG32k3a_t", "curandStatePhilox4_32_10_t", "curandStateXORWOW_t"]
curandGenerators			= ["curandStateXORWOW_t"]

# ==============================================================================
# 
# ==============================================================================
def printInitStatesNormal_DTemplates():
	code = ""
	for normal in normalSerialGenerators:
		for uniform in uniformSerialGenerators:
			code += "template void initStatesNormal_D<UniformSerialNormalSerialConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformSerialNormalSerialConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
	for normal in normalSharedGenerators:
		for uniform in uniformSerialGenerators:
			code += "template void initStatesNormal_D<UniformSerialNormalSharedConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformSerialNormalSharedConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
	for normal in normalParallelGenerators:
		for uniform in uniformSerialGenerators:
			code += "template void initStatesNormal_D<UniformSerialNormalParallelConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformSerialNormalParallelConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
	for normal in normalSerialGenerators:
		for uniform in uniformParallelGenerators:
			code += "template void initStatesNormal_D<UniformParallelNormalSerialConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformParallelNormalSerialConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
#	for normal in normalSharedGenerators:
#		for uniform in uniformParallelGenerators:
#			code += "template void initStatesNormal_D<UniformParallelNormalSharedConfiguration, " + uniform + ", " + normal + ">(\n"
#			code += "\t\tconst unsigned long long seed,\n"
#			code += "\t\tconst unsigned int numOfConfigurations,\n"
#			code += "\t\tUniformParallelNormalSharedConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
#			code += "\t\tcudaStream_t &stream);\n"
#	for normal in normalParallelGenerators:
#		for uniform in uniformParallelGenerators:
#			code += "template void initStatesNormal_D<UniformParallelNormalParallelConfiguration, " + uniform + ", " + normal + ">(\n"
#			code += "\t\tconst unsigned long long seed,\n"
#			code += "\t\tconst unsigned int numOfConfigurations,\n"
#			code += "\t\tUniformParallelNormalParallelConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
#			code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code
	
def printInitStatesNormal_HTemplates():
	code = ""
	for normal in normalSerialGenerators:
		for uniform in uniformSerialGenerators:
			code += "template void initStatesNormal_H<UniformSerialNormalSerialConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformSerialNormalSerialConfiguration<" + uniform + ", " + normal + "> *hConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
	for normal in normalSharedGenerators:
		for uniform in uniformSerialGenerators:
			code += "template void initStatesNormal_H<UniformSerialNormalSharedConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformSerialNormalSharedConfiguration<" + uniform + ", " + normal + "> *hConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
	for normal in normalParallelGenerators:
		for uniform in uniformSerialGenerators:
			code += "template void initStatesNormal_H<UniformSerialNormalParallelConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformSerialNormalParallelConfiguration<" + uniform + ", " + normal + "> *hConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
	for normal in normalSerialGenerators:
		for uniform in uniformParallelGenerators:
			code += "template void initStatesNormal_H<UniformParallelNormalSerialConfiguration, " + uniform + ", " + normal + ">(\n"
			code += "\t\tconst unsigned long long seed,\n"
			code += "\t\tconst unsigned int numOfConfigurations,\n"
			code += "\t\tUniformParallelNormalSerialConfiguration<" + uniform + ", " + normal + "> *hConfigurations,\n"
			code += "\t\tcudaStream_t &stream);\n"
#	for normal in normalSharedGenerators:
#		for uniform in uniformParallelGenerators:
#			code += "template void initStatesNormal_H<UniformParallelNormalSharedConfiguration, " + uniform + ", " + normal + ">(\n"
#			code += "\t\tconst unsigned long long seed,\n"
#			code += "\t\tconst unsigned int numOfConfigurations,\n"
#			code += "\t\tUniformParallelNormalSharedConfiguration<" + uniform + ", " + normal + "> *hConfigurations,\n"
#			code += "\t\tcudaStream_t &stream);\n"
#	for normal in normalParallelGenerators:
#		for uniform in uniformParallelGenerators:
#			code += "template void initStatesNormal_H<UniformParallelNormalParallelConfiguration, " + uniform + ", " + normal + ">(\n"
#			code += "\t\tconst unsigned long long seed,\n"
#			code += "\t\tconst unsigned int numOfConfigurations,\n"
#			code += "\t\tUniformParallelNormalParallelConfiguration<" + uniform + ", " + normal + "> *hConfigurations,\n"
#			code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code
	
def printInitStatesUniform_DTemplates():
	code = ""
	for uniform in uniformSerialGenerators:
		code += "template void initStatesUniform_D<UniformSerialConfiguration, " + uniform + ">(\n"
		code += "\t\tconst unsigned long long seed,\n"
		code += "\t\tconst unsigned int numOfConfigurations,\n"
		code += "\t\tUniformSerialConfiguration<" + uniform + "> *dConfigurations,\n"
		code += "\t\tcudaStream_t &stream);\n"
	for uniform in uniformParallelGenerators:
		code += "template void initStatesUniform_D<UniformParallelConfiguration, " + uniform + ">(\n"
		code += "\t\tconst unsigned long long seed,\n"
		code += "\t\tconst unsigned int numOfConfigurations,\n"
		code += "\t\tUniformParallelConfiguration<" + uniform + "> *dConfigurations,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code
	
def printInitStatesUniform_HTemplates():
	code = ""
	for uniform in uniformSerialGenerators:
		code += "template void initStatesUniform_H<UniformSerialConfiguration, " + uniform + ">(\n"
		code += "\t\tconst unsigned long long seed,\n"
		code += "\t\tconst unsigned int numOfConfigurations,\n"
		code += "\t\tUniformSerialConfiguration<" + uniform + "> *hConfigurations,\n"
		code += "\t\tcudaStream_t &stream);\n"
	for uniform in uniformParallelGenerators:
		code += "template void initStatesUniform_H<UniformParallelConfiguration, " + uniform + ">(\n"
		code += "\t\tconst unsigned long long seed,\n"
		code += "\t\tconst unsigned int numOfConfigurations,\n"
		code += "\t\tUniformParallelConfiguration<" + uniform + "> *hConfigurations,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printGetRandomNumbersNormal_DTemplates():
	code = ""
	for datatype in datatypes:
		for normal in normalSerialGenerators:
			for uniform in uniformSerialGenerators:
				code += "template void getRandomNumbersNormal_D<UniformSerialNormalSerialConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformSerialNormalSerialConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *dNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
		for normal in normalSharedGenerators:
			for uniform in uniformSerialGenerators:
				code += "template void getRandomNumbersNormal_D<UniformSerialNormalSharedConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformSerialNormalSharedConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *dNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
		for normal in normalParallelGenerators:
			for uniform in uniformSerialGenerators:
				code += "template void getRandomNumbersNormal_D<UniformSerialNormalParallelConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformSerialNormalParallelConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *dNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
		for normal in normalSerialGenerators:
			for uniform in uniformParallelGenerators:
				code += "template void getRandomNumbersNormal_D<UniformParallelNormalSerialConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformParallelNormalSerialConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *dNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
#		for normal in normalSharedGenerators:
#			for uniform in uniformParallelGenerators:
#				code += "template void getRandomNumbersNormal_D<UniformParallelNormalSharedConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
#				code += "\t\tUniformParallelNormalSharedConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
#				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
#				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
#				code += "\t\t" + datatype + " *dNormalRandomNumbers,\n"
#				code += "\t\tcudaStream_t &stream);\n"
#		for normal in normalParallelGenerators:
#			for uniform in uniformParallelGenerators:
#				code += "template void getRandomNumbersNormal_D<UniformParallelNormalParallelConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
#				code += "\t\tUniformParallelNormalParallelConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
#				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
#				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
#				code += "\t\t" + datatype + " *dNormalRandomNumbers,\n"
#				code += "\t\tcudaStream_t &stream);\n"
		code += "\n"
	code += "\n"
	return code

def printGetRandomNumbersNormal_HTemplates():
	code = ""
	for datatype in datatypes:
		for normal in normalSerialGenerators:
			for uniform in uniformSerialGenerators:
				code += "template void getRandomNumbersNormal_H<UniformSerialNormalSerialConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformSerialNormalSerialConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *hNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
		for normal in normalSharedGenerators:
			for uniform in uniformSerialGenerators:
				code += "template void getRandomNumbersNormal_H<UniformSerialNormalSharedConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformSerialNormalSharedConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *hNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
		for normal in normalParallelGenerators:
			for uniform in uniformSerialGenerators:
				code += "template void getRandomNumbersNormal_H<UniformSerialNormalParallelConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformSerialNormalParallelConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *hNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
		for normal in normalSerialGenerators:
			for uniform in uniformParallelGenerators:
				code += "template void getRandomNumbersNormal_H<UniformParallelNormalSerialConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
				code += "\t\tUniformParallelNormalSerialConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
				code += "\t\t" + datatype + " *hNormalRandomNumbers,\n"
				code += "\t\tcudaStream_t &stream);\n"
#		for normal in normalSharedGenerators:
#			for uniform in uniformParallelGenerators:
#				code += "template void getRandomNumbersNormal_H<UniformParallelNormalSharedConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
#				code += "\t\tUniformParallelNormalSharedConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
#				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
#				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
#				code += "\t\t" + datatype + " *hNormalRandomNumbers,\n"
#				code += "\t\tcudaStream_t &stream);\n"
#		for normal in normalParallelGenerators:
#			for uniform in uniformParallelGenerators:
#				code += "template void getRandomNumbersNormal_H<UniformParallelNormalParallelConfiguration, " + uniform + ", " + normal + ", " + datatype + ">(\n"
#				code += "\t\tUniformParallelNormalParallelConfiguration<" + uniform + ", " + normal + "> *dConfigurations,\n"
#				code += "\t\tconst unsigned long long numOfNormalRandomNumbers,\n"
#				code += "\t\tconst unsigned int numOfNormalRandomNumbersPerThread,\n"
#				code += "\t\t" + datatype + " *hNormalRandomNumbers,\n"
#				code += "\t\tcudaStream_t &stream);\n"
		code += "\n"
	code += "\n"
	return code

def printGetRandomNumbersUniform_DTemplates():
	code = ""
	for uniform in uniformSerialGenerators:
		code += "template void getRandomNumbersUniform_D<UniformSerialConfiguration, " + uniform + ">(\n"
		code += "\t\tUniformSerialConfiguration<" + uniform + "> *dConfigurations,\n"
		code += "\t\tconst unsigned long long numOfUniformRandomNumbers,\n"
		code += "\t\tconst unsigned int numOfUniformRandomNumbersPerThread,\n"
		code += "\t\tunsigned int *dUniformRandomNumbers,\n"
		code += "\t\tcudaStream_t &stream);\n"
	for uniform in uniformParallelGenerators:
		code += "template void getRandomNumbersUniform_D<UniformParallelConfiguration, " + uniform + ">(\n"
		code += "\t\tUniformParallelConfiguration<" + uniform + "> *dConfigurations,\n"
		code += "\t\tconst unsigned long long numOfUniformRandomNumbers,\n"
		code += "\t\tconst unsigned int numOfUniformRandomNumbersPerThread,\n"
		code += "\t\tunsigned int *dUniformRandomNumbers,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printGetRandomNumbersUniform_HTemplates():
	code = ""
	for uniform in uniformSerialGenerators:
		code += "template void getRandomNumbersUniform_H<UniformSerialConfiguration, " + uniform + ">(\n"
		code += "\t\tUniformSerialConfiguration<" + uniform + "> *dConfigurations,\n"
		code += "\t\tconst unsigned long long numOfUniformRandomNumbers,\n"
		code += "\t\tconst unsigned int numOfUniformRandomNumbersPerThread,\n"
		code += "\t\tunsigned int *hUniformRandomNumbers,\n"
		code += "\t\tcudaStream_t &stream);\n"
	for uniform in uniformParallelGenerators:
		code += "template void getRandomNumbersUniform_H<UniformParallelConfiguration, " + uniform + ">(\n"
		code += "\t\tUniformParallelConfiguration<" + uniform + "> *dConfigurations,\n"
		code += "\t\tconst unsigned long long numOfUniformRandomNumbers,\n"
		code += "\t\tconst unsigned int numOfUniformRandomNumbersPerThread,\n"
		code += "\t\tunsigned int *hUniformRandomNumbers,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================
def printRandInitUniformProxy():
	code = ""
	for uniform in curandGenerators:
		code += "__device__ inline void randInitUniform(unsigned long long seed, " + uniform + " &state) {\n"
		code += "\tcurand_init(seed, blockIdx.x * blockDim.x + threadIdx.x, 0, &state);\n"
		code += "}\n"
	code += "\n"
	return code

def printRandUniformProxy():
	code = ""
	for uniform in curandGenerators:
		code += "__device__ inline unsigned int randUniform(" + uniform + " &state) {\n"
		code += "\treturn curand(&state);\n"
		code += "}\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================

def printRandNormalTemplates():
	code = ""
	for normal in normalSerialGenerators:
		for uniform in uniformGenerators:
			code += "template __device__ float randNormal<" + uniform + ">(\n"
			code += "\t\t" + uniform + " &uniformState,\n"
			code += "\t\t" + normal + " &normalState);\n"
	for normal in normalSharedGenerators:
		for uniform in uniformGenerators:
			code += "template __device__ float randNormal<" + uniform + ">(\n"
			code += "\t\t" + uniform + " &uniformState,\n"
			code += "\t\t" + normal + " &normalState);\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================

def printRandNormalParallelTemplates():
	code = ""
	for normal in normalParallelGenerators:
		for uniform in uniformGenerators:
			code += "template __device__ float randNormal<" + uniform + ">(\n"
			code += "\t\t" + uniform + " &uniformState,\n"
			code += "\t\t" + normal + " &normalState);\n"
	code += "\n"
	return code

randInterfaceTemplates = open("../gpgpu-src/rand/randInterfaceTemplates", "w")
randInterfaceTemplates.write(printInitStatesNormal_DTemplates())
randInterfaceTemplates.write(printInitStatesNormal_HTemplates())
randInterfaceTemplates.write(printInitStatesUniform_DTemplates())
randInterfaceTemplates.write(printInitStatesUniform_HTemplates())
randInterfaceTemplates.write(printGetRandomNumbersNormal_DTemplates())
randInterfaceTemplates.write(printGetRandomNumbersNormal_HTemplates())
randInterfaceTemplates.write(printGetRandomNumbersUniform_DTemplates())
randInterfaceTemplates.write(printGetRandomNumbersUniform_HTemplates())
randInterfaceTemplates.close()

randUniformProxy = open("../gpgpu-src/rand/randUniformProxy", "w")
randUniformProxy.write(printRandInitUniformProxy())
randUniformProxy.write(printRandUniformProxy())
randUniformProxy.close()

randNormalTemplates = open("../gpgpu-src/rand/randNormalTemplates", "w")
randNormalTemplates.write(printRandNormalTemplates())
randNormalTemplates.close()

randNormalParallelTemplates = open("../gpgpu-src/rand/randNormalParallelTemplates", "w")
randNormalParallelTemplates.write(printRandNormalParallelTemplates())
randNormalParallelTemplates.close()

