#!/usr/bin/python
datatypes = ["float", "double"]

# ==============================================================================
# 
# ==============================================================================

def printSingleAverage_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void singleAverage_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dInput,\n"
		code += "\t\tconst unsigned int numOfAllElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerSingleAverage,\n"
		code += "\t\tconst unsigned int numOfSingleAverages,\n"
		code += "\t\t" + datatype + " *dOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printSingleAverage_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void singleAverage_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hInput,\n"
		code += "\t\tconst unsigned int numOfAllElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerSingleAverage,\n"
		code += "\t\tconst unsigned int numOfSingleAverages,\n"
		code += "\t\t" + datatype + " *hOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printDoubleAverage_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void doubleAverage_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dInput,\n"
		code += "\t\tconst unsigned int numOfAllElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerDoubleAverage,\n"
		code += "\t\tconst unsigned int numOfDoubleAverages,\n"
		code += "\t\t" + datatype + " *dOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printDoubleAverage_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void doubleAverage_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hInput,\n"
		code += "\t\tconst unsigned int numOfAllElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerDoubleAverage,\n"
		code += "\t\tconst unsigned int numOfDoubleAverages,\n"
		code += "\t\t" + datatype + " *hOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printTridiagIntegralApproximation_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void tridiagIntegralApproximation_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dInput,\n"
		code += "\t\tconst " + datatype + " initElement,\n"
		code += "\t\tconst unsigned int numOfAllElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerTridiagIntegral,\n"
		code += "\t\tconst unsigned int numOfTridiagIntegrals,\n"
		code += "\t\tconst unsigned int d,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\tconst " + datatype + " fineStepSize,\n"
		code += "\t\t" + datatype + " *dOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printTridiagIntegralApproximation_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void tridiagIntegralApproximation_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hInput,\n"
		code += "\t\tconst " + datatype + " initElement,\n"
		code += "\t\tconst unsigned int numOfAllElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerTridiagIntegral,\n"
		code += "\t\tconst unsigned int numOfTridiagIntegrals,\n"
		code += "\t\tconst unsigned int d,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\tconst " + datatype + " fineStepSize,\n"
		code += "\t\t" + datatype + " *hOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printAveragedEuler_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void averagedEuler_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dZ1Init,\n"
		code += "\t\tconst " + datatype + " *dZ2Init,\n"
		code += "\t\tconst " + datatype + " *dOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *dOuSingleAverages,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *dZ1,\n"
		code += "\t\t" + datatype + " *dZ2,\n"
		code += "\t\t" + datatype + " *dDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printAveragedEuler_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void averagedEuler_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hZ1Init,\n"
		code += "\t\tconst " + datatype + " *hZ2Init,\n"
		code += "\t\tconst " + datatype + " *hOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *hOuSingleAverages,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *hZ1,\n"
		code += "\t\t" + datatype + " *hZ2,\n"
		code += "\t\t" + datatype + " *hDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printAveragedHeun_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void averagedHeun_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dZ1Init,\n"
		code += "\t\tconst " + datatype + " *dZ2Init,\n"
		code += "\t\tconst " + datatype + " *dOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *dOuSingleAverages,\n"
		code += "\t\tconst " + datatype + " *dOuDoubleAverages,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *dZ1,\n"
		code += "\t\t" + datatype + " *dZ2,\n"
		code += "\t\t" + datatype + " *dDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printAveragedHeun_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void averagedHeun_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hZ1Init,\n"
		code += "\t\tconst " + datatype + " *hZ2Init,\n"
		code += "\t\tconst " + datatype + " *hOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *hOuSingleAverages,\n"
		code += "\t\tconst " + datatype + " *hOuDoubleAverages,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *hZ1,\n"
		code += "\t\t" + datatype + " *hZ2,\n"
		code += "\t\t" + datatype + " *hDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printTaylor3_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void taylor3_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dZ1Init,\n"
		code += "\t\tconst " + datatype + " *dZ2Init,\n"
		code += "\t\tconst " + datatype + " *dOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *dOuLineIntegrals,\n"
		code += "\t\tconst " + datatype + " *dOuTriaIntegrals,\n"
		code += "\t\tconst " + datatype + " *dOuTetrahIntegrals,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *dZ1,\n"
		code += "\t\t" + datatype + " *dZ2,\n"
		code += "\t\t" + datatype + " *dDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printTaylor3_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void taylor3_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hZ1Init,\n"
		code += "\t\tconst " + datatype + " *hZ2Init,\n"
		code += "\t\tconst " + datatype + " *hOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *hOuLineIntegrals,\n"
		code += "\t\tconst " + datatype + " *hOuTriaIntegrals,\n"
		code += "\t\tconst " + datatype + " *hOuTetrahIntegrals,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *hZ1,\n"
		code += "\t\t" + datatype + " *hZ2,\n"
		code += "\t\t" + datatype + " *hDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printTaylor4_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void taylor4_D2D<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *dZ1Init,\n"
		code += "\t\tconst " + datatype + " *dZ2Init,\n"
		code += "\t\tconst " + datatype + " *dOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *dOuLineIntegrals,\n"
		code += "\t\tconst " + datatype + " *dOuTriaIntegrals,\n"
		code += "\t\tconst " + datatype + " *dOuTetrahIntegrals,\n"
		code += "\t\tconst " + datatype + " *dOuPentacIntegral,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *dZ1,\n"
		code += "\t\t" + datatype + " *dZ2,\n"
		code += "\t\t" + datatype + " *dDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printTaylor4_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void taylor4_H2H<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *hZ1Init,\n"
		code += "\t\tconst " + datatype + " *hZ2Init,\n"
		code += "\t\tconst " + datatype + " *hOuRealization,\n"
		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *hOuLineIntegrals,\n"
		code += "\t\tconst " + datatype + " *hOuTriaIntegrals,\n"
		code += "\t\tconst " + datatype + " *hOuTetrahIntegrals,\n"
		code += "\t\tconst " + datatype + " *hOuPentacIntegral,\n"
		code += "\t\tconst unsigned int numOfTotalCoarseSteps,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *hZ1,\n"
		code += "\t\t" + datatype + " *hZ2,\n"
		code += "\t\t" + datatype + " *hDdu,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================


def printAveragedEulerKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void averagedEulerKernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *z1Init,\n"
		code += "\t\tconst " + datatype + " *z2Init,\n"
		code += "\t\tconst " + datatype + " *ouRealization,\n"
#		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *ouSingleAverages,\n"
		code += "\t\tconst unsigned int numOfCoarseStepsPerThread,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *z1,\n"
		code += "\t\t" + datatype + " *z2,\n"
		code += "\t\t" + datatype + " *ddu);\n"
	code += "\n"
	return code

def printAveragedHeunKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void averagedHeunKernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *z1Init,\n"
		code += "\t\tconst " + datatype + " *z2Init,\n"
		code += "\t\tconst " + datatype + " *ouRealization,\n"
#		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *ouSingleAverages,\n"
		code += "\t\tconst " + datatype + " *ouDoubleAverages,\n"
		code += "\t\tconst unsigned int numOfCoarseStepsPerThread,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *z1,\n"
		code += "\t\t" + datatype + " *z2,\n"
		code += "\t\t" + datatype + " *ddu);\n"
	code += "\n"
	return code

def printTaylor3KernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void taylor3Kernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *z1Init,\n"
		code += "\t\tconst " + datatype + " *z2Init,\n"
		code += "\t\tconst " + datatype + " *ouRealization,\n"
#		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *ouLineIntegrals,\n"
		code += "\t\tconst " + datatype + " *ouTriaIntegrals,\n"
		code += "\t\tconst " + datatype + " *ouTetrahIntegrals,\n"
		code += "\t\tconst unsigned int numOfCoarseStepsPerThread,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *z1,\n"
		code += "\t\t" + datatype + " *z2,\n"
		code += "\t\t" + datatype + " *ddu);\n"
	code += "\n"
	return code

def printTaylor4KernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void taylor4Kernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *z1Init,\n"
		code += "\t\tconst " + datatype + " *z2Init,\n"
		code += "\t\tconst " + datatype + " *ouRealization,\n"
#		code += "\t\tconst unsigned int ouStride,\n"
		code += "\t\tconst " + datatype + " *ouLineIntegrals,\n"
		code += "\t\tconst " + datatype + " *ouTriaIntegrals,\n"
		code += "\t\tconst " + datatype + " *ouTetrahIntegrals,\n"
		code += "\t\tconst " + datatype + " *ouPentacIntegrals,\n"
		code += "\t\tconst unsigned int numOfCoarseStepsPerThread,\n"
		code += "\t\tconst unsigned int degreeOfSolverParallelism,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\t" + datatype + " *z1,\n"
		code += "\t\t" + datatype + " *z2,\n"
		code += "\t\t" + datatype + " *ddu);\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================

def printSumKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void sumKernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *elementsToSum,\n"
		code += "\t\tconst unsigned int numOfElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerSum,\n"
		code += "\t\t" + datatype + " *sums);\n"
	code += "\n"
	return code
	
def printSingleAverageKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void singleAverageKernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *elementsToAverage,\n"
		code += "\t\tconst unsigned int numOfElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerAverage,\n"
		code += "\t\tconst unsigned int N,\n"
		code += "\t\t" + datatype + " *averages);\n"
	code += "\n"
	return code

def printDoubleAverageKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void doubleAverageKernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *elementsToAverage,\n"
		code += "\t\tconst unsigned int numOfElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerAverage,\n"
		code += "\t\tconst unsigned int N,\n"
		code += "\t\t" + datatype + " *averages);\n"
	code += "\n"
	return code

def printTridiagIntegralApproximationKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void tridiagIntegralApproximationKernel<" + datatype + ">(\n"
		code += "\t\tconst " + datatype + " *initElements,\n"
		code += "\t\tconst " + datatype + " *elementsToIntegrate,\n"
		code += "\t\tconst unsigned int numOfElements,\n"
		code += "\t\tconst unsigned int numOfElementsPerAverage,\n"
		code += "\t\tconst unsigned int d,\n"
		code += "\t\tconst " + datatype + " coarseStepSize,\n"
		code += "\t\tconst " + datatype + " fineStepSize,\n"
		code += "\t\t" + datatype + " *tridiagIntegrals);\n"
	code += "\n"
	return code

randInterfaceTemplates = open("../gpgpu-src/solver/solverInterfaceTemplates", "w")
randInterfaceTemplates.write(printSingleAverage_D2DTemplates())
randInterfaceTemplates.write(printSingleAverage_H2HTemplates())
randInterfaceTemplates.write(printDoubleAverage_D2DTemplates())
randInterfaceTemplates.write(printDoubleAverage_H2HTemplates())
randInterfaceTemplates.write(printTridiagIntegralApproximation_D2DTemplates())
randInterfaceTemplates.write(printTridiagIntegralApproximation_H2HTemplates())
randInterfaceTemplates.write(printAveragedEuler_D2DTemplates())
randInterfaceTemplates.write(printAveragedEuler_H2HTemplates())
randInterfaceTemplates.write(printAveragedHeun_D2DTemplates())
randInterfaceTemplates.write(printAveragedHeun_H2HTemplates())
randInterfaceTemplates.write(printTaylor3_D2DTemplates())
randInterfaceTemplates.write(printTaylor3_H2HTemplates())
randInterfaceTemplates.write(printTaylor4_D2DTemplates())
randInterfaceTemplates.write(printTaylor4_H2HTemplates())
randInterfaceTemplates.close()

randInterfaceTemplates = open("../gpgpu-src/solver/ktTemplates", "w")
randInterfaceTemplates.write(printAveragedEulerKernelTemplates())
randInterfaceTemplates.write(printAveragedHeunKernelTemplates())
randInterfaceTemplates.write(printTaylor3KernelTemplates())
randInterfaceTemplates.write(printTaylor4KernelTemplates())
randInterfaceTemplates.close()

randInterfaceTemplates = open("../gpgpu-src/solver/averagerTemplates", "w")
randInterfaceTemplates.write(printSumKernelTemplates())
randInterfaceTemplates.write(printSingleAverageKernelTemplates())
randInterfaceTemplates.write(printDoubleAverageKernelTemplates())
randInterfaceTemplates.write(printTridiagIntegralApproximationKernelTemplates())
randInterfaceTemplates.close()

