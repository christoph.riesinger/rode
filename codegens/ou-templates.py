#!/usr/bin/python
datatypes = ["float", "double"]

# ==============================================================================
# 
# ==============================================================================

def printRealizeOUProcess_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void realizeOUProcess_D2D<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *dRandomNumbers,\n"
		code += "\t\tconst " + datatype + " stepSize,\n"
		code += "\t\tconst " + datatype + " O_0,\n"
		code += "\t\tconst unsigned int numOfRealizations,\n"
		code += "\t\t" + datatype + " *dRealization,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printRealizeOUProcess_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void realizeOUProcess_H2H<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *hRandomNumbers,\n"
		code += "\t\tconst " + datatype + " stepSize,\n"
		code += "\t\tconst " + datatype + " O_0,\n"
		code += "\t\tconst unsigned int numOfRealizations,\n"
		code += "\t\t" + datatype + " *hRealization,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printScanOU_D2DTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void scanOU_D2D<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *dInput,\n"
		code += "\t\tconst " + datatype + " mu,\n"
		code += "\t\tconst unsigned int arrayLength,\n"
		code += "\t\t" + datatype + " *dOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

def printScanOU_H2HTemplates():
	code = ""
	for datatype in datatypes:
		code += "template void scanOU_H2H<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *hInput,\n"
		code += "\t\tconst " + datatype + " mu,\n"
		code += "\t\tconst unsigned int arrayLength,\n"
		code += "\t\t" + datatype + " *hOutput,\n"
		code += "\t\tcudaStream_t &stream);\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================

def printRealizeOUProcessKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void realizeOUProcessKernel<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *prefixSums,\n"
		code += "\t\tconst " + datatype + " O_0,\n"
		code += "\t\tconst " + datatype + " mu,\n"
		code += "\t\tconst " + datatype + " sigma,\n"
		code += "\t\tconst unsigned int numOfRealizations,\n"
		code += "\t\t" + datatype + " *realization);\n"
	code += "\n"
	return code

# ==============================================================================
# 
# ==============================================================================

def printScanExclusiveOUKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void scanExclusiveOUKernel<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *input,\n"
		code += "\t\tconst " + datatype + " mu,\n"
		code += "\t\tconst unsigned int arrayLength,\n"
		code += "\t\t" + datatype + " *output,\n"
		code += "\t\t" + datatype + " *offsetArray);\n"
	code += "\n"
	return code

def printScanInclusiveOUKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void scanInclusiveOUKernel<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *input,\n"
		code += "\t\tconst " + datatype + " mu,\n"
		code += "\t\tconst unsigned int arrayLength,\n"
		code += "\t\t" + datatype + " *output,\n"
		code += "\t\t" + datatype + " *offsetArray);\n"
	code += "\n"
	return code

def printScanOUFixKernelTemplates():
	code = ""
	for datatype in datatypes:
		code += "template __global__ void scanOUFixKernel<" + datatype + ">(\n"
		code += "\t\t" + datatype + " *input,\n"
		code += "\t\t" + datatype + " *offsetArray,\n"
		code += "\t\tconst " + datatype + " mu,\n"
		code += "\t\tconst unsigned int arrayLength);\n"
	code += "\n"
	return code

randInterfaceTemplates = open("../gpgpu-src/ou/ouInterfaceTemplates", "w")
randInterfaceTemplates.write(printRealizeOUProcess_D2DTemplates())
randInterfaceTemplates.write(printRealizeOUProcess_H2HTemplates())
randInterfaceTemplates.write(printScanOU_D2DTemplates())
randInterfaceTemplates.write(printScanOU_H2HTemplates())
randInterfaceTemplates.close()

randInterfaceTemplates = open("../gpgpu-src/ou/ouProcessTemplates", "w")
randInterfaceTemplates.write(printRealizeOUProcessKernelTemplates())
randInterfaceTemplates.close()

randInterfaceTemplates = open("../gpgpu-src/ou/ouScanTemplates", "w")
randInterfaceTemplates.write(printScanExclusiveOUKernelTemplates())
randInterfaceTemplates.write(printScanInclusiveOUKernelTemplates())
randInterfaceTemplates.write(printScanOUFixKernelTemplates())
randInterfaceTemplates.close()

