#!/bin/sh

# Setting up MPI, Compiler and CUDA
# openmpi + gcc + cuda
export PATH=/usr/apps.sp3/mpi/openmpi/1.8.2/g4.3.4_cuda6.5/bin:$PATH
export LD_LIBRARY_PATH=/usr/apps.sp3/mpi/openmpi/1.8.2/g4.3.4_cuda6.5/lib:$LD_LIBRARY_PATH

# mvapich2 + gcc + cuda
# export PATH=/usr/apps.sp3/mpi/mvapich2/2.0rc1/g4.3.4_cuda6.5/bin:$PATH
# export LD_LIBRARY_PATH=/usr/apps.sp3/mpi/mvapich2/2.0rc1/g4.3.4_cuda6.5/lib:$LD_LIBRARY_PATH

# Execute the parallel jobs
cd ${HOME}/workspace/rode/gpgpu-src

for((numOfNodes=MIN_NUM_OF_NODES; numOfNodes<=MAX_NUM_OF_NODES; numOfNodes+=16))
do
	let numOfRanks=NUM_OF_RANKS_PER_NODE*numOfNodes
	
	mpirun -np ${numOfRanks} -hostfile ${PBS_NODEFILE} --map-by slot ./bin/rode_single_float > ../results/${numOfRanks}_single_float.o.txt
	mpirun -np ${numOfRanks} -hostfile ${PBS_NODEFILE} --map-by slot ./bin/rode_single_double > ../results/${numOfRanks}_single_double.o.txt
	mpirun -np ${numOfRanks} -hostfile ${PBS_NODEFILE} --map-by slot ./bin/rode_tridiag_float > ../results/${numOfRanks}_tridiag_float.o.txt
	mpirun -np ${numOfRanks} -hostfile ${PBS_NODEFILE} --map-by slot ./bin/rode_tridiag_double > ../results/${numOfRanks}_tridiag_double.o.txt
done

