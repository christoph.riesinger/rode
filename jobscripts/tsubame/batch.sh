#!/bin/sh

# The TSUBAME cluster uses an own implementation of the PBS job scheduler: t2sub.
# As input, it expects parameters which set up the parallel environment (e.g.
# proposed execution time, amount of ressources, etc.) and a bash script
# (jobscript.sh) which describes the actual execution of the parallel program,
# i.e. a call of mpirun with the executable.

# queue="R731592"
queue="G"
let minNumOfNodes=0
let maxNumOfNodes=480
let numOfGPUsPerNode=3
let numOfRanksPerNode=${numOfGPUsPerNode}
let numOfThreadsPerProcess=1
let numOfThreadsPerNode=numOfThreadsPerProcess*numOfRanksPerNode
let amountOfMemPerNode=${numOfRanksPerNode}*4

# The place option forces distribution of chunks among nodes even insufficient
# data is offered (see 5.3.6)
# free:    As many chunks as possible are scheduled on one single node (depends
#          on job queue (e.g. 24 for S queue)) before a second node is used
# scatter: Scatters chunks (number after = sign) among different nodes, so
#          different chunks would be scheduled on the same node
# pack:    At maximum, one node is allocated
place="scatter"

echo "minNumOfNodes:          "${minNumOfNodes}
echo "maxNumOfNodes:          "${maxNumOfNodes}
echo "numOfGPUsPerNode:       "${numOfGPUsPerNode}
echo "numOfRanksPerNode:      "${numOfRanksPerNode}
echo "numOfThreadsPerProcess: "${numOfThreadsPerProcess}
echo "numOfThreadsPerNode:    "${numOfThreadsPerNode}
echo "amountOfMemPerNode:     "${amountOfMemPerNode}"GB"

t2sub -q ${queue} -W group_list=t2g-TUM-Infomatics -l walltime=00:08:00 -l select=${maxNumOfNodes}:gpus=${numOfGPUsPerNode}:mpiprocs=${numOfRanksPerNode}:ncpus=${numOfThreadsPerNode}:mem=${amountOfMemPerNode}gb -l place=${place} -v MIN_NUM_OF_NODES=${minNumOfNodes},MAX_NUM_OF_NODES=${maxNumOfNodes},NUM_OF_RANKS_PER_NODE=${numOfRanksPerNode} -o ${HOME}/workspace/rode/results/output.txt -e ${HOME}/workspace/rode/results/error.txt ./jobscript.sh

