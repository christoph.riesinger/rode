#!/bin/sh

# Setting up MPI, Compiler and CUDA
# parastationmpi + gcc + cuda
module load cuda/6.0
module load parastation/gcc-5.0.27

# Execute the parallel job
cd ${HOME}/workspace/rode/gpgpu-src
mpiexec -np=${TOTAL_NUMBER_OF_RANKS} ./bin/rode

