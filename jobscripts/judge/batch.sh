#!/bin/sh

# The JUDGE cluster uses Moab, a scheduler which is pretty similar to PBS: msub.
# As input, it expects parameters which set up the parallel environment (e.g.
# proposed execution time, amount of ressources, etc.) and a bash script
# (jobscript.sh) which describes the actual execution of the parallel program,
# i.e. a call of mpirun with the executable.

queue="common"
let maxNumOfNodes=206
let numOfGPUsPerNode=2
let numOfRanksPerNode=${numOfGPUsPerNode}
let amountOfMemPerNode=${numOfRanksPerNode}*4

echo "maxNumOfNodes:      "${maxNumOfNodes}
echo "numOfGPUsPerNode:   "${numOfGPUsPerNode}
echo "numOfRanksPerNode:  "${numOfRanksPerNode}
echo "amountOfMemPerNode: "${amountOfMemPerNode}"GB"

for((numOfNodes=8; numOfNodes<=maxNumOfNodes; numOfNodes+=4))
do
	let numOfGPUs=numOfGPUsPerNode*numOfNodes
	let numOfRanks=numOfRanksPerNode*numOfNodes
	
	echo "Scheduling job on "${numOfNodes}" nodes, spawning "${numOfRanks}" MPI ranks to utilize "${numOfGPUs}" GPUs"

	msub -q ${queue} -l nodes=${numOfNodes}:ppn=${numOfRanksPerNode}:gpus=${numOfGPUsPerNode}:mem=${amountOfMemPerNode}gb:walltime=00:01:00 -v TOTAL_NUMBER_OF_RANKS=${numOfRanks} -o ${HOME}/workspace/rode/results/${numOfRanks}.o.txt -e ${HOME}/workspace/rode/results/${numOfRanks}.e.txt ./jobscript.sh
done

