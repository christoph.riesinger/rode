function x = OU_Matrix(x0, t_end, dt, tau, c, N, seeds)
% function OU(t_start, t_end, dt, tau, c)
% Calculates N sample paths of an Ornstein-Uhlenbeck process from time
% t_start to time t_end. The method is an implementation of Eq. 3.5 a of
% Gillespie's 1996 paper: Exact numerical simulation of the
% Ornstein-Uhlenbeck process and its integral.
%
% Input:
% x0:       initial value
% t_end:    end time of the simulation
% dt:       time step
% tau:      relaxation time
% c:        diffusion constant
% N:        number of sample paths to be generated
% seeds:    seeds for random number generation
%
% Output:
% x:        a matrix with N sample paths of the Ornstein-Uhlenbeck process,
%           that is, it has dimension (N)*(Nt), where Nt = ceil(t_end/dt)+1.


% Use this command to fix a sequence of random numbers
% [hi,ho]=size(seeds);
% assert(hi==1 && ho==1);

% randn('state',seeds);

% Parameters of the method
mu = exp(-dt/tau);
sigma_x = sqrt(c*tau/2*(1-mu^2));

% disp(mu);
% disp(sigma_x);

% equivalent presentations of Nt (total number of samplings):
% - Nt = ceil(t_end/h) * "number of subintervalls during one 'big' timestep h"
Nt = ceil(t_end/dt)+1;

% Computation of the sample path  matrix x
x = zeros(N,Nt);

% Shouldn't be necessary, since we take x0 = 0
x(:,1) = x0;

% The following seperation in two for-looks is the difference to the
% non-vectorized version.

% Fills n1 with uniformly distributed random numbers. For every path there's an
% own seed in seeds.
n1 = zeros(N,Nt);
for j = 1:N
   rng(seeds(j));  % Commented out if running in release mode / debug mode comment in
   % Uniformly distributed random number
   n1(j,2:end) = randn(1,Nt-1);
end

p = '../test/ou/';
f = 'rn_gold.dat';
fid = fopen([p f],'w'); 
formatSpec = '%.20f\n';
fprintf(fid, formatSpec, n1(1,:));
fclose(fid);

% Computation of the OU matrix. Formula can be found in Alfredo's summary,
% formula (3).
for i = 2:Nt
    for j=1:N
       % Matrix of OU Processes
       x(j,i) = x(j,i-1)*mu + sigma_x*n1(j,i);
    end
end

p = '../test/ou/';
f = 'ou_gold.dat';
fid = fopen([p f],'w'); 
formatSpec = '%.20f\n';
fprintf(fid, formatSpec, x(1,:));
fclose(fid);
