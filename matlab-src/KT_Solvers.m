function xMat = KT_Solvers(x0, v0, t_end, h, zeta, om, noSubcyclingTimeSteps, method, N, seeds)
% 
% Solves the Kanai-Tajimi 2nd order differential equation in its RODE form,
% using the averaged explicit Euler and Heun methods.
%
% Input:
% x0:       initial value of the position
% v0:       initial value of the velocity
% t_end:    end time of the simulation
% h:        timestep
% zeta:     parameter of the diff. eq.
% om:       parameter of the diff. eq.
% noSubcyclingTimeSteps: number of subintervals to average the function (only for Euler and Heun)
% method:   Choose 1 for 'Euler', 2 for 'Heun', or 3 for 'RODE3'
% N:        number of stochastic realisations/sample paths
% seeds:    seeds for random number generation
%
% Output:
% x:        solution of the position for the Kanai-Tajimi differential equation
% v:        solution of the velocity for the Kanai-Tajimi differential equation

t = 0:h:t_end;
Nt = length(t);

% Obtain a sample path of the Ornstein-Uhlenbeck process. Since we need to
% average this function on each interval h, we generate it with noSubcyclingTimeSteps*t_end/h
% points

tau = 1;                                    % Relaxation time of the OU process
c = 1;                                      % Diffusion constant of the OU process

% Perform the transformation to the Z variables, namely:
% Z = [z1;z2] := [x,y-O_t] = [x, -x'-O_t]

% Initial values
% Z = [z1_0;z2_0] := [x_0,y_0-O_0] = [x_0, -x_0'-O_0] = [x_0, -x_0'-x_0] = [x_0, -y_0-x_0]
z1 = x0;
z2 = -v0-x0;
% Z is a matrix and has dimension N*2 (rows * cols). For now, it only contains
% the initial values x_0 for position and v_0 for velocity.
Z = [z1; z2];                       % Solution vector Z

if (method==1 || method==2 )
    % Setup the step size for one subcycling step
    if method==1 %Euler 
        % From KT_Stochastic we have noSubcyclingTimeSteps = 1 / h
        del = h/noSubcyclingTimeSteps;         % Sub-step-size to carry out the averaging
    else
        noSubcyclingTimeSteps = ceil(1/h^3);
        del = h/noSubcyclingTimeSteps;
    end
    
    % Here we generate a matrix with N sample paths of the OU process
    
    % ou = OU_Matrix_No_Vectorization(0, t_end, del, tau, c, N, seeds);
    % ou contains all samplings over the whole simulation time [0,t_end]
    % including the subcycling steps with stepsize del.
    ou = OU_Matrix(0, t_end, del, tau, c, N, seeds);
    % disp('size ou')
    % size(ou)
    % dlmwrite('ou.csv',ou,'precision','%1.16e')
    
    % ou_Coarse contains only those samplings at the big steps stepsize h.
    ou_Coarse = ou(:,1:noSubcyclingTimeSteps:end);                    
    % disp('size ou-coarse')
    % size(ou_Coarse)
    % dlmwrite('oucoarse.csv',ou_Coarse,'precision','%1.16e')
    
    p = '../test/ou/';
    f = 'ou_gold.dat';
    fid = fopen([p f],'w'); 
    formatSpec = '%.20f\n';
    fprintf(fid, formatSpec, ou(1,:));
    fclose(fid);
    
    p = '../test/ou/';
    f = 'ou_coarse_gold.dat';
    fid = fopen([p f],'w'); 
    formatSpec = '%.20f\n';
    fprintf(fid, formatSpec, ou_Coarse(1,:));
    fclose(fid);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solve the differential equation by the chosen method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch method
    
    case 1 %'Euler'
        [xMat] = stochasticEuler(Nt, ou, ou_Coarse, Z, h, zeta, om, noSubcyclingTimeSteps, N);
        
        % disp('size xMat')
        % size(xMat)
        % dlmwrite('xMat.csv',xMat,'precision','%1.16e')

    case 2 %'Heun'
        [xMat] = stochasticHeun(Nt, ou, ou_Coarse, Z, h, zeta, om, noSubcyclingTimeSteps, N);

    otherwise
        disp('Error: Method not correctly specified.')
end
end % function



function [xMat] = stochasticEuler(Nt, ou, ou_Coarse, Z, h, zeta, om, noSubcyclingTimeSteps, N)
    xMat   = zeros(2*N,Nt); 
    
	% New Z. Don't mistake it with the Z in KT_Solvers and with the Z in this
	% signature.
	% In every iteration, two vaules are computed and stored. It's done in every
	% "big" timesptep (Nt) and in every path (N).
    Z = zeros(2*N,Nt);
    
    p = '../test/solver/';
    fSingle = 'singleAverage_gold.dat';
    fidSingle = fopen([p fSingle],'w'); 
    formatSpec = '%.20f\n';
    
    % loop over time
    for n = 1:Nt-1
        % Loop over all the different realizations of the OU process
        for nn = 1:N
            % Now we take the average of the OU process over the current interval
            ou_av = sum(ou(nn,(n-1)*noSubcyclingTimeSteps+2 : n*noSubcyclingTimeSteps+1));
            ou_av = ou_av/noSubcyclingTimeSteps;

            G = [-1;               1 - 2*zeta*om]*ou_av;      % First average of G
            H = [-Z((nn-1)*2+2,n); -2*zeta*om*Z((nn-1)*2+2,n) + om^2*Z((nn-1)*2+1,n)];
            
            % Solution of the KT RODE
            Z((nn-1)*2+1:(nn-1)*2+2,n+1) = Z((nn-1)*2+1:(nn-1)*2+2,n) + h*( G + H );                               
        end
    
        fprintf(fidSingle, formatSpec, ou_av);
        
        % Transform Z vector to x and v coordinates and create matrix of
        % all solutions
        % Z = [z1;z2] := [x,y-OU] = ???
        xMat(1:2:end,n+1) =  Z(1:2:end,n+1);
        xMat(2:2:end,n+1) = -Z(2:2:end,n+1) - ou_Coarse(:,n+1);        
    end
    
    fZ = 'z_gold.dat';
    fidZ = fopen([p fZ],'w'); 
    fprintf(fidZ, formatSpec, Z);
    fclose(fidZ);
    
    fclose(fidSingle);
end

    
function [xMat] = stochasticHeun(Nt, ou, ou_Coarse, Z,h, zeta, om, noSubcyclingTimeSteps, N)
    xMat = zeros(2*N,Nt);
    
    % New Z. Don't mistake it with the Z in KT_Solvers and with the Z in this
	% signature.
	% In every iteration, two vaules are computed and stored. It's done in every
	% "big" timesptep (Nt) and in every path (N).
    Z = zeros(2*N,Nt);
    
    formatSpec = '%.20f\n';
    p = '../test/solver/';
    fSingle = 'singleAverage_gold.dat';
    fDouble = 'doubleAverage_gold.dat';
    fidSingle = fopen([p fSingle],'w');
    fidDouble = fopen([p fDouble],'w');
    
    % loop over time
    for n = 1:Nt-1

        factor = noSubcyclingTimeSteps - (0:(noSubcyclingTimeSteps-1));
        % Loop over all the different realizations of the OU process
        for nn = 1:N
            % Now we take the average of the OU process over the current interval
            ou_av = sum(ou(nn,(n-1)*noSubcyclingTimeSteps+2 : n*noSubcyclingTimeSteps+1));
            ou_av2 = sum(factor.*ou(nn,(n-1)*noSubcyclingTimeSteps+2 : n*noSubcyclingTimeSteps+1));
            ou_av = ou_av/noSubcyclingTimeSteps;
            ou_av2 = 2*ou_av2/noSubcyclingTimeSteps^2;
            
            G1 = [-1; 1 - 2*zeta*om]*ou_av;      % First average of G
            G2 = [-1; 1 - 2*zeta*om]*ou_av2;     % Double average of F

            H = [-Z(2,n); -2*zeta*om*Z(2,n) + om^2*Z(1,n)];
            H2 = [ -( Z(2,n) + h*G2(2) + h*H(2) ); -2*zeta*om*( Z(2,n) + h*G2(2) + h*H(2)) + om^2*( Z(1,n) + h*G2(1) + h*H(1) )];
            
            % Solution of the KT RODE
            Z((nn-1)*2+1:(nn-1)*2+2,n+1) = Z((nn-1)*2+1:(nn-1)*2+2,n) + h*( G1 + H/2 + H2/2 );
        end
            
        fprintf(fidSingle, formatSpec, ou_av);
        fprintf(fidDouble, formatSpec, ou_av2);

        % Transform Z vector to x and v coordinates and create matrix of
        % all solutions
        % Z = [z1;z2] := [x,y-OU] = ???
        xMat(1:2:end,n+1) =  Z(1:2:end,n+1);
        xMat(2:2:end,n+1) = -Z(2:2:end,n+1) - ou_Coarse(:,n+1);
    end
    
    fZ = 'z_gold.dat';
    fidZ = fopen([p fZ],'w'); 
    fprintf(fidZ, formatSpec, Z);
    fclose(fidZ);
    
    fclose(fidSingle);
    fclose(fidDouble);
end
        
