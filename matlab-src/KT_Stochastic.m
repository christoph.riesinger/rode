function ddu = KT_Stochastic(h,t_end,method,N,seeds)
% KT_Stochastic.m
% Calls the different solvers for the Kanai-Tajimi model, specifying the
% equation parameters.
%
% Input:
% h:        stepsize of subcycling time steps
% t_end:    final time
% method:   numerical scheme (choose 'Euler','Heun', or 'RODE3')
% N:        number of stochastic realisations
% seeds:    seeds for random number generation
%
% Output
% u:        displacement of the ground
% du:       velocity of the ground
% ddu:      acceleration of the ground (solution of the KT equation)

% Equation parameters

x0 = 0;                 % Initial value of the position
v0 = 0;                 % Initial value of the velocity
zeta_g = 0.64;          % Eq. parameter
omega_g = 15.56;        % Eq. parameter        
noSubcyclingTimeSteps = ceil(1/h);

% Solve for x_g and x'_g in the Kanai-Tajimi model

xMat = KT_Solvers(x0, v0, t_end, h, zeta_g, omega_g, noSubcyclingTimeSteps, method, N, seeds);

% Output ground acceleration u''_g. This output can be used directly for the
% multi-story buildings. The formula corresponds to the very first formula
% on Alfredo's abstract on page 1.
%
% xMat(2:2:end,:): Liefert eine Matrix zurück, die alle geraden Spalten von
% xMat enthält. 2:2:end heißt, dass von 2 bis end iteriert wird und und der
% Zähler immer um 2 inkrementiert wird.

ddu = -2*zeta_g*omega_g*xMat(2:2:end,:) - omega_g^2*xMat(1:2:end,:);

p = '../test/solver/';
f = 'ddu_gold.dat';
fid = fopen([p f],'w'); 
formatSpec = '%.20f\n';
fprintf(fid, formatSpec, ddu(1,:));
fclose(fid);
