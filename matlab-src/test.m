tf = 2;             % end point of time
% h = 1/(2^10 - 123);
h = 1/(2^5 - 3);
tau = 1;            % system parameter
c = 1;              % system parameter
N = 1;              % number of sample paths
seeds = 5678;

% ouMatrix = OU_Matrix(0, tf, h, tau, c, N, seeds);
ddu      = KT_Stochastic(h,tf,2,N,seeds);